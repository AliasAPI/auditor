<?php

namespace AliasAPI\StepsTEAM;

use AliasAPI\Api as API;
use AliasAPI\Alias as Alias;
use AliasAPI\Autoload as Autoload;

if (! function_exists('bootstrap')) {
    function bootstrap()
    {
        $settings = [];

        \defined('BASEPATH') || \define('BASEPATH', \realpath(\dirname(__FILE__) . '/..'));
        require_once(BASEPATH . '/vendor/aliasapi/api/src/set_defaults.php');

        $settings = API\set_defaults();

        Autoload\pipeline(
            $settings['basepath'],
            $settings['load_paths'],
            $settings['load_types'],
            $settings['extra_files'],
            $settings['max_depth']
        );

        API\set_domains();

        // $_SESSION['alias_attributes'] = Alias\pipeline(['alias' => 'StepsTEAM']);

        return $settings;
    }
}
