<?php

namespace AliasAPI\Auditor;

use AliasAPI\Api as API;

require_once('../src/bootstrap.php');

$settings = bootstrap();

API\pipeline($settings);
