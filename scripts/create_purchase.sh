#!/bin/sh

curl "https://alias.api/" \
    --request POST \
    --include \
    --insecure \
    --user SandboxRest:api_pass \
    --header "Content-Type: application/json" \
    --data '{
            "actionS" : "create purchase",
            "alias" : "SandboxRest",
            "user" : "UUID",
            "cart" : "promote-1",
            "amount" : "10.00",
            "currency" : "USD",
            "description" : "Movin on up",
            "cancel_url" : "https://alias.api/cancel",
            "return_url" : "https://alias.api/return"
        }'
printf "\n"
