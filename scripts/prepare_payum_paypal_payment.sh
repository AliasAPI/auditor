#!/bin/sh

curl "https://alias.api" \
    --request POST \
    --include \
    --insecure \
    --user PayumSandbox:api_pass \
    --header "Content-Type: application/json" \
    --data '{
        "actionS" : "prepare payum paypal payment",
        "alias" : "PayumSandbox",
        "user" : "UUID",
        "cart" : "promote-1",
        "description" : "The first transaction",
        "total" : "10.00",
        "currency" : "USD"
    }'
printf "\n"
