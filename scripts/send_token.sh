#!/bin/sh

curl "https://localhost/new/public/?api=req" \
    --request POST \
    --include \
    --insecure \
    --header "Authorization: Bearer ACCESS_TOKEN" \
    --header "Content-Type: application/json" \
    --data '{ "title": "Test the API", "order": 10 }'
printf "\n"
