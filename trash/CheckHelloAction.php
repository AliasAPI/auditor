<?php

declare(strict_types=1);

namespace AliasAPI\Action\Ping;

class CheckHelloAction
{
    public function __invoke(Payload $payload)
    {
        $okay = $payload->getOkay();
        $okay = $payload->setOkay(false);

        return $payload;
    }
}
