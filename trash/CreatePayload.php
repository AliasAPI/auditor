<?php

declare(strict_types=1);

namespace AliasAPI\Action\Ping;

use AliasAPI\Action\Ping as P;

class CreatePayload
{
    public function __invoke()
    {
        $payload = new P\Payload();
        $payload->setOkay(true);

        return $payload;
    }
}
