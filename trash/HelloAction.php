<?php

declare(strict_types=1);

namespace AliasAPI\Action\Ping;

class HelloAction
{
    public function __invoke(Payload $payload)
    {
        echo "HelloAction<pre>";
        var_dump($payload);
        return $payload;
    }
}
