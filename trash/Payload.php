<?php

declare(strict_types=1);

namespace AliasAPI\Action\Ping;

class Payload
{
    private $okay = true;

    public function setOkay(bool $okay)
    {
        $this->okay = $okay;
        return $this;
    }

    public function getOkay()
    {
        return $this->okay;
    }
}
