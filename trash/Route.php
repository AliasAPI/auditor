<?php

use AliasAPI\Action\Ping\HelloAction;
use AliasAPI\Action\Ping\TimeAction;

$router = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
    // $r->addRoute("GET", "/pings/hello", \AliasAPI\Action\Ping\HelloAction::class);
    // Group Capture
    // Group Complete
    // Group Refund

    // No route or /api ???
    $r->addRoute("GET", "/pings/{id}", HelloAction::class);
    // api pipeline
    // Interruptable set checkAction to return false
    // $r->addRoute("GET", "/api/capture", CaptureAction::class);
});
