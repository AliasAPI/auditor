<?php

declare(strict_types=1);

namespace AliasAPI\Action\Ping;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class TimeAction
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Check for Requirements?

        $response->getBody()->write("Drew!");

        return $response;
    }
}
