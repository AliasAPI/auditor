<?php

declare(strict_types=1);

namespace AliasAPI\Authorize;

// username, client, pass, actionS,
function check_authorization_headers($headers): void
{
    if (is_string($headers) && !empty($headers)) {
        if (stripos($headers, 'Bearer') !== 0 && stripos($headers, 'Basic') !== 0) {
            answer(400, ["The authorization header did not contain Basic or Bearer."]);
        }
    } else {
        answer(400, ["You must send an authorization header."]);
    }
}
