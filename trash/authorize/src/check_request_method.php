<?php

declare(strict_types=1);

namespace AliasAPI\Authorize;

function check_request_method()
{
    // Only allow access for curl post requests.
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        answer(400, ["This API only accepts POST requests."]);
    }
}
