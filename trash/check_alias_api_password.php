<?php

declare(strict_types=1);

namespace AliasAPI\Authorize;

function check_alias_api_password(string $api_pass, $alias_attributes)
{
    if (! isset($alias_attributes) || empty($alias_attributes)) {
        answer(501, ["The alias_attributes are not set on the server side."]);
    } elseif (!isset($api_pass) || empty($api_pass)) {
        answer(401, ["The Alias api_pass is required."]);
    } elseif ($api_pass !== $alias_attributes['api_pass']) {
        answer(401, ["The Alias api_pass does not match."]);
    }
}
