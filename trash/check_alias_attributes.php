


<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

function check_alias_attributes(array $attributes): void
{
    if (empty($attributes)) {
        answer(501, ["The Alias has no attributes."]);
    }

    if (!isset($attributes['alias'])) {
        answer(501, ["The Alias alias has not been configured."]);
    }

    if (!isset($attributes['api_pass'])) {
        answer(501, ["The Alias api_pass has not been configured."]);
    }

    if (isset($attributes['api_pass']) && empty($attributes['api_pass'])) {
        answer(501, ["The Alias api_pass is configured to blank."]);
    }

    if (!isset($attributes['authorized_actions']) || empty($attributes['authorized_actions'])) {
        answer(501, ["The Alias has no authorized API actions configured."]);
    }

    if (!isset($attributes['test_mode']) || !is_bool($attributes['test_mode'])) {
        answer(501, ["The Alias test_mode has not been configured correctly."]);
    }

    if (isset($attributes['database'])) {
        if (!isset($attributes['database']['dsn']) || empty($attributes['database']['dsn'])) {
            answer(501, ["The database [ dsn ] has not been configured correctly."]);
        }

        if (!isset($attributes['database']['username']) || empty($attributes['database']['username'])) {
            answer(501, ["The database [ username ] has not been configured correctly."]);
        }

        if (!isset($attributes['database']['password'])
        || ($attributes['test_mode'] !== true && empty($attributes['database']['password']))) {
            answer(501, ["The database [ password ] has not been configured correctly."]);
        }
    }
}
