<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

function check_alias_configs_global(): void
{
    global $only_set_in_set_alias_configurations_global_function;

    if (!isset($only_set_in_set_alias_configurations_global_function)) {
        answer(501, ["The alias configurations global is not set."]);
    } elseif (!is_array($only_set_in_set_alias_configurations_global_function)) {
        answer(501, ["The alias configurations global is not an array."]);
    } elseif (empty($only_set_in_set_alias_configurations_global_function)) {
        answer(501, ["The alias configurations global is empty."]);
    }
}
