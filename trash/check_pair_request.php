<?php

declare(strict_types=1);

namespace AliasAPI\ApiPair;

function check_pair_request($request)
{
    if (isset($request['body']['actionS']) && $request['body']['actionS'] == 'create alias pair') {
        if (!isset($request['body']['ally'])) {
            answer(400, ["The ally is not set in the request."]);
        }

        if (!isset($request['body']['ally']['alias'])) {
            answer(400, ["The ally alias is not set in the request."]);
        }

        if (!isset($request['body']['ally']['client'])) {
            answer(400, ["The ally client is not set in the request."]);
        }

        if (!isset($request['body']['tag'])) {
            answer(400, ["The tag is not set in the request."]);
        }

        if (!isset($request['body']['client_url'])) {
            answer(400, ["The client_url is not set in the request."]);
        }

        if (!isset($request['body']['public_key'])) {
            answer(400, ["The public_key is not set in the request."]);
        }
    }
}
