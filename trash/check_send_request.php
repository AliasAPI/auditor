<?php

declare(strict_types=1);

namespace AliasAPI\Authorize;

/** Checks the curl input
 * @param $cred array, has credentials for the transmission
 * @param $request array, has cURL request to be transmitted
 * curl_url where the curl transmission will be sent
 * curl_user the username to authorize the request
 * curl_pass the password to authorize the request
 * curl_token string, optional authorization token
 * curl_method string, curl method (i.e. GET, POST, DELETE, PUT)
 */
function check_send_request($cred, $json_request)
{
    comment("check_send_request()", 1);

    if (!is_array($cred) || empty($cred)) {
        answer(400, ["The curl \$credentials array is not set."]);
    }

    if (! isset($cred['curl_url'])) {
        answer(400, ["The curl_url was not set for curl."]);
    }

    if (! isset($cred['curl_user'])) {
        answer(400, ["The curl_user was not set for curl."]);
    }

    if (! isset($cred['curl_pass'])) {
        answer(400, ["The curl_pass was not set for curl."]);
    }

    if (! isset($cred['curl_method'])) {
        answer(400, ["The curl_method was not set for curl."]);
    }

    if (is_array($json_request)) {
        answer(400, ["The json_request is an array."]);
    }

    if (empty($json_request) || !$json_request || $json_request == null) {
        answer(400, ["The json_request is empty."]);
    }
    comment("The input for curl is OK.", 1);
}
