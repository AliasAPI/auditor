<?php

declare(strict_types=1);

function get_posted_curl(string $headers): ?array
{
    $username = '';
    $password = '';
    $access_token = '';
    $payload = '';

    // checking $_SERVER['HTTP_AUTHORIZATION'] is set or not
    if (isset($headers) && !empty($headers)) {

        // extract Basic auth values from  $_SERVER['HTTP_AUTHORIZATION']
        if (preg_match('/Basic\s+(.*)$/i', $headers, $matches)) {
            list($username, $password) = explode(':', base64_decode($matches[1]));
        } else {
            $username = (isset($_SERVER['PHP_AUTH_USER'])) ? $_SERVER['PHP_AUTH_USER'] : null;
            $password = (isset($_SERVER['PHP_AUTH_PW'])) ? $_SERVER['PHP_AUTH_PW'] : null;
        }

        if (preg_match('/Bearer\s+(.*)$/i', $headers, $matches)) {
            $access_token = $matches[1];
        }

        $payload = file_get_contents("php://input");

        secret("username $username | password $password | access_token $access_token | payload $payload ", 1);

        return array($username, $password, $access_token, $payload);
    }
}
