<?php


function test_url_status($url)
{
    comment("Testing the url status", 1);

    $ch = curl_init($url);

    @curl_setopt($ch, CURLOPT_HEADER, true);
    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
    @curl_setopt($ch, CURLOPT_TIMEOUT, 10);

    $output = curl_exec($ch);

    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    return $httpcode;
}
