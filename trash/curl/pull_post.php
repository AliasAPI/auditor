<?php

function pull_post()
{
    comment("pull_post()", 1);

    // Get the (JSON) post that was transmitted
    $post = file_get_contents("php://input");

    if ($post) {
        comment("post received [ " . $post . " ]", 1);
    } else {
       answer(400, ["No post received."]);
    }

    return $post;
}
