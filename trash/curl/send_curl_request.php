<?php

/**
* Transmits a cURL request
* @param $cred array, credentials
* @param $request json, has cURL request to be transmitted
* @return $response json, or raw return?
*/
function send_curl_request($cred, $json_request)
{
    $response = '';

    $this->debug[] = "Curl class";

    // sayd( $json_request );
    // $this->debug[] = "sendRequest() [[[[ $json_request ]]]] ]";
    // $this->checkInput($cred, $json_request);

    // If there are NO errors . . .
    if (empty($this->errors)) {
        $this->debug[] = "sendRequest()";

        // Otherwise, send the transmission
        // $header = $this->setHeader($cred, $json_request);
        // OPTS whas here

        try {
            $this->addSession($cred['curl_url'], $opts);

            $this->debug[] = "Transmitting to [ " . $cred['curl_url'] . " ]";

            // Get the response from the remote server
            $response = $this->exec();

            $this->debug[] = "\$response = [ " . $response . " ]";

            $this->clear();
        } catch (\Exception $e) {
            echo "Exception " . $e->getMessage();
        }
    }

    // Return the json
    return $response;
}
