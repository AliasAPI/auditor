<?php


function set_setopts($cred, $json_request)
{
    comment("set_send_header()");

    $header = array();

    $header[] = 'Content-type: application/json';

    if (isset($cred['access_token']) && !empty($cred['access_token'])) {
        $header[] = "Authorization: Token " . $cred['access_token'];
    }

    // For some security reason we do not set the content-length?
    // $header[] = 'Content-length: ' . strlen($json_request);

    // Build an array of curl_setopt options
    $setopts = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_USERPWD => $cred['curl_user'] . ':' . $cred['curl_pass'],
        CURLOPT_CUSTOMREQUEST => $cred[ 'curl_method' ],
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_TIMEOUT => 3, // Removed because of the blank responses
        // CURLOPT_POST => TRUE, // Use POST method
        CURLOPT_POSTFIELDS => "" . $json_request . "",
        CURLOPT_VERBOSE => true );

    return $setopts;
}
