<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

function get_alias_attributes(string $alias): ?array
{
    $alias_configs = get_alias_configs_global();

    if (array_key_exists($alias, $alias_configs)) {
        $alias_attributes = $alias_configs[$alias];

        return $alias_attributes;
    } else {
        answer(501, ["The Alias is not found in the alias_configs."]);
    }
}
