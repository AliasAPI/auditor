<?php

declare(strict_types=1);

namespace AliasAPI\Omnipay;

use League\Pipeline as PL;

class PipeCancelPurchase
{
    public function __invoke(array $request)
    {
        try {
            // tag:PipeCancelPurchase
            if ($request['actionS'] == 'cancel purchase') {
                $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetAliasAttributes)
                    ->pipe(new SetParameters)
                    ->pipe(new ConnectORM)
                    ->pipe(new SelectTransaction)
                    ->pipe(new DeletePurchase)
                    ->pipe(new DeleteJsonFile)
                    ->pipe(new SetRedirectUrl)
                    ->pipe(new SetResponse);

                return (array) $pipeline->process($request);
            }
        } catch (\Exception $e) {
            $error[] = "There was an error while processing the Cancel Purchase Pipeline.";
            $error[] = "Exception Type: " . get_class($e) . "";
            $error[] = "Error Message: " . $e->getMessage() . "";
            return [500, $error];
        }
    }
}
