<?php

declare(strict_types=1);

namespace AliasAPI\Omnipay;

use League\Pipeline as PL;

class PipeCreatePurchase
{
    public function __invoke(array $request)
    {
        try {
            // tag:PipeCreatePurchase
            if ($request['actionS'] == 'create purchase') {
                $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetAliasAttributes)
                    ->pipe(new SetCreatePurchaseOK)
                    ->pipe(new SetParameters)
                    ->pipe(new ConnectORM)
                    ->pipe(new CreateGateway)
                    ->pipe(new InitializeGateway)
                    ->pipe(new BuildRedirectUrls)
                    ->pipe(new CreatePurchase)
                    ->pipe(new SendPurchaseOrder)
                    ->pipe(new SetTransactionRow)
                    ->pipe(new InsertTransaction)
                    ->pipe(new SelectTransaction)
                    ->pipe(new CreateJsonFile)
                    ->pipe(new SetRedirectUrl)
                    ->pipe(new SetResponse);

                return (array) $pipeline->process($request);
            }
        } catch (\Exception $e) {
            $error[] = "There was an error while processing the Create Purchase Pipeline.";
            $error[] = "Exception Type: " . get_class($e) . "";
            $error[] = "Error Message: " . $e->getMessage() . "";
            return [500, $error];
        }
    }
}
