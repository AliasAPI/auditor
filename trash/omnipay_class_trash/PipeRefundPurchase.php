<?php

declare(strict_types=1);

namespace AliasAPI\Omnipay;

use League\Pipeline as PL;

class PipeRefundPurchase
{
    public function __invoke(array $request)
    {
        try {
            // tag:PipeRefundPurchase
            if ($request['actionS'] == 'refund purchase') {
                $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetAliasAttributes)
                    ->pipe(new SetParameters)
                    ->pipe(new ConnectORM)
                    ->pipe(new SelectTransaction)
                    ->pipe(new SetRefundPurchaseOK)
                    ->pipe(new CreateGateway)
                    ->pipe(new InitializeGateway)
                    ->pipe(new RefundPurchase)
                    ->pipe(new SetTransactionRow)
                    ->pipe(new InsertTransaction)
                    ->pipe(new SelectTransaction)
                    ->pipe(new DeleteJsonFile)
                    ->pipe(new SetRedirectUrl)
                    ->pipe(new SetResponse);

                return (array) $pipeline->process($request);
            }
        } catch (\Exception $e) {
            $error[] = "There was an error while processing the Refund Purchase Pipeline.";
            $error[] = "Exception Type: " . get_class($e) . "";
            $error[] = "Error Message: " . $e->getMessage() . "";
            return [500, $error];
        }
    }
}
