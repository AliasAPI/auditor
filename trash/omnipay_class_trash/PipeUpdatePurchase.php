<?php

declare(strict_types=1);

namespace AliasAPI\Omnipay;

use League\Pipeline as PL;

class PipeUpdatePurchase
{
    public function __invoke(array $request)
    {
        try {
            // tag:PipeUpdatePurchase
            if ($request['actionS'] == 'update purchase') {
                $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetAliasAttributes)
                    ->pipe(new SetParameters)
                    ->pipe(new ConnectORM)
                    ->pipe(new SelectTransaction)
                    // Implement only if the system fails to update the status, saleid, refundid columns.
                    // ->pipe(new CreateGateway)
                    // ->pipe(new InitializeGateway)
                    // ->pipe(new FetchPurchase)
                    // ->pipe(new SetTransactionRow)
                    // ->pipe(new UpdateTransaction)
                    ->pipe(new DeleteJsonFile)
                    ->pipe(new SetRedirectUrl)
                    ->pipe(new SetResponse);

                return (array) $pipeline->process($request);
            }
        } catch (\Exception $e) {
            $error[] = "There was an error while processing the Update Purchase Pipeline.";
            $error[] = "Exception Type: " . get_class($e) . "";
            $error[] = "Error Message: " . $e->getMessage() . "";
            return [500, $error];
        }
    }
}
