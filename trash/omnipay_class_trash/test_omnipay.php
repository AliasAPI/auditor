<?php

namespace AliasAPI\Omnipay;

test_omnipay();

function test_omnipay()
{
    // Run the autoloader and
    require_once(__DIR__ . '/bootstrap.php');
    $tag_array = bootstrap();

    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();

    $request = configure_requests();
    // sayd($request);
    if ($request['actionS'] == 'create purchase') {
        $pipe = new PipeCreatePurchase;
        $response = $pipe($request);
    }

    if ($request['actionS'] == 'cancel purchase') {
        $pipe = new PipeCancelPurchase;
        $response = $pipe($request);
    }
    // sayd($request);
    if ($request['actionS'] == 'complete purchase') {
        $pipe = new PipeCompletePurchase;
        $response = $pipe($request);
    }

    $request = configure_requests('refund');

    if ($request['actionS'] == 'refund purchase') {
        $pipe = new PipeRefundPurchase;
        $response = $pipe($request);
    }

    $request = configure_requests('update');

    if ($request['actionS'] == 'update purchase') {
        $pipe = new PipeUpdatePurchase;
        $response = (array) $pipe($request);
    }

    echo "<pre>";
    echo "<hr>test_omnipay response:<br>";
    print_r($response);
}
