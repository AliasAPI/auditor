<?php

// For Authorization
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Cache\AuthorizationCache;
use PayPal\Core\PayPalConfigManager;
use PayPal\Rest\ApiContext;

function authorize_paypa1($request)
{
    global $response;

    // If there are errors, stop processing
    if (isset($response['errors'])) {
        return;
    }

    $response['debug'][] = 'authorize_paypa1()';

    $apiContext = new \PayPal\Rest\ApiContext(
        new \PayPal\Auth\OAuthTokenCredential(
        $request['secret']['client_user'], // ClientID
        $request['secret']['client_pass']  // ClientSecret
        )
    );

    // Not sure why this is set
    $apiContext->setConfig(
        array( 'mode' => $request['secret']['mode']
            // 'log.LogEnabled' => $prep['log.LogEnabled'],
            // 'log.FileName' => $prep['log.FileName'],
            // 'log.LogLevel' => $prep['log.LogLevel'],
            // 'cache.enabled' => $prep['cache.enabled'],
            // 'cache.FileName' => $prep['cache.FileName'],
            // 'http.CURLOPT_CONNECTTIMEOUT' => $prep['http.CURLOPT_CONNECTTIMEOUT'],
            // 'http.headers.PayPal-Partner-Attribution-Id' => $prep['http.headers.PayPal-Partner-Attribution-Id'],
            // 'log.AdapterFactory' => $prep['log.AdapterFactory']
        )
    );

    /*
    The following lines get us all the way to the writing the cache file problem
    $cred = new Paypal\Auth\OAuthTokenCredential(
        $request['secret']['client_user'], $request['secret']['client_pass'] );
    $config = Paypal\Core\PayPalConfigManager::getInstance()->getConfigHashmap();
    $token = $cred->getAccessToken($config);
    die( $token );
    */

    return $apiContext;
}
