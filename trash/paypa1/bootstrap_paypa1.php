<?php


function bootstrap_paypa1( $request )
{
    global $response;

    // If there are errors, stop processing
    if( isset( $response['errors'] ) )
    {
        return;
    }

    $response['debug'][] = 'bootstrap_paypa1()';

    // Autoload the PayPal PHP SDK Package.
    // Make sure the PayPal PHP SDK has been cloned to the server
    if( file_exists( PROJECT_PATH . 'PayPal-PHP-SDK/autoload.php' ) )
    {
        require_once( PROJECT_PATH . 'PayPal-PHP-SDK/autoload.php' );
        $response['debug'][] = 'PayPal-PHP-SDK autoloaded.';
    }
    else
    {
        $response['errors'][] = 'The PayPal-PHP-SDK/autoload.php is missing.';
        return;
    }

    if( isset(  $request['secret']['email'] ) )
    {
        $request['secret']['business'] = $request['secret']['email'];
        $response['debug'][] = "Added to \$request [ business ] = email";
    }

    // Configure PayPal's API URL here.
    if( $request['secret']['test_mode'] === TRUE )
    {
        // Sandbox
        $request['secret']['mode'] = 'sandbox';
        $request['secret']['paypal_url'] = 'https://api.sandbox.paypal.com/cgi-bin/webscr';
    }
    elseif( $request['secret']['test_mode'] === FALSE )
    {
        // Live Production server uses real money
        $request['secret']['mode'] = 'live';
        $request['secret']['paypal_url'] = 'https://api.paypal.com/';
    }

    $response['debug'][] = "Added to \$request [ mode ] = " . $request['secret']['mode'] . "";
    $response['debug'][] = "Added to \$request [ paypal_url ] = " . $request['secret']['paypal_url'] . "";

    return $request;
}
