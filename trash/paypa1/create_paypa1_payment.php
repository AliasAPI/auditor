<?php


use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Template;
use PayPal\Api\Transaction;


function create_paypa1_payment( $request, $apiContext )
{
    global $response;

    $response['debug'][] = "create_paypa1_payment()";

    $data = array();

    // This code is based on the example at:
    // http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/OrderCreateUsingPayPal.html

    $payer = new Payer();
    $payer->setPaymentMethod("paypal");

    $response['debug'][] = "setPaymentMethod [ paypal ]";

    // Itemized information
    // (Optional) Lets you specify item wise information
    $item1 = new Item();
    $item1->setName( $request['name'] )
        ->setCurrency( $request['currency'] )
        ->setQuantity( 1 )
        ->setSku( $request['sku'] )
        ->setDescription( $request['name'] )
        ->setPrice( $request['amount'] );

    $response['debug'][] = "setSku [ " . $request['sku'] . " ]";
    $response['debug'][] = "setDescription [ " . $request['name'] . " ]";
    $response['debug'][] = "setPrice [ " . $request['amount'] . " ]";
    $response['debug'][] = "setCurrency [ " . $request['currency'] . " ]";

    $itemList = new ItemList();
    $itemList->setItems( array( $item1 ) );

    // Additional payment details
    // Use this optional field to set additional payment information such as tax, shipping charges etc.
    /*
    $details = new Details();
    $details->setShipping(1.2)
        ->setTax(1.3)
        ->setSubtotal(17.50);
    */

    // Amount
    // Lets you specify a payment amount. You can also specify additional details such as shipping, tax.
    $amount = new Amount();
    $amount->setCurrency( $request['currency'] )
        ->setTotal( $request['amount'] );
        // ->setDetails($details);

    // Transaction
    // A transaction defines the contract of a payment - what is the payment for and who is fulfilling it.
    $transaction = new Transaction();
    $transaction->setAmount( $amount )
        ->setItemList( $itemList )
        ->setDescription( $request['name'] )
        ->setInvoiceNumber( uniqid() );

    // Redirect urls
    // Set the urls that the buyer must be redirected to after payment approval/ cancellation.
    // $baseUrl = getBaseUrl(); // this caused an error
    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl( $request['return_url'] )
                 ->setCancelUrl( $request['cancel_url'] );

    $response['debug'][] = "setReturnUrl [ " . $request['return_url'] . " ]";
    $response['debug'][] = "setReturnUrl [ " . $request['cancel_url'] . " ]";

    // Payment
    // A Payment Resource; create one using the above types and intent set to 'order'
    $payment = new Payment();
    $payment->setIntent("sale")
        ->setPayer( $payer )
        ->setRedirectUrls( $redirectUrls )
        ->setTransactions( array( $transaction ) );

    $response['debug'][] = "setIntent [ sale ]";
    // print_r( $payment ); die();

    // For Sample Purposes Only.
    // $request = clone $payment;

    // Create Payment
    // Create a payment by calling the 'create' method passing it a valid apiContext. (See bootstrap.php for more on ApiContext) The return object contains the state and the url to which the buyer must be redirected to for payment approval
    try
    {
        // echo "<pre>"; print_r( $apiContext ); die();
        $result = $payment->create( $apiContext );
        // $response['debug'][] = print_r( $payment, TRUE );

        says( '$result', $result );
    }
    catch( Exception $ex )
    {
        // says('create_paypa1_payment()', $ex );
        // $error = json_decode( $ex );
        // $data = print_r( $error, TRUE );
        $response['errors'][] = $ex;
    }


    try
    {
        // Use PayPal's custom json_encode because they suck
        // $json = $result->toJson();

        // Use the second parameter "TRUE" to get the array right right
        // $data = json_decode( $json, TRUE );

        // Get the transation_id?
        $response['transaction_id'] = $result->getId();

        // Get the token
        $response['token'] = $result->getToken();

        // Get the transaction state
        $response['status'] = $result->getState();

        // Get redirect url
        // The API response provides the url that you must redirect the buyer to.
        // Retrieve the url from the $payment->getApprovalLink() method
        $response['approval_url'] = $result->getApprovalLink();
    }
    catch( Exception $ex )
    {
        // says('create_paypa1_payment()', $ex );
        // $error = json_decode( $ex );
        // $data = print_r( $error, TRUE );
        $response['errors'][] = $ex;
    }
}


?>
