<?php


use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;

/**
 * Executes the PayPal payment transaction
 * @param $request['']
 * @param
 * @param
 */
function execute_paypa1_payment( $request, $apiContext )
{
    // Initialize vars
    global $response;
    $data = array();

    // If there are errors, stop processing
    if( isset( $response['errors'] ) )
    {
        return;
    }

    $response['debug'][] = 'execute_paypa1_payment()';

    // If PayPal has returned the user after they click "Continue"
    if( isset( $request['url_params']['paymentId'] ) && isset( $request['url_params']['PayerID'] ) )
    {
        $response['debug'][] = 'The paymentId and PayerID are in url_params.';

        $payment = Payment::get( $request['url_params']['paymentId'], $apiContext );
        $execute = new PaymentExecution();
        $execute->setPayerId( $request['url_params']['PayerID'] );

        try
        {
            // Execute the payment
            $result = $payment->execute( $execute, $apiContext );

            // Use PayPal's custom json_encode because they suck
            $json = $result->toJson();

            // Use the second parameter "TRUE" to get the array right right
            $data = json_decode( $json, TRUE );
            // echo "<hr>"; print_r( $data );

            // alias_uid_step
            $response['sku'] = $data['transactions'][0]['item_list']['items'][0]['sku'];

            // transaction_id
            $response['transaction_id'] = $data['id'];

            // amount
            $response['amount'] = $data['transactions'][0]['related_resources'][0]['sale']['amount']['details']['subtotal'];
            $response['currency'] = $data['transactions'][0]['related_resources'][0]['sale']['amount']['currency'];

            // payment_status
            $response['status'] = $data['transactions'][0]['related_resources'][0]['sale']['state'];

            // sale_id
            $response['sale_id'] = $data['transactions'][0]['related_resources'][0]['sale']['id'];

            $response['extra']['total_amount'] = $data['transactions'][0]['related_resources'][0]['sale']['amount']['total'];
            $response['extra']['payer'] = $data['payer'];
            $response['extra']['payee'] = $data['transactions'][0]['payee'];
            // echo "<pre>"; echo $json; print_r( $data ); die();
        }
        catch( Exception $ex )
        {
            $error = json_decode( $ex->getData() );
            $data = print_r( $error, TRUE );
            $response['errors'][] = $data;
        }
    }
    else
    {
        $response['debug'][] = "The paymentId and PayerID did not make it in url_params";
        $response['errors'][] = "paymentId = [ " . $request['url_params']['paymentId'] . " ]";
        $response['errors'][] = "PayerID = [ " . $request['url_params']['PayerID'] . " ]";
    }
}
