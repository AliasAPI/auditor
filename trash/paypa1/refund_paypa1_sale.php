<?php

use PayPal\Api\Amount;
use PayPal\Api\Refund;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;

/**
 * Immediately refunds the payer
 * @param $request['amount'] the amount of the sale (or amount being refunded)
 * @param $request['currency'] the currency code (i.e. USD, EUR)
 * @param $request['sale_id'] the id of the completed paymentId / transaction_id
 */
function refund_paypa1_sale( $request, $apiContext )
{
    global $response;

    // If there are errors, stop processing
    if( isset( $response['errors'] ) )
    {
        return;
    }

    $response['debug'][] = 'refund_paypa1_sale()';

    // Initializ vars
    $data = array();

    // Refund amount
    // Includes both the refunded amount (to Payer) and refunded fee (to Payee). Use the $amt->details field to mention fees refund details.
    $amt = new Amount();
    // The $request['currency'] is not included on the return_url
    $amt->setCurrency( $request['currency'] )
        ->setTotal( $request['amount'] );

    // Refund object
    $refundRequest = new RefundRequest();
    $refundRequest->setAmount( $amt );

    // Sale
    // A sale transaction. Create a Sale object with the given sale transaction id.
    $sale = new Sale();
    $sale->setId( $request['sale_id'] );

    try
    {
        $result = $sale->refundSale( $refundRequest, $apiContext );

        // Use PayPal's custom json_encode because they suck
        $json = $result->toJson();

        $data = json_decode( $json, TRUE );
        // echo "<pre>"; print_r( $data );

        // Extra info
        $response['type'] = 'refund';

        // transaction_id
        $response['transaction_id'] = $data['parent_payment'];

        // sale_id
        $response['sale_id'] = $data['sale_id'];

        // refund_id
        $response['refund_id'] = $data['id'];

        // Amount
        $response['amount'] = $data['total_refunded_amount']['value'];

        // refund state (ex: completed)
        $response['status'] = $data['state'];
    }
    catch( \Exception $ex )
    {
        // maybe $ex->getMessage();
        $error = json_decode( $ex->getData() );
        $data = print_r( $error, TRUE );
        $response['errors'][] = print_r( $error, TRUE );
    }
}
