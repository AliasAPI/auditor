<?php


function use_paypa1_sdk( $request )
{
    global $response;

    // If there are errors, stop processing
    if( isset( $response['errors'] ) )
    {
        return;
    }

    $response['debug'][] = 'use_paypa1_sdk()';

    $request = bootstrap_paypa1( $request );

    // Add the final request to the response
    $response['debug']['request'] = $request;

    $apiContext = authorize_paypa1( $request );
    says( 'request', $request, 'response', $response, 'apiContext', $apiContext );

    if( isset( $request['url_params']['paymentId'] ) && isset( $request['url_params']['PayerID'] ) )
    {
        // Get the results after the user pays PayPal
        execute_paypa1_payment( $request, $apiContext );
        says( 'request', $request, 'response', $response, 'apiContext', $apiContext );

        // $test_refund = 'yes';
        // Display output and run the refund while developing
        if( 1 == 2 )
        {
            // execute_paypa1_payment() provides amount, currency, sale_id
            $request = $response;
            refund_paypa1_sale( $request, $apiContext );
        }
    }
    elseif( isset( $request['sale_id'] ) && !empty( $request['sale_id'] )
            && isset( $request['amount'] ) && !empty( $request['amount'] )
            && isset( $request['currency'] ) && !empty( $request['currency'] ) )
    {
        refund_paypa1_sale( $request, $apiContext );
    }
    elseif( isset( $request['sku'] ) )
    {
        create_paypa1_payment( $request, $apiContext );
        says( '$request', $request, '$response', $response, 'apiContext', $apiContext );
    }

    return $response;
}
