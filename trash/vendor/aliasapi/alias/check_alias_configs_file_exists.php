<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

function check_alias_configs_file_exists(string $alias_json_configs_file): void
{
    if (file_exists($alias_json_configs_file)) {
        comment("The \$alias_json_configs_file exists.", 1);
    } else {
        answer(501, ["The \$alias_json_configs_file does not exist."]);
    }
}
