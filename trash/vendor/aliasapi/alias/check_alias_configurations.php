<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

function check_alias_configurations($alias_configurations): void
{
    $json_error = get_json_error();

    // THis needs to be a good error
    if ($json_error) {
        answer(501, ["The JSON alias config file error: $json_error"]);
    }
}
