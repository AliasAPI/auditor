<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

function get_alias_configs_global(): array
{
    global $only_set_in_set_alias_configs_global_function;

    return $only_set_in_set_alias_configs_global_function;
}
