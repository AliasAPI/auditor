<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Alias as Alias;

function pipeline()
{
    $alias_json_configs_file = Alias\configure_alias_configs_file();

    Alias\check_alias_configs_file_exists($alias_json_configs_file);

    $alias_json = \file_get_contents($alias_json_configs_file);

    $alias_configurations = Alias\decode_json($alias_json);

    Alias\check_alias_configurations($alias_configurations);

    Alias\set_alias_configs_global($alias_configurations);

    Alias\check_alias_api_username($alias, $alias_configurations);

    $alias_attributes = Alias\get_alias_attributes($alias);

    Alias\check_alias_attributes($alias_attributes);

    // Alias\check_alias_api_password($api_pass, $alias_attributes);

    return $alias_attributes;
}
