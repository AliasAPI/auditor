<?php

declare(strict_types=1);

namespace AliasAPI\Authorize;

function check_payload(array $payload): void
{
    $json_error = get_json_error();

    // THis needs to be a good error
    if ($json_error) {
        answer(400, ["The JSON curl payload has an error: $json_error"]);
    } elseif (!isset($payload['actionS']) || empty($payload['actionS'])) {
        answer(400, ["The curl payload did not include the actionS command."]);
    } elseif (!isset($payload['ally']) || empty($payload['ally'])) {
        answer(400, ["The curl payload did not include the ally identifier."]);
    } else {
        comment("The payload has the [ " . $payload['actionS'] . " ] actionS.", 1);
    }
}
