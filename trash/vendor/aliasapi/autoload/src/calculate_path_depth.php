<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function calculate_path_depth(string $base_path, string $path): int
{
    // Subtract the base_path to easily count the subdirectories
    $sub_path = str_replace($base_path, '', $path);
    // Add the +1 to start the count at 1 rather than 0 for better understanding
    $path_depth = count(array_filter(explode("/", $sub_path)))+1;

    return $path_depth;
}
