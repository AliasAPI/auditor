<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function configure_autoload_basepath($basepath = '')
{
    if (!defined('AUTOLOAD_BASEPATH')) {
        if ($basepath == '') {
            $basepath = dirname(dirname(dirname(dirname(__DIR__))));
        }

        define('AUTOLOAD_BASEPATH', $basepath);

        return AUTOLOAD_BASEPATH;
    }
}
