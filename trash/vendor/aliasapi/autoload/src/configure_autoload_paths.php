<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function configure_autoload_paths($load_paths = []): array
{
    if (empty($load_paths)) {
        $load_paths = array(
        // Files can be in src/functions because files are globbed recursively.
        AUTOLOAD_BASEPATH . '/packages',
        AUTOLOAD_BASEPATH . '/src/functions',
        );
    }

    return $load_paths;
}
