<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function configure_extra_autoload_files(array $extra_files = []): array
{
    $default_files = array();

    // Include the composer autoloader
    if (file_exists(AUTOLOAD_BASEPATH . '/vendor/autoload.php')) {
        $default_files[] = AUTOLOAD_BASEPATH . '/vendor/autoload.php';
    }

    $extra_files = array_merge($extra_files, $default_files);

    return $extra_files;
}
