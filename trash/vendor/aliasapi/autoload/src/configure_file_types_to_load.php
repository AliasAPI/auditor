<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function configure_file_types_to_load($types = ['*.php']): array
{
    return $types;
}
