<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

use  AliasAPI\Autoload as A;

function glob_files_recursively($start_path, $path = '', $pattern = '*', $max_depth)
{
    $paths = glob($path.'*', GLOB_MARK|GLOB_ONLYDIR);

    $files = glob($path.$pattern);

    foreach ($paths as $path) {
        $path_depth = A\calculate_path_depth($start_path, $path);

        if ($path_depth <= $max_depth) {
            $files = array_merge($files, A\glob_files_recursively($start_path, $path, $pattern, $max_depth));
        }
    }

    return $files;
}
