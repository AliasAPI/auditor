<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

use AliasAPI\Autoload as AL;

function pipeline($basepath = '', $load_paths = [], $extra_files = [], $max_depth)
{
    // Check Everything here.
    
    require_once(__DIR__ . '/require_autoload_files.php');

    AL\require_autoload_files();

    $basepath = AL\configure_autoload_basepath($basepath);

    $load_paths = AL\configure_autoload_paths($load_paths);

    $load_file_types = AL\configure_file_types_to_load($types = ['*.php']);

    $max_depth = AL\configure_recursive_depth($max_depth);

    $load_files = AL\glob_autoload_paths_files($load_paths, $load_file_types, $max_depth);

    $extra_files = AL\configure_extra_autoload_files($extra_files);

    AL\autoload_specified_files($load_files, $extra_files);
}
