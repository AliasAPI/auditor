<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function require_autoload_files(): void
{
    $autoload_files = glob(__DIR__ . '/*.php');

    foreach ($autoload_files as $file) {
        // echo "$file<br>";
        require_once($file);
    }
}
