<?php

namespace AliasAPI\CrudJson;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

function check_file_exists(string $file_name): bool
{
    $exists = false;

    if (\defined('JSON_PATH') && \file_exists(JSON_PATH)) {
        $file_path = JSON_PATH . DIRECTORY_SEPARATOR . $file_name;

        $adapter = new Local('/');

        $filesystem = new Filesystem($adapter);

        $exists = $filesystem->has($file_path);
        // var_dump($exists); die();
    }

    return $exists;
}
