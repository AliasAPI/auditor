<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

function create_json_file($file_name, $data_array): bool
{
    $response = false;

    if (\defined('JSON_PATH') && \file_exists(JSON_PATH)) {
        $file_path = JSON_PATH . '/' . $file_name;

        $adapter = new Local('/');

        $filesystem = new Filesystem($adapter);

        $json = \json_encode($data_array, JSON_PRETTY_PRINT);

        $json = \utf8_encode($json);

        $response = $filesystem->write($file_path, $json);

        return $response;
    }
}
