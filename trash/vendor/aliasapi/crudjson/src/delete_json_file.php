<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

function delete_json_file($file_name): bool
{
    if (\defined('JSON_PATH') && \file_exists(JSON_PATH)) {
        $file_path = JSON_PATH . DIRECTORY_SEPARATOR . $file_name;

        $adapter = new Local('/');

        $filesystem = new Filesystem($adapter);

        $bool = $filesystem->delete($file_path);

        return $bool;
    }
}
