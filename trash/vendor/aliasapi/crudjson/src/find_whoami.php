<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

function find_whoami(): string
{
    if (isset(\posix_getpwuid(\posix_geteuid())['name'])) {
        $whoami = \posix_getpwuid(\posix_geteuid())['name'];
    } elseif (\exec('whoami')) {
        $whoami = \exec('whoami');
    } elseif (\shell_exec('whoami')) {
        $whoami = \shell_exec('whoami');
    }

    return $whoami;
}
