<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

function find_writable_path($path = '')
{
    $writable_path = '';

    if ($path !== '' && \is_writable($path)) {
        $writable_path = $path;
    // Use the app directory to place a data folder
    } elseif (\defined('AUTOLOAD_BASEPATH') !== '' && \is_writable(AUTOLOAD_BASEPATH)) {
        $writable_path = AUTOLOAD_BASEPATH;
    } elseif (\is_writable(\ini_get('upload_tmp_dir'))) {
        // The /opt/lampp/temp/ directory
        $writable_path = \ini_get('upload_tmp_dir');
    } elseif (\is_writable(\sys_get_temp_dir())) {
        // The /tmp directory
        $writable_path = \sys_get_temp_dir();
    } else {
        $whoami = AliasAPI\CrudJson\get_whoami();
        // answer(500, ["User [ $whoami ] cannot write to the filesystem."]);
    }

    if ($writable_path !== '') {
        $writable_path = \rtrim($writable_path, DIRECTORY_SEPARATOR);
        // echo "find_writable_path() $writable_path<br>";
        \define('JSON_PATH', $writable_path);
    }

    return $writable_path;
}
