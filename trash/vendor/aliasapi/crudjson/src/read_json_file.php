<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

function read_json_file($file_name): array
{
    if (\defined('JSON_PATH') && \file_exists(JSON_PATH)) {
        $file_path = JSON_PATH . DIRECTORY_SEPARATOR . $file_name;

        $adapter = new Local('/');

        $filesystem = new Filesystem($adapter);

        $json = $filesystem->read($file_path);

        $data_array = \json_decode($json);

        return $data_array;
    }
}
