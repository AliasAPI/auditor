<?php

declare(strict_types=1);

function write_log(string $json_messages, bool $append = true): void
{
    if (defined('JSON_PATH') && is_writable(JSON_PATH)) {
        $tmp_file = JSON_PATH . DIRECTORY_SEPARATOR . 'debug.json';

        if (!empty($json_messages)) {
            $log = humanize_json($json_messages);
        }

        // Delete / clear the log file
        if (file_exists($tmp_file) && $append == false) {
            unlink($tmp_file);
        }

        $tmp = file_put_contents($tmp_file, $log . PHP_EOL, FILE_APPEND);
    }
}
