<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use RedBeanPHP\Facade as R;

function check_connect_orm(): bool
{
    // Don't use the obvious:  use \RedBeanPHP\R as R;
    $is_connected = R::testConnection();

    return $is_connected;
}
