<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use RedBeanPHP\Facade as R;

function connect_orm(array $database_config)
{
    if (! isset($database_config['dsn'])) {
        sayd($database_config);
    }
    // 2DO+++ Learn how to check to see  if the class is already declared.
    // Then consider using this to declare it globally:
    // class_alias('\RedBeanPHP\Facade', '\R');
    if (!defined("REDBEAN_MODEL_PREFIX")) {
        define('REDBEAN_MODEL_PREFIX', '');
    }

    try {
        // Setup Redbean DB connection and initialize RB for use
        $RedBean = R::setup(
            $database_config['dsn'],
            $database_config['username'],
            $database_config['password'],
            $database_config['frozen'],
            $database_config['partial']
        );

        // Only update the fields of a bean that have been changed rather than the entire bean.
        // https://redbeanphp.com/api/classes/RedBeanPHP.Facade.html#method_usePartialBeans
        // R::usePartialBeans(true);

        // Convert arrays that will be inserted into a column into JSON first
        // https://redbeanphp.com/api/classes/RedBeanPHP.Facade.html#method_useJSONFeatures
        R::useJSONFeatures(true);

        // sayd($RedBean);

        // R::freeze(true);
        // R::debug(false);
        return $RedBean;
    } catch (\Exception $e) {
        $error[] = "connect_orm() There was an error while connecting to the ORM.";
        $error[] = "Exception Type: " . get_class($e) . "";
        $error[] = "Error Message: " . $e->getMessage() . "";
        die($error);
    }
}
