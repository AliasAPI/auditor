<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\Crudtable as CT;
use RedBeanPHP\Facade as R;

function create_row(string $table, array $key_pairs_row)
{
    $binding_string = '';
    $binding_array = array();

    try {
        // Update the table name with RedBean rules.
        $table_name = CT\sanitize_name($table);

        // Build up the bindings . . .
        foreach ($key_pairs_row as $key => $value) {
            // Update the column name with RedBean rules.
            $key = CT\sanitize_name($key);

            // 2DO+++ create bindings???

            // Correct the column names.
            $clean_array[$key] = $value;
        }

        // sayd('table_name', $table_name, 'clean_array', $clean_array);

        // Only insert a new transaction row if 1 does not exist
        $record = R::findOrCreate($table_name, $clean_array);
        // sayd($record);

        return $record;
    } catch (\Exception $e) {
        // CAUTION: Displaying an error here is only visible to the payment gateway
        $error[] = "There was an error while inserting a transaction row.";
        $error[] = "Exception Type: " . get_class($e) . "";
        $error[] = "Error Message: " . $e->getMessage() . "";
        // answer(500, [$error]);
    }
}
