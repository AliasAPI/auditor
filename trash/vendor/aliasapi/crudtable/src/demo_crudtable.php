<?php

declare(strict_types=1);

namespace AliasAPI\Omnipay;

use AliasAPI\Autoload as A;
// use AliasAPI\CrudJson as F;
use AliasAPI\CrudTable as CT;

// demo_crud_table();

function demo_crud_table()
{
    require_once('vendor/aliasapi/autoload/src/pipeline.php');
    $base_path = '/opt/lampp/htdocs/omnipay/vendor/aliasapi/crudtable';
    $extra_files = [
    '/opt/lampp/htdocs/omnipay/vendor/autoload.php',
    '/opt/lampp/htdocs/omnipay/say.php'
    ];
    A\pipeline($base_path, $load_paths = [$base_path], $extra_files);

    $database_config['dsn'] = "mysql:host=localhost;dbname=test";
    $database_config['username'] = "root";
    $database_config['password'] = "";
    $database_config['frozen'] = false;
    $database_config['partial'] = true;

    $RedBean = CT\connect_orm($database_config);

    $row['alias'] = 'testalias2';
    $row['user'] = 'user2';
    $row['cart'] = 'CART';
    $row['tag'] = 'TAG1';

    $row['type'] = 'sale';
    $row['transactionid'] = 'TRANASACTIONID';
    $row['amount'] = '10.00';
    $row['currency'] = 'USD';
    $row['fee'] = '0.10';

    $row['status'] = 'completed';
    $row['saleid'] = '';
    $row['refundid'] = '';
    $row['created'] = '2019-04-07 13:00:00';
    $row['updated'] = '2019-04-07 13:00:02';
    $row['redirect_url'] = '';

    $really_bad_tablename = "16:13t()RANs-AC_Tions";
    $record = CT\create_row($really_bad_tablename, $row);

    $key_pairs_array = array('tag' => 'TAG1' );
    $read_rows = CT\read_rows($really_bad_tablename, $key_pairs_array);

    $update_pairs_array = array('saleid' => 'UPDATED_SALE_ID');
    $updated_id = CT\update_rows($really_bad_tablename, $key_pairs_array, $update_pairs_array);

    $key_pairs_array = ['saleid' => 'UPDATED_SALE_ID'];
    $deleted_id = CT\delete_rows($really_bad_tablename, $key_pairs_array);

    sayd($updated_id);
}
