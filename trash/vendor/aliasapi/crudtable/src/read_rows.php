<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\Crudtable as CT;
use RedBeanPHP\Facade as R;

function read_rows(string $table, array $key_pairs_array): array
{
    $binding_string = '';
    $binding_array = array();

    try {
        // Update the table name with RedBean rules.
        $table_name = CT\sanitize_name($table);
        // print_r($key_pairs_array);
        // Build up the bindings . . .
        foreach ($key_pairs_array as $column => $value) {
            // Update the column name with RedBean rules.
            $column = CT\sanitize_name($column);

            $bind = ':' . $column;
            $binding_string .= "AND $column = $bind ";
            $binding_array["$bind"] = $value;
        }

        // Remove beginning or ending "AND"
        $binding_string = trim($binding_string, "AND ");

        $prepared_query = "SELECT * FROM $table_name WHERE $binding_string";

        // sayd('column', $column, 'bind', $bind, 'query_array', $query_array, $query, 'query', 'bind_array', $bind_array);
        $read_rows = (array) R::getAll(
            $prepared_query,
            $binding_array
        );

        return $read_rows;
    } catch (\Exception $e) {
        // CAUTION: Displaying an error here is only visible to the payment gateway
        $error[] = "There was an error while reading rows.\n";
        $error[] = "Exception Type: " . get_class($e) . "\n";
        $error[] = "Error Message: " . $e->getMessage() . "\n";
        // answer(500, [$error]);\
        // 2DO+++ figure out how to handle database errors
        echo $error;
        die();
    }
}
