<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

function sanitize_name(string $name): string
{
    // CAUTION: Do not use column names ending in _id or ID.
    // Redbean uses _id and will change the nature of the column.
    // Somehow it overwrites ID and changes it to _id as well.
    // $payload['alias'], $payload['user'], $payload['cart'],

    // The bean name MUST be lowercase
    $name = strtolower($name);

    // Remove the special characters for Redbean.
    $name = preg_replace('/[^a-z0-9]+/', '', $name);

    // Make sure the table name starts with a letter
    $name = preg_replace("/^[^a-z]+/", '', $name);

    return $name;
}
