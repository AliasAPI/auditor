<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Builder;
use ParagonIE\Paseto\Purpose;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\Version2\AsymmetricSecretKey;
use ParagonIE\Paseto\Protocol\Version2;

// $expiration_interval examples "+1 day", "+2 hours", "+10 minutes", "+3600 seconds"
function build_paseto_token(string $local_private_key, string $expiration_interval, array $extra_claims): string
{
    echo "build_paseto_token()<br>";

    try {
        $local_private_key_bin = Base64UrlSafe::decode($local_private_key);
        $local_private_key_obj = new AsymmetricSecretKey($local_private_key_bin);

        // Converts automatically to a string
        $paseto_token_obj = (new Builder())
            // Use the private_key to prove this server set the token
            ->setKey($local_private_key_obj)
            // Only use Version 2
            ->setVersion(new Version2())
            // Public keys are for authentication and authorization
            ->setPurpose(Purpose::public())
            // Set it to expire in one day
            // 2DO+++ add date add functionality
            // http://php.net/manual/en/dateinterval.createfromdatestring.php
            ->setExpiration(new DateTime($expiration_interval))
            // Store arbitrary data
            ->setClaims($extra_claims);

        $paseto_token = (string) $paseto_token_obj;

        echo "paseto_token [ $paseto_token ]<hr>";

        return $paseto_token;
    } catch (PasetoException $ex) {
        echo "build_paseto_token() " . $ex->getMessage();
        // 2DO+++ answer(400, ["$ex->getMessage()"]);
        die();
    }
}
