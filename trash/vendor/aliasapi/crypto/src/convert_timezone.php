<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

function convert_timezone($datetime_stamp, $from = 'UTC', $to = 'America/New_York', $format = 'Y-m-d H:i:s'): string
{
    // $datetime_stamp = "2019-03-23T08:09:27Z"; convert_timezone($datetime_stamp);
    echo "convert_timezone()<br>";

    try {
        if ($datetime_stamp === null) {
            $datetime_stamp = (new DateTime())->format('Y-m-d H:i:s');
        }

        $dateTime = new DateTime($datetime_stamp, new DateTimeZone($from));

        $dateTime->setTimezone(new DateTimeZone($to));

        echo "converted datetime [ " . $dateTime->format($format) . " ]<hr>";

        return $dateTime->format($format);
    } catch (Exception $ex) {
        return '';
    }
}
