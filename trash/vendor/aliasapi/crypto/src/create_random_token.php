<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

function create_random_token(int $length = 32, $insecure = ''): string
{
    if ($insecure) {
        // Define a character set that is easily communicated
        $characters = "2345679FHKQRSUXY";

        $token = '';

        // Count the characters in the set
        $characterslength = strlen($characters)-1;

        // Loop 6 times to get 6 random characters for $login
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, $characterslength)];
        }

        return $token;
    }

    if (!isset($length) || intval($length) <= 8) {
        $length = 32;
    }

    if (function_exists('random_bytes')) {
        return bin2hex(random_bytes($length));
    }

    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    }

    if (function_exists('openssl_random_pseudo_bytes')) {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}
