<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Keys\Version2\SymmetricKey;
use ParagonIE\Paseto\Protocol\Version2;

function derive_paseto_shared_key($public_key_1, $public_key_2): string
{
    echo "derive_shared_key()<br>";
    // The sequence of keys matters in calculating the resulting shared_key.
    // Always use the public key from the request first.
    // Server1 sends its public_key to Server2.
    // Use Server1's public_key as the first attribute here;.
    // No . . . Put the keys in an array . . .
    $keys = array($public_key_1, $public_key_2);
    // print_r($keys); echo "<br>";

    // Sort keys in alphabetical order to reduce human error.
    sort($keys);
    // print_r($keys);

    $combined_keys = $keys[0].$keys[1];

    $string_length = strlen($combined_keys);

    echo "combined keys string_length [ $string_length ]<br>";

    $key_length = Version2::getSymmetricKeyByteLength();

    echo "required key_length [ $key_length ]<br>";

    $excess_character_count = $string_length - $key_length;

    echo "$string_length total - $key_length required = $excess_character_count excess.<br>";

    $half_excess_count = (int) floor($excess_character_count / 2);

    echo "$excess_character_count excess / 2 = $half_excess_count offset<br>";

    echo "Get $key_length characters starting from the $half_excess_count character.<br>";

    $middle_section = mb_strimwidth($combined_keys, $half_excess_count, $key_length);

    echo "Using the middle section of the two keys to create a shared symmetric key<br>";

    $shared_key_obj = new SymmetricKey($middle_section);

    $shared_key = Base64UrlSafe::encode($shared_key_obj->raw());

    echo "shared_key [ $shared_key ]<hr>";

    return $shared_key;
}
