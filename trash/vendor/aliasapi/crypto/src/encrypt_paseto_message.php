<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\Version2\SymmetricKey;
use ParagonIE\Paseto\Protocol\Version2;

function encrypt_paseto_message(string $message, string $shared_key): string
{
    echo "encrypt_paseto_message()<br>";

    try {
        $shared_key_bin = Base64UrlSafe::decode($shared_key);
        $shared_key_obj = new SymmetricKey($shared_key_bin);

        $encrypted_message = Version2::encrypt($message, $shared_key_obj);

        echo "encrypted_message [ $encrypted_message ]<hr>";

        return $encrypted_message;
    } catch (PasetoException $ex) {
        echo "encrypt_paseto_message() " . $ex->getMessage();
        // 2DO+++ answer(400, ["$ex->getMessage()"]);
        die();
    }
}
