<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

function set_paseto_token_claims(): array
{
    // 2DO+++ Implement more detailed token claims
    // https://tools.ietf.org/html/rfc7519#page-10

    $claims = array();
    // Set the 'aud' claim. Audience
    $claims['aud'] = '';
    // Set the 'exp' claim. Expiration
    $claims['exp'] = '';
    // Get the 'jti' claim. JWT ID (to help prevent replay)
    $claims['jti'] = '';
    // Set the 'iat' claim. IssuedAt
    $claims['iat'] = '';
    // Set the 'iss' claim. Issuer
    $claims['iss'] = '';
    // Get the 'nbf' claim. Not Before
    $claims['nbf'] = '';
    // Get the 'sub' claim. Subject
    $claims['sub'] = '';

    return $claims;
}
