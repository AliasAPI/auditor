<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Keys\Version2\AsymmetricSecretKey;
use ParagonIE\Paseto\Protocol\Version2;

// This prepares a signed message to be transmitted to a remote server
function sign_paseto_message(array $message, string $local_private_key): string
{
    echo "sign_paseto_message()<br>";

    $local_private_key_bin = Base64UrlSafe::decode($local_private_key);
    $local_private_key_obj = new AsymmetricSecretKey($local_private_key_bin);

    $json_message = \json_encode($message, JSON_PRETTY_PRINT);

    echo "json_message [ $json_message ]<br>";

    try {
        $signed_message = Version2::sign($json_message, $local_private_key_obj);

        echo "signed_message [ $signed_message ]<hr>";

        return $signed_message;
    } catch (PasetoException $ex) {
        echo "sign_paseto_message() " . $ex->getMessage();
        // 2DO+++ answer(400, ["$ex->getMessage()"]);
        die();
    }
}
