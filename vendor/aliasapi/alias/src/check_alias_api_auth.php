<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function check_alias_api_auth(array $request, $alias_attributes): void
{
    $actionS = '';
    
    if (isset($request['tag_file']['actionS'])) {
        $actionS = $request['tag_file']['actionS'];
    } elseif (isset($request['body']['actionS'])) {
        $actionS = $request['body']['actionS'];
    }

    if ($actionS && ! in_array($actionS, $alias_attributes['authorized_actions'])) {
        Messages\respond(400, ["The alias is not authorized to perform the actionS."]);
    }
}
