<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function check_alias_api_pass(array $request, array $alias_attributes): void
{
    $api_pass = '';

    if (! isset($request['tag_file']) && isset($request['headers']['auth'][0])) {
        $user_pass = explode(',', $request['headers']['auth'][0]);
        $api_pass = preg_replace("/[^a-zA-Z0-9]/", "", $user_pass[1]);
    }

    if ($api_pass !== $alias_attributes['api_keys']['api_pass']) {
        Messages\respond(400, ["The api_pass is incorrect. Remember to exclude characters."]);
    }
}
