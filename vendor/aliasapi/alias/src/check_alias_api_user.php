<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function check_alias_api_user(string $alias, array $alias_configurations): void
{
    if (!$alias) {
        Messages\respond(400, ["The alias is not set. The request setting or tag file is missing."]);
    } elseif (! array_key_exists($alias, $alias_configurations)) {
        Messages\respond(400, ["The alias is not found in the alias_configs_file."]);
    }
}
