<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function check_alias_attributes(array $attributes): void
{
    if (empty($attributes)) {
        Messages\respond(501, ["The Alias has no attributes."]);
    }

    if (!isset($attributes['alias'])) {
        Messages\respond(501, ["The Alias alias has not been configured."]);
    }

    if (!isset($attributes['api_keys']['api_pass'])) {
        Messages\respond(501, ["The Alias api_pass has not been configured."]);
    }

    if (!isset($attributes['api_keys']['salt'])) {
        Messages\respond(501, ["The Alias salt has not been configured."]);
    }

    if (!isset($attributes['authorized_actions']) || empty($attributes['authorized_actions'])) {
        Messages\respond(501, ["The Alias has no authorized API actions configured."]);
    }

    if (!isset($attributes['debug_config'])) {
        Messages\respond(501, ["The Alias debug_config has not been configured correctly."]);
    }

    if (!isset($attributes['debug_config']['detail_level']) || !is_numeric($attributes['debug_config']['detail_level'])) {
        Messages\respond(501, ["The Alias debug_config detail_level has not been configured correctly."]);
    }

    if (isset($attributes['database'])) {
        if (!isset($attributes['database']['dsn']) || empty($attributes['database']['dsn'])) {
            Messages\respond(501, ["The database [ dsn ] has not been configured correctly."]);
        }

        if (!isset($attributes['database']['username']) || empty($attributes['database']['username'])) {
            Messages\respond(501, ["The database [ username ] has not been configured correctly."]);
        }

        if (!isset($attributes['database']['password'])
        || ($attributes['test_mode'] !== true && empty($attributes['database']['password']))) {
            Messages\respond(501, ["The database [ password ] has not been configured correctly."]);
        }
    }
}
