<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function check_alias_configs_file_exists(string $alias_json_configs_file): void
{
    if (\file_exists($alias_json_configs_file)) {
        // 2DO+++ replace this
        echo "";
    } else {
        Messages\respond(501, ["The alias json configurations file does not exist."]);
    }
}
