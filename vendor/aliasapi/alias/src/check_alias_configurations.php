<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function check_alias_configurations($alias_configurations): void
{
    $json_error = get_json_error();

    // This needs to be a good error
    if ($json_error) {
        Messages\respond(501, ["The JSON alias config file error: $json_error"]);
    }
}
