<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

function configure_alias_configs_file(): string
{
    // 2DO+++ Add dev path for .alias.json AFTER the unit tests are written.
    // NOTE Chances are, the alias.json will change throughout development
    if (defined('BASEPATH')) {
        return BASEPATH . '/config/.alias.json';
    } else {
        Messages\respond(501, ["The BASEPATH constant is not defined."]);
    }
}
