<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function decode_alias_configurations($alias_json)
{
    $alias_configurations = (array) \json_decode($alias_json, true);

    if (empty($alias_configurations)) {
        Messages\respond(500, ["The alias configurations file cannot be decoded."]);
    }

    return $alias_configurations;
}
