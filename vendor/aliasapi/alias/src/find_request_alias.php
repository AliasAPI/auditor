<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function find_request_alias(array $request): string
{
    $alias = '';

    if (isset($request['tag_file']['alias'])) {
        $alias = $request['tag_file']['alias'];
    } elseif (isset($request['headers']['auth'][0])) {
        // sayd($request['headers']['auth'][0], 'here');
        $user_pass = [];

        $user_pass = explode(',', $request['headers']['auth'][0]);

        $alias = preg_replace("/[^a-zA-Z0-9]/", "", $user_pass[0]);
    } elseif (isset($request['params']['ally']['alias'])) {
        $alias = $request['params']['ally']['alias'];
    } elseif (isset($request['params']['alias'])) {
        $alias = $request['params']['alias'];
    } elseif (isset($request['alias'])) {
        $alias = $request['alias'];
    }

    if ($alias === '') {
        sayd($request);
        Messages\respond(400, ["There is no alias set in the request."]);
    }

    return $alias;
}
