<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function get_alias_attributes(string $alias, array $alias_configs): ?array
{
    if (array_key_exists($alias, $alias_configs)) {
        $alias_attributes = (array) $alias_configs[$alias];

        return $alias_attributes;
    } else {
        Messages\respond(501, ["The Alias is not found in the alias_configs."]);
    }
}
