<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function load_alias_configs_file($alias_configs_file)
{
    $alias_json = \file_get_contents($alias_configs_file);

    if ($alias_json === false) {
        Messages\respond(501, ["Could not load the alias configurations file."]);
    }

    return $alias_json;
}
