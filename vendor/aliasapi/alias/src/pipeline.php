<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Alias as Alias;

function pipeline(array $request): array
{
    $alias = Alias\find_request_alias($request);

    $alias_json_configs_file = Alias\configure_alias_configs_file();

    Alias\check_alias_configs_file_exists($alias_json_configs_file);

    $alias_json = load_alias_configs_file($alias_json_configs_file);

    // 2DO+++ Consider encrypting the .alias.json file
    // $alias_json = decrypt_alias_configs_file($alias_json_file);

    $alias_configurations = (array) \json_decode($alias_json, true);

    Alias\check_alias_configurations($alias_configurations);

    Alias\set_alias_configs_global($alias_configurations);

    Alias\check_alias_api_user($alias, $alias_configurations);

    $alias_configs = Alias\get_alias_configs_global();

    $alias_attributes = Alias\get_alias_attributes($alias, $alias_configs);

    Alias\check_alias_attributes($alias_attributes);

    Alias\check_alias_api_pass($request, $alias_attributes);

    Alias\check_alias_api_auth($request, $alias_attributes);

    Alias\set_alias_salt_constant($alias_attributes);

    return $alias_attributes;
}
