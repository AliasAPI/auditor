<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages as Messages;

function set_alias_configs_global(array $alias_configs): void
{
    global $only_set_in_set_alias_configs_global_function;

    if (!isset($only_set_in_set_alias_configs_global_function) && !empty($alias_configs)) {
        $only_set_in_set_alias_configs_global_function = (array) $alias_configs;
    } else {
        Messages\respond(501, ["The alias_configurations array is empty."]);
    }
}
