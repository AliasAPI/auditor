<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

function set_alias_salt_constant(array $alias_attributes): void
{
    if (isset($alias_attributes['api_keys']['salt']) && ! \defined('ALIAS_SALT')) {
        define('ALIAS_SALT', $alias_attributes['api_keys']['salt']);
    }
}
