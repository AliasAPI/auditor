<?php

declare(strict_types=1);

namespace AliasAPI\Api;

use AliasAPI\Auditor as Auditor;

function call_audit_account(array $request): void
{
    if (is_callable('AliasAPI\Auditor\audit_account')) {
        Auditor\audit_account($request);
    }
}
