<?php

declare(strict_types=1);

namespace AliasAPI\Api;

use AliasAPI\Auditor as Auditor;

function call_capture_captcha(array $request): void
{
    if (is_callable('AliasAPI\Auditor\capture_captcha')) {
        Auditor\capture_captcha($request);
    }
}
