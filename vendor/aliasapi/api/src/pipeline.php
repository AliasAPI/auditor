<?php

declare(strict_types=1);

namespace AliasAPI\Api;

use AliasAPI\Api as API;
use AliasAPI\Alias as Alias;
use AliasAPI\Autoload as Autoload;
use AliasAPI\CrudJson as CrudJson;
use AliasAPI\CrudPair as CrudPair;
use AliasAPI\Messages as Messages;

// use AliasAPI\Steps as Steps;
// use AliasAPI\Users as Users;

function pipeline($set)
{
    Autoload\require_autoload_files();

    $basepath = Autoload\configure_autoload_basepath($set['basepath']);

    $load_paths = Autoload\configure_autoload_paths($set['load_paths']);

    $load_file_types = Autoload\configure_file_types_to_load($set['load_types']);

    $max_depth = Autoload\configure_recursive_depth($set['max_depth']);

    $load_files = Autoload\glob_autoload_paths_files($load_paths, $load_file_types, $max_depth);

    $extra_files = Autoload\configure_extra_autoload_files($set['extra_files']);

    Autoload\autoload_specified_files($load_files, $extra_files);


    API\set_domains();

    CrudJson\define_json_path(true, $set['json_path']);


    $request = Messages\get_server_request();

    $tag = CrudJson\find_request_tag($request);

    $request = CrudJson\open_tag_file($request, $tag);

    Messages\check_server_request($request);


    $alias = Alias\find_request_alias($request);

    $alias_configs_file = Alias\configure_alias_configs_file();

    Alias\check_alias_configs_file_exists($alias_configs_file);

    $alias_file = Alias\load_alias_configs_file($alias_configs_file);

    // 2DO+++ Consider encrypting the .alias.json file
    // $alias_json = decrypt_alias_configs_file($alias_json_file);

    $alias_configurations = Alias\decode_alias_configurations($alias_file);

    Alias\check_alias_configurations($alias_configurations);

    Alias\set_alias_configs_global($alias_configurations);

    Alias\check_alias_api_user($alias, $alias_configurations);

    $alias_configs = Alias\get_alias_configs_global();

    $alias_attributes = Alias\get_alias_attributes($alias, $alias_configs);

    Alias\check_alias_attributes($alias_attributes);

    // Alias\check_alias_api_pass($request, $alias_attributes);

    Alias\check_alias_api_auth($request, $alias_attributes);


    CrudPair\check_pair_requirements($request, $alias_attributes);

    $pair_file_path = CrudPair\set_pair_file_path($request, $alias_attributes);

    CrudPair\delete_pair_file($request, $pair_file_path);

    CrudPair\create_pair_file($request, $alias_attributes, $pair_file_path);

    CrudPair\update_pair_file($request, $alias_attributes, $pair_file_path);

    $pair_file_array = CrudPair\read_pair_file($pair_file_path);


    // $payload = decode_json($payload_json);

    // check_payload($payload, $alias_attributes);

    // check_alias_api_actions($payload, $alias_attributes);

    // sayd($request);

    // put parameters into $request

    // sayd('alias', $alias, 'api_pass', $api_pass, 'access_token', $access_token);
    // sayd('actionS', $payload['actionS'], 'payload', $payload);


    // Process Payments and Refunds ONLY IF AliasAPI\Money is installed
    // if (!isset($request['body']['tag']) && !isset($request['params']['tag'])) {
    //     respond(400, ["The tag is not set in the server request."]);
    // }
    // $response = API\call_pipeline_create_purchase($request);

    // API\call_pipeline_cancel_purchase($request);

    // API\call_pipeline_complete_purchase($request);

    // API\call_pipeline_refund_purchase($request);

    // API\call_pipeline_update_transaction($request);


    // Process Account Auditing ONLY IF AliasAPI\Auditor is installed
    // API\call_audit_account($request);

    // API\call_capture_captcha($request);


    // Convert response to json, encrypt the message, and emit the response
    Messages\respond(400, ["The request made it to the end of the API pipeline."]);
}
