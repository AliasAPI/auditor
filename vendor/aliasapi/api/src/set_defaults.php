<?php

declare(strict_types=1);

namespace AliasAPI\Api;

function set_defaults()
{
    $defaults = [];

    \date_default_timezone_set('UTC');

    if (\session_status() == PHP_SESSION_NONE && !\headers_sent()) {
        \session_start();
    }

    \defined('BASEPATH') || \define('BASEPATH', \realpath(\dirname(__FILE__) . '/../../..'));

    require_once(BASEPATH . '/vendor/aliasapi/autoload/src/require_autoload_files.php');
    require_once(BASEPATH . '/vendor/aliasapi/autoload/src/pipeline.php');
    require_once(BASEPATH . '/vendor/aliasapi/api/src/pipeline.php');
    require_once(BASEPATH . '/vendor/aliasapi/client/src/pipeline.php');

    // 2DO+++ Turning off error messages fails; Please fix it.
    \ini_set('display_errors', 'Off');
    \ini_set('display_startup_errors', 'Off');
    \error_reporting(~E_ALL);

    $defaults['basepath'] = BASEPATH;
    $defaults['load_paths'] = [];
    $defaults['load_types'] = ['*.php'];
    $defaults['max_depth'] = 3;
    $defaults['extra_files'] = [];
    $defaults['json_path'] = BASEPATH . '/jsondata';

    return $defaults;
}
