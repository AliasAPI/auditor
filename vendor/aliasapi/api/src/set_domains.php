<?php

declare(strict_types=1);

namespace AliasAPI\Api;

function set_domains()
{
    if (defined('BASEPATH')) {
        if (file_exists(BASEPATH . '/config/.domains.json')) {
            $domains_json = file_get_contents(BASEPATH . '/config/.domains.json');
            $domains = (array) json_decode($domains_json, true);

            foreach ($domains as $locale => $apis) {
                // Note: stripos returns false or 0 as the position in the string
                if (stripos(BASEPATH, $locale) !== false) {
                    \defined('AUDITOR') || \define('AUDITOR', $apis['auditor']);
                    \defined('MONEY') || \define('MONEY', $apis['money']);
                    \defined('STEPS') || \define('STEPS', $apis['steps']);
                    \defined('USERS') || \define('USERS', $apis['users']);
                    \defined('VIEWS') || \define('VIEWS', $apis['views']);
                    \defined('WEBSITE') || \define('WEBSITE', $apis['website']);
                }
            }
        }
    }
}
