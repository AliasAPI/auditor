NOTE TO SELF:
    RETURN ERROR MESSAGES FROM AUDITOR
    The Steps server should create the Register Step (with Sponsor URL)
    And Steps server should handle CRUD because it's all just a step




Example public/index.php file:




I have already set up a web server for you.
It is located at:
    https://www.000webhost.com
    https://viewsfolder.000webhostapp.com/auditor/public/index.php
    I will send you the username and password on another channel.

It will host the code for AliasAPI which receives and sends the JSON response.

This is it:
. The user enters the username, password, and captcha.
. We send the 3 fields to AliasAPI/Auditor (auditor.class.php).
. The response specified on line 60 of auditor.class.php is returned as JSON.

I am always available on Google Hangouts at drewbrownjr@gmail.com .


REQUEST:

{
    "tag":"TESTTAG",
    "actionS":"capture captcha",
    "ally": {
        "alias":"Auditor",
        "client":"StepsTEAM"
    }
}

RESPONSE:

{
    "tag":"TAG1",
    "data":"Website Data with the Captcha so the user can enter it"
}



REQUEST:

{
    "tag":"TAG2",
    "actionS":"audit account",
    "ally": {
        "alias":"Auditor",
        "client":"StepsTEAM"
    },

    "username":"accountUsername",
    "password":"accountPassword",
    "captcha":"displayedCaptcha"
}


RESPONSE:

{
    "tag":"TAG2",
    "sponsor":"ReturnDataSponsor",
    "rank":"ReturnDataRank",
    "placement":"ReturnDataPlacement",
    "kyc":"ReturnDataKYC",
    "purchase":"ReturnDataPurchase"
}
