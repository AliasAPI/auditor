<?php


function audit_karatbars_status( $session, $status, $step )
{
    /*
     * array
     *  'sponsor' => string 'test'
     *  'rank' => string 'Distributor'
     *  'placement' => string 'automatic'
     *  'kyc' => string 'Not Uploaded'
     *  'purchase' => string 'No'
     */

    $error = NULL;

    if( isset( $status['error'] ) && !empty( $status['error'] ) )
    {
        return $status['error'];
    }

    // register (and all other steps) should audit the geneology
    if( strtolower( $status[ 'sponsor' ] ) !== strtolower( $session[ 'sponsor' ] ) )
    {
        $error = "Your Karatbars sponsor is [ " . $status[ 'sponsor' ] . " ] and it should be [ " . $session[ 'sponsor' ] . " ]";
        return $error;
    }

    if( $step === 'upgrade-to-bronze' )
    {
        if( !in_array( $status['package'], array('Bronze', 'Silver', 'Gold', 'VIP', 'Premium'), TRUE ) )
        {
            $error = "You need to purchase a Bronze (or higher) package from Karatbars.";
            return $error;
        }
    }

    if( $step === 'send-docs' )
    {
        if( $status['kyc'] !== "Approved" )
        {
            $error = "Your KYC documents are not approved yet.";
            return $error;
        }
    }

    // commission-card ???

    if( $step === 'upgrade-to-silver' )
    {
        if( !in_array( $status['package'], array('Silver', 'Gold', 'VIP', 'Premium'), TRUE ) )
        {
            $error = "You need to purchase a Silver (or higher) package from Karatbars.";
            return $error;
        }
    }


    if( $step === 'purchase-product' )
    {
        if( $status['purchase'] !== "Yes" )
        {
            $error = "You need to purchase gold on the auto exchange.";
            return $error;
        }
    }


    if( $step === 'upgrade-to-gold' )
    {
        if( !in_array( $status['package'], array('Gold', 'VIP', 'Premium'), TRUE ) )
        {
            $error = "You need to purchase a Gold (or higher) package from Karatbars.";
            return $error;
        }
    }


    if( $step === 'upgrade-to-vip' )
    {
        if( !in_array( $status['package'], array('VIP', 'Premium'), TRUE ) )
        {
            $error = "You need to purchase a VIP (or higher) package from Karatbars.";
            return $error;
        }
    }


    return $error;
}


?>
