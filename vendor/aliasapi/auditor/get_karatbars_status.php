<?php


function get_karatbars_status( $username, $password )
{
    /*
     * returned array
     *    'Sponsor' => string 'test'
     *    'Rank' => string 'Distributor'
     *    'Placement' => string 'automatic'
     *    'KYC' => string 'Not Uploaded'
     *    'Purchase' => string 'No'
     */

    if( file_exists( BASEPATH . 'auditor/auditor.class.php' ) && file_exists( BASEPATH . 'auditor/simple_html_dom.php' ) )
    {
        require_once(BASEPATH . 'auditor/auditor.class.php');
        require_once(BASEPATH . 'auditor/simple_html_dom.php');

        $Auditor = new Auditor( $username, $password );

        return array_change_key_case( $Auditor->ReturnData, CASE_LOWER );
    }
    else
    {
        echo "Did you include the Auditor code, Drew?";
    }
}


?>