<?php



function register($post, $session, $step)
{
    if (isset($post[ 'action' ]) && $post[ 'action' ] !== $step) {
        // Display the success message for the previous step
        display_view($post[ 'action' ] . '-success.html');
        display_view('footer.html');
        die();
    } elseif (!isset($post[ 'action' ])) {
        // Select and save a sponsor for the user
        $assigned_sponsor = assign_sponsor($session['userid']);

        // echo "<center>Your sponsor is [ " . $session[ 'sponsor' ] . " ]<br><br>";
        echo "<a href=http://karatbars.com/?page=signup&s=" . $assigned_sponsor . " target=_blank>";

        // Display the front end images for the step.
        display_view($step . '.html');

        echo "</a>";
    } else {
        // If they have clicked the register button and there are no errors
        if (isset($post[ 'action' ]) && $post[ 'action' ] == "register") {

            // If there is no username entered
            if (!isset($post[ 'username' ]) || empty($post[ 'username' ])) {
                $error = "Please enter the new Karatbars username (that you got during registration).";
            } else {
                // Log into the user's account and get the key attributes
                $status = get_karatbars_status($post[ 'username' ], $post[ 'password' ]);

                if (isset($status['sponsor']) && $status['sponsor'] !== '') {
                    // Inherit the sponsor from Karatbars during registration
                    // This allows any Karatbars affiliate to use our system
                    $session[ 'sponsor' ] = $status[ 'sponsor' ];

                    // Update the sponsor based on the Karatbars return
                    update_sponsor($session[ 'userid' ], $status[ 'sponsor' ]);
                }

                $error = audit_karatbars_status($session, $status, 'register');
            }

            // If there's not No error
            if ($error !== null) {
                $_POST[ 'register_error' ] = $error;
                echo "<br><br><br><br><br><br>";
            } elseif (isset($post[ 'username' ]) && !empty($post[ 'username' ])
                && isset($status['sponsor']) && !empty($status['sponsor'])) {
                // Update the username for the user
                query("UPDATE `users` SET
                        `username` = '" . $post[ 'username' ] . "'
                        WHERE `userid` = '" . $session[ 'userid' ] . "'
                        LIMIT 1 ");

                // Record the step
                insert_user_step($session[ 'userid' ], 'register', '');

                // Display the success message
                // display_view('register-success.html');

                step_up();
            }
        }
    }
}
