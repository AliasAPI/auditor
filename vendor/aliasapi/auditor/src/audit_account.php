<?php

declare(strict_types=1);

namespace AliasAPI\Auditor;

use AliasAPI\Messages as Messages;

function audit_account(array $request): void
{
    if (isset($request['actionS']) && $requst['actionS'] == 'audit account') {
        $username = $request['body']['username'];
        $password = $request['body']['password'];
        $captcha = $request['body']['captcha'];

        $result = new Auditor($username, $password, $captcha);

        // If there is an error, return it
        $res['error'] = $result->Error;
        $res['tag'] = $request['tag'];
        $res['sponsor'] = $result['sponsor'];
        $res['rank'] = $result['rank'];
        $res['placement'] = $result['placement'];
        $res['kyc'] = $result['kyc'];
        $res['purchase'] = $result['purchase'];

        Messages\respond($http_status_code, $res);
    }
}
