<?php

namespace AliasAPI\Auditor;

class Auditor
{
    public function __construct($Username, $Password, $Captcha)
    {
        $CH = curl_init();
        $HTML = new simple_html_dom();
        $OptionHTML = new simple_html_dom();

        // Assign Login Information Data
        $this->Username = $Username;
        $this->Password = $Password;
        $this->Captcha = $Captcha;

        if ($this->LogIN($CH) == true) {
            $BinaryTree = $this->ScrapData($CH, $this->BinaryTreeURL);
            $HTML->load($BinaryTree);
            $ReturnData[ "Sponsor" ] = $HTML->find(".row", 3)->children(0)->children(0)->children(1)->children(0)->children(1);
            $ReturnData[ "Sponsor" ] = strip_tags($ReturnData[ "Sponsor" ]);

            $ReturnData[ "Rank" ] = $HTML->find(".row", 3)->children(0)->children(0)->children(1)->children(5)->children(1);
            $ReturnData[ "Rank" ] = strip_tags($ReturnData[ "Rank" ]);

            $ReturnData[ "Placement" ] = $HTML->getElementById("binary_option");
            $ReturnData[ "Placement" ] = strip_tags($ReturnData[ "Placement" ], "<option>");
            $OptionHTML->load($ReturnData[ "Placement" ]);
            foreach ($OptionHTML->find('option') as $Element) {
                if (strpos($Element, 'selected="selected"') == true) {
                    $ReturnData[ "Placement" ] = $Element;
                }
            }
            $ReturnData[ "Placement" ] = strip_tags($ReturnData[ "Placement" ]);

            $ReturnData[ "KYC" ] = $HTML->find(".row", 3)->children(1)->children(0)->children(1)->children(4)->children(1);
            $ReturnData[ "KYC" ] = strip_tags($ReturnData[ "KYC" ]);


            $PurchaseHistory = $this->ScrapData($CH, $this->PurchaseHistoryURL);
            $HTML->load($PurchaseHistory);
            $ReturnData[ "Purchase" ] = $HTML->getElementById("debit_card_transfers")->childNodes(1);
            $ReturnData[ "Purchase" ] = trim(strip_tags($ReturnData[ "Purchase" ]));
            if (empty($ReturnData[ "Purchase" ]) == true) {
                $ReturnData[ "Purchase" ] = "No";
            } else {
                $ReturnData[ "Purchase" ] = "Yes";
            }

            $ReturnData = array_change_key_case($ReturnData, CASE_LOWER);

            $this->ReturnData = $ReturnData;
        } else {
            $this->ReturnData = $this->Error;
        }

        curl_close($CH);
    }


    // URLs to Scrape
    protected $LoginURL = "https://karatbars.com/index.php?action=login";
    protected $BinaryTreeURL = "https://karatbars.com/members.php?page=binarytree";
    protected $PurchaseHistoryURL = "https://karatbars.com/members.php?page=purchasehistory";
    // Login Information Containers
    protected $Username;
    protected $Password;
    // Technical Information
    public $Error = null;
    protected $Agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36";
    protected $CookieFile = "cookie.txt";
    public $ReturnData = null;


    protected function ScrapData($CH, $URL, $Data = null)
    {
        curl_setopt($CH, CURLOPT_URL, $URL);
        curl_setopt($CH, CURLOPT_USERAGENT, $this->Agent);
        curl_setopt($CH, CURLOPT_POST, 1);
        if ($Data !== null) {
            curl_setopt($CH, CURLOPT_POSTFIELDS, $Data);
        }
        curl_setopt($CH, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($CH, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($CH, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($CH, CURLOPT_SSL_VERIFYHOST, 0);
        CURL_SETOPT($CH, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($CH, CURLOPT_COOKIEJAR, $this->CookieFile);
        curl_setopt($CH, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($CH, CURLOPT_TIMEOUT, 30);

        return curl_exec($CH);
    }


    public function LogIn($CH)
    {
        $Data = array(
            "page" => "login",
            "username" => $this->Username,
            "password" => $this->Password,
            "captcha" => $this->Captcha,
            "javascript" => 0 );

        $Data = http_build_query($Data);

        $LogInResult = $this->ScrapData($CH, $this->LoginURL, $Data);

        if (strpos($LogInResult, "Dashboard") == false) {
            $this->Error[ "Error" ] = "Login Failed!";
        } else {
            return true;
        }
    }
}
