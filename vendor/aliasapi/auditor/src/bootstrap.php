<?php

declare(strict_types=1);

namespace AliasAPI\Auditor;

use AliasAPI\Api as API;

function bootstrap()
{
    $settings = [];

    require_once(realpath(dirname(__FILE__) . '/../vendor/aliasapi/api/src/set_defaults.php'));

    $settings = API\set_defaults();

    if (\dirname(__FILE__) == '/storage/ssd1/847/6572847/public_html') {
        \defined('WEBSITE') || \define('WEBSITE', 'https://viewsfolder.000webhostapp.com/auditor/public');
    } else {
        \defined('WEBSITE') || \define('WEBSITE', 'http://localhost/auditor/public');
        \ini_set('display_errors', '1');
        \ini_set('display_startup_errors', '1');
        \error_reporting(E_ALL);
    }

    return $settings;
}
