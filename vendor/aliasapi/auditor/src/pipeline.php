<?php

declare(strict_types=1);

namespace AliasAPI\Auditor;

function pipeline(array $request): void
{
    capture_captcha($request);

    audit_account($request);
}
