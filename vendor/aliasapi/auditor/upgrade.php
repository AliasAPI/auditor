<?php


function upgrade($post, $session, $step)
{
    if (isset($post[ 'action' ]) && $post[ 'action' ] !== $step) {
        // Display the success message for the previous step
        display_view($post[ 'action' ] . '-success.html');
        display_view('footer.html');
        die();
    } elseif (!isset($post[ 'action' ])) {
        display_view($step . '.html');

        echo "</center><br><br>";

        echo "<center>";
        echo "<form name=send method=post action=index.php?page=$step>";
        echo "<input type=text name=username size=25 value=" . $session[ 'username' ] . " readonly><br>";
        echo "<font color=green>Please enter your Karatbars password:</font><br>";
        echo "<input type=password name=password maxlength=96 size=25 value=\"";
        if (isset($_POST[ 'password' ])) {
            echo decode_text($_POST[ 'password' ], '');
        }
        echo "\">";
        echo "<input type=hidden name=action value=$step><br><br>";
        echo "<input type=submit name=submit value=\"Audit My Account\">";
        echo "</form>";
        echo "</center>";
    } else {
        // If they have clicked the register button and there are no errors
        if (isset($post[ 'action' ]) && $post[ 'action' ] == $step) {
            // Log into the user's account and get the key attributes
            $status = get_karatbars_status($session[ 'username' ], $_POST[ 'password' ]);

            $error = audit_karatbars_status($session, $status, $step);

            // If there's not No error
            if ($error !== null) {
                echo "<br><br><br><center><h1><font color=red>$error</font></h1></center>";
                display_view($step . '-fail.html');
                display_view('footer.html');
                die();
            } else {
                // Record the step
                insert_user_step($session[ 'userid' ], $step, '');

                step_up();
            }
        }
    }
}
