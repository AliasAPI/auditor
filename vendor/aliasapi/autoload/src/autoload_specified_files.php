<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function autoload_specified_files(array $load_files, array $extra_files): void
{
    $load_these_files = \array_merge($load_files, $extra_files);

    foreach ($load_these_files as $index => $file) {
        if (! is_dir($file) && file_exists($file)) {
            require_once($file);
        }
    }
}
