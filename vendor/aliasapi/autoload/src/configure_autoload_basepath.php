<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function configure_autoload_basepath($basepath = '')
{
    if (! defined('BASEPATH')) {
        if ($basepath == '') {
            // autoload up to ../aliasapi up to ../vendor up to ../
            $basepath = dirname(dirname(dirname(dirname(__DIR__))));
        }

        define('BASEPATH', $basepath);
    }

    return BASEPATH;
}
