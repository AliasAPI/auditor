<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function configure_autoload_paths(array $load_paths = []): array
{
    if (empty($load_paths)) {
        $load_paths = array(
            // Files can be in src/functions because files are globbed recursively.
            BASEPATH . '/src',
            BASEPATH . '/vendor/aliasapi'
        );
    }

    return $load_paths;
}
