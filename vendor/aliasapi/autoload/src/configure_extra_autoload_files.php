<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function configure_extra_autoload_files(array $extra_files = []): array
{
    $default_files = [];

    if (file_exists(BASEPATH . '/vendor/autoload.php')) {
        // Run the composer autoloader
        $default_files[] = BASEPATH . '/vendor/autoload.php';
    }

    $extra_files = array_merge($extra_files, $default_files);

    return $extra_files;
}
