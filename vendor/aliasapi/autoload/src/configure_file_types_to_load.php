<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

function configure_file_types_to_load($file_types = ['*.php']): array
{
    return $file_types;
}
