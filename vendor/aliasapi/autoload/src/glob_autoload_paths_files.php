<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

use  AliasAPI\Autoload as A;

function glob_autoload_paths_files(array $load_paths, array $load_file_types, $max_depth): array
{
    $all_files = array();
    $files_glob = array();

    foreach ($load_paths as $path) {
        foreach ($load_file_types as $pattern) {
            $files_glob = A\glob_files_recursively($path, $path, $pattern, $max_depth);
            $all_files = array_merge($all_files, $files_glob);
        }
    }

    return $all_files;
}
