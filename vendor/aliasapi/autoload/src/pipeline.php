<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

use AliasAPI\Autoload as Autoload;

function pipeline($basepath = '', $load_paths = [], $load_types = [], $extra_files = [], $max_depth)
{
    require_once(__DIR__ . '/require_autoload_files.php');

    Autoload\require_autoload_files();

    $basepath = Autoload\configure_autoload_basepath($basepath);

    $load_paths = Autoload\configure_autoload_paths($load_paths);

    $load_file_types = Autoload\configure_file_types_to_load($load_types);

    $max_depth = Autoload\configure_recursive_depth($max_depth);

    $load_files = Autoload\glob_autoload_paths_files($load_paths, $load_file_types, $max_depth);

    $extra_files = Autoload\configure_extra_autoload_files($extra_files);

    Autoload\autoload_specified_files($load_files, $extra_files);
}
