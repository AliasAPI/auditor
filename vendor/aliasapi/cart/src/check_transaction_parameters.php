<?php


function check_transaction_parameters( $prep )
{
    if( ! isset( $prep['amount'] ) )
    {
        $prep['error'][] = "The [ amount ] was not set.";
    }

    if( ! isset( $prep['business'] ) )
    {
        $prep['error'][] = "The [ business ] was not set.";
    }

    if( ! isset( $prep['clientId'] ) )
    {
        $prep['error'][] = "The [ clientId  ] was not set.";
    }

    if( ! isset( $prep['clientSecret'] ) )
    {
        $prep['error'][] = "The [ clientSecret ] was not set.";
    }

    if( ! isset( $prep['login'] ) )
    {
        $prep['error'][] = "The [ login ] was not set.";
    }

    if( ! isset( $prep['merchant'] ) )
    {
        $prep['error'][] = "The [ merchant ] was not set.";
    }

    if( isset( $prep['type'] ) && $prep['type'] == 'buy' && ! isset( $prep['name'] ) )
    {
        $prep['error'][] = "The [ name ] was not set.";
    }

    if( ! isset( $prep['paypal_mode'] )
        || ( $prep['paypal_mode'] !== 'live'
        && $prep['paypal_mode'] !== 'sandbox' ) )
    {
        $prep['error'][] = "The [ paypal_mode ] was not set.";
    }

    if( ! isset( $prep['sell'] ) )
    {
        $prep['error'][] = "The [ sell ] was not set.";
    }

    if( isset( $prep['type'] ) && $prep['type'] == 'buy' && ! isset( $prep['sku'] ) )
    {
        $prep['error'][] = "The [ sku ] was not set.";
    }

    if( ! isset( $prep['type'] ) || empty( $prep['type'] ) )
    {
        $prep['error'][] = "The [ type ] was not set.";
    }

    if( ! isset( $prep['userid'] ) )
    {
        $prep['error'][] = "The [ userid ] was not set.";
    }

    if( ! isset( $prep['website'] ) )
    {
        $prep['error'][] = "The [ website ] was not set.";
    }

    // If a payment transaction has been returned
    if( isset( $prep['transaction_id'] ) && ! empty( $prep['transaction_id'] ) )
    {
        if( ! isset( $prep['status'] ) || empty( $prep['status'] ) )
        {
            $prep['error'][] = "The [ status ] was not set.";
        }

        if( ! isset( $prep['sale_id'] ) || empty( $prep['sale_id'] ) )
        {
            $prep['error'][] = "The [ sale_id ] was not set.";
        }
    }

    if( isset( $prep['display_refund-1_link'] ) && $prep['display_refund-1_link'] == 'yes'
        && ( ! isset( $prep['sale_id'] ) || $prep['sale_id'] == '' ) )
    {
        $prep['error'][] = "The [ display_refund-1_link ] was set and the [ sale_id ] was not set.";
    }

    if( isset( $prep['display_refund-1_link'] ) && $prep['display_refund-1_link'] == 'yes'
        && ( ! isset( $prep['sale_id'] ) || $prep['sale_id'] == '' ) )
    {
        $prep['error'][] = "The [ display_refund-1_link ] was set and the [ sale_id ] was not set.";
    }

    /*
    if( isset( $prep['error'] ) && !empty( $prep['error'] ) )
    {
        sayd( $prep );
    }
    */

    return $prep;
}


?>
