<?php


function display_payment_status( $status, $user_step )
{
    if( isset( $status['status'] ) && isset( $user_step ) )
    {
        if( $status['status'] == 'pending' || $status['status'] == 'processed' || $status['status'] == 'in-progress' )
        {
            display_view( 'promote-pending.html' );
        }
        elseif( $status['status'] == 'completed' || $status['status'] == 'partially_refunded' )
        {
            $_POST['action'] = $user_step;
            $_POST['transaction_success'] = $user_step;

            step_up();
        }
        elseif( isset( $status['status'] ) && !empty( $status['status'] ) )
        //if( $status['status'] !== 'completed' || $status['status'] !== 'partially_refunded' )
        {
            // failed, declined, expired, reversed, voided
            display_view( $user_step . '-fail.html' );
        }
    }
}

?>
