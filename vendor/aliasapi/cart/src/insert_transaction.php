<?php


function insert_transaction( $prep )
{
    if( isset( $prep['transaction_id'] ) && !empty( $prep['transaction_id'] )
        && isset( $prep['type'] ) && !empty( $prep['type'] ) )
    {
        // Check to see if the transaction has already been recorded
        list( $trans_id ) = fetch_row( "SELECT `transactionid` FROM `user_payments`
                                        WHERE `transactionid` = '" . $prep['transaction_id'] . "'
                                        AND `type` = '" . $prep['type'] . "'
                                        LIMIT 1 " );

        // If the transaction does NOT already exist
        if( empty( $trans_id ) )
        {
            // Store refunds as negative values to make queries and math easier
            $adjusted_amount = ( $prep['type'] == 'refund' ) ? $prep['amount'] * -1 : $prep['amount'];

            $refund_id = ( isset( $prep['refund_id'] ) ) ? $prep['refund_id'] : '';

            // INSERT TRANSACTION ---------------------------------------------------------------------------------//
            query( "INSERT INTO `user_payments` SET
                    `uid` = '" . $prep['userid'] . "',
                    `merchant` = '" . $prep['merchant'] . "',
                    `step` = '" . $prep['sell'] . "',
                    `type` = '" . $prep['type'] . "',
                    `transactionid` = '" . $prep['transaction_id'] . "',
                    `amount` = '" . $adjusted_amount . "',
                    `status` = '" . $prep['status'] . "',
                    `saleid` = '" . $prep['sale_id'] . "',
                    `refundid` = '" . $refund_id . "',
                    `datetime` = NOW() " );

            // Check to see if the transaction has already been recorded
            list( $trans_recorded ) = fetch_row( "SELECT `transactionid` FROM `user_payments`
                                                  WHERE `type` = '" . $prep['type'] . "'
                                                  AND `transactionid` = '" . $prep['transaction_id'] . "'
                                                  LIMIT 1 " );

            if( !$trans_recorded )
            {
                emergency( "Transaction [ " . $prep['transaction_id'] . " ] type [ " . $prep['type'] . " ] for [ " . $prep['amount'] . " ] was not saved.", "1" );
            }
        }
    }
}


?>
