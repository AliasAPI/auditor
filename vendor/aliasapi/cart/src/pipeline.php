<?php

declare(strict_types=1);

namespace AliasAPI\Cart;

use AliasAPI\Cart as Cart;

function pipeline(array $request, array $user): array
{
    if (\defined('MODE') && MODE == 'live') {
        \define('PROMOTE_1_PRICE', 99);
        \define('PROMOTE_2_PRICE', 125);
        \define('PROMOTE_3_PRICE', 299);
        \define('PROMOTE_4_PRICE', 399);
    } else {
        \define('PROMOTE_1_PRICE', 1);
        \define('PROMOTE_2_PRICE', 2);
        \define('PROMOTE_3_PRICE', 3);
        \define('PROMOTE_4_PRICE', 4);
    }

    $steps = set_ally_alias($request, $steps);

    $steps = set_ally_uuid($request, $steps);

    $steps = set_ally_cart($request, $steps);
}
