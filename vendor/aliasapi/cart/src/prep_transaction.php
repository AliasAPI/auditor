<?php


function prep_transaction()
{
    $prep = array();

    // Get sku
    if( isset( $_GET['sku'] ) && !empty( $_GET['sku'] ) )
    {
        $sku_array = preg_split( '/_/', $_GET['sku'] );
        $get = array();
        $get['sell'] = $sku_array[0];
        $get['userid'] = $sku_array[2];
        $get['merchant'] = $sku_array[4];
    }

    // If Paypal sends sets variables to execute the payment
    if( isset( $_GET['paymentId'] ) && isset( $_GET['PayerID'] ) )
    {
        $prep['paymentId'] = $_GET['paymentId'];
        $prep['transaction_id'] = $_GET['paymentId'];
        $prep['PayerID'] = $_GET['PayerID'];
        $prep['type'] = 'buy';

        unset( $_GET['paymentId'] );
        unset( $_GET['PayerID'] );
    }

    if( isset( $_GET['page'] ) && !empty( $_GET['page'] ) )
    {
        $prep['page'] = $_GET['page'];
    }

    if( isset( $_GET['sale_id'] ) && !empty( $_GET['sale_id'] ) )
    {
        $prep['sale_id'] = $_GET['sale_id'];
        $prep['type'] = 'refund';
    }

    if( isset( $_GET['refund'] ) && !empty( $_GET['refund'] ) )
    {
        $prep['refund'] = $_GET['refund'];
        $prep['type'] = 'refund';
    }

    if( isset( $_GET['refund_id'] ) && !empty( $_GET['refund_id'] ) )
    {
        $prep['refund_id'] = $_GET['refund_id'];
        $prep['type'] = 'refund';
    }

    // Set userid
    if( isset( $_SESSION['userid'] ) && !empty( $_SESSION['userid'] ) )
    {
        $prep['userid'] = $_SESSION['userid'];
    }
    elseif( isset( $get['userid'] ) && !empty( $get['userid'] ) )
    {
        $prep['userid'] = $get['userid'];
    }

    // Set merchant
    if( isset( $_SESSION['merchant'] ) && !empty( $_SESSION['merchant'] ) )
    {
        $prep['merchant'] = $_SESSION['merchant'];
    }
     elseif( isset( $get['merchant'] ) && !empty( $get['merchant'] ) )
    {
        $prep['merchant'] = $get['merchant'];
    }

    // Set merchant credentials
    if( isset( $prep['merchant'] ) && $prep['merchant'] == '1'
        && defined( 'MERCHANT_1_PAYPAL_ID' )
        && defined( 'MERCHANT_1_PAYPAL_API_CLIENTID' )
        && defined( 'MERCHANT_1_PAYPAL_API_CLIENSECRET' ) )
    {
        $prep['business'] = MERCHANT_1_PAYPAL_ID;
        $prep['clientId'] = MERCHANT_1_PAYPAL_API_CLIENTID;
        $prep['clientSecret'] = MERCHANT_1_PAYPAL_API_CLIENSECRET;
    }
    elseif( isset( $prep['merchant'] ) && $prep['merchant'] == '2'
        && defined( 'MERCHANT_2_PAYPAL_ID' )
        && defined( 'MERCHANT_2_PAYPAL_API_CLIENTID' )
        && defined( 'MERCHANT_2_PAYPAL_API_CLIENSECRET' ) )
    {
        $prep['business'] = MERCHANT_2_PAYPAL_ID;
        $prep['clientId'] = MERCHANT_2_PAYPAL_API_CLIENTID;
        $prep['clientSecret'] = MERCHANT_2_PAYPAL_API_CLIENSECRET;
    }

    // Set sell
    if( isset( $_SESSION['user_step'] ) && !empty( $_SESSION['user_step'] ) )
    {
        if( $_SESSION['user_step'] == 'promote-1' || $_SESSION['user_step'] == 'promote-2'
            || $_SESSION['user_step'] == 'promote-3' || $_SESSION['user_step'] == 'promote-4' )
        {
            $prep['user_step'] = $_SESSION['user_step'];
            $prep['sell'] = $_SESSION['user_step'];
            $prep['type'] = 'buy';
        }

        if( $_SESSION['user_step'] == 'refund-1' )
        {
            $prep['user_step'] = $_SESSION['user_step'];
            $prep['sell'] = 'promote-1';
            $prep['type'] = 'refund';
        }
    }
    elseif( isset( $get['sell'] ) && !empty( $get['sell'] ) )
    {
        $prep['sell'] = $get['sell'];
    }


    // Set sku
    if( isset( $prep['sell'] ) && isset( $prep['userid'] ) && isset( $prep['merchant'] ) )
    {
        $prep['sku'] = $prep['sell'] . '_member_' . $prep['userid'] . '_merchant_' . $prep['merchant'];
    }

    // Set selling attributes
    if( isset( $prep['sell'] ) && $prep['sell'] == 'promote-1' && defined( 'PROMOTE_1_PRICE' ) )
    {
        $prep['name'] = "StepsTEAM Lifetime Membership";
        $prep['amount'] = PROMOTE_1_PRICE;
    }
    elseif( isset( $prep['sell'] ) && $prep['sell'] == 'promote-2' && defined( 'PROMOTE_2_PRICE' ) )
    {
        $prep['name'] = "StepsTEAM Service 2";
        $prep['amount'] = PROMOTE_2_PRICE;
    }
    elseif( isset( $prep['sell'] ) && $prep['sell'] == 'promote-3' && defined( 'PROMOTE_3_PRICE' ) )
    {
        $prep['name'] = "StepsTEAM Service 3";
        $prep['amount'] = PROMOTE_3_PRICE;
    }
    elseif( isset( $prep['sell'] ) && $prep['sell'] == 'promote-4' && defined( 'PROMOTE_4_PRICE' ) )
    {
        $prep['name'] = "StepsTEAM Service 4";
        $prep['amount'] = PROMOTE_4_PRICE;
    }

    // Set login
    if( isset( $_SESSION['login'] ) && !empty( $_SESSION['login'] ) )
    {
        $prep['login'] = $_SESSION['login'];
    }
    elseif( isset( $_GET['L'] ) && !empty( $_GET['L'] ) )
    {
        $prep['login'] = $_GET['L'];
    }

    // Set Paypal mode
    if( defined( 'PAYPAL_MODE' ) )
    {
        $prep['paypal_mode'] = PAYPAL_MODE;
    }

    // Set website
    if( defined( 'WEBSITE' ) )
    {
        $prep['website'] = WEBSITE;
    }

    // If the user has clicked the link to get to the refund page
    if( isset( $prep['page'] ) && $prep['page'] == 'refund-1-request'
        // . . . and has not clicked the link to execute the refund transaction
        && !isset( $prep['refund'] )
        && isset( $prep['user_step'] ) && $prep['user_step'] == 'refund-1'
        && isset( $prep['website'] ) && !empty( $prep['website'] )
        && isset( $prep['login'] ) && !empty( $prep['login'] )
        && isset( $prep['sku'] ) && !empty( $prep['sku'] ) )
    {
        $prep['display_refund-1_link'] = 'yes';
    }

    // If there is a buying transaction to execute
    if( ( ( isset( $prep['paymentId'] ) && !empty( $prep['paymentId'] ) )
        && ( isset( $prep['PayerID'] ) && !empty( $prep['PayerID'] ) ) )
        // or if there is a refund transaction to execute
        || ( isset( $prep['sale_id'] ) && !empty( $prep['sale_id'] ) ) )
    {
        $prep['execute_transaction'] = 'yes';
    }

    // If the user has clicked to process the refund transaction
    if( isset( $prep['refund'] ) && !empty( $prep['refund'] )
        // . . . and the refund transaction has not already been processed
        && ( !isset( $prep['refund_id'] ) || empty( $prep['refund_id'] ) ) )
    {
        $prep['process_refund-1'] = 'yes';
        $prep['type'] = 'refund';
    }

    return $prep;
}


?>
