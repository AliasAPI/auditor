<?php


function process_payment_WRAPPER()
{
    // Initialize vars
    $status = array();

    $prep = prep_transaction();

    if (isset($prep['execute_transaction'])) {
        $prep = check_transaction_parameters($prep);

        if (!isset($prep['error'])) {
            $status = select_transaction_status($prep);

            $prep = array_merge($prep, $status);

            $prep = check_transaction_parameters($prep);
        }
        /*
         * 'paymentId' => 'PAY-20NGA'
         * 'PayerID' => 'WDFJKDF6NRJ'
         * 'type' => 'buy'
         * 'userid' => '1'
         * 'merchant' => '1'
         * 'business' => 'sales@imtesting.com'
         * 'clientId' => 'AXRuzbNIRiVD58jUs-r'
         * 'clientSecret' => 'EQM3O-pN-JTingu'
         * 'sell' => 'promote-1'
         * 'user_step' => 'promote-1'
         * 'sku' => 'promote-1_member_1_merchant_1'
         * 'name' => 'Lifetime Membership'
         * 'amount' => int 99
         * 'login' => 'd6dee835ebcf7'
         * 'paypal_mode' => 'sandbox'
         * 'website' => 'http://localhost/stepsteam/'
         */

        // If there are no reasons NOT to process the transaction. . .
        if (! isset($prep['error'])) {
            $response = call_aliasapi($prep);

            /*
             * Payment Response Array:
             * 'userid' => '1'
             * 'merchant' => '1'
             * 'step' => 'promote-1'
             * 'transaction_id' => 'PAY-8GFHIY'
             * 'amount' => '99.00'
             * 'sale_id' => '2UW91865WL217263U'
             * 'status' => 'completed'
             */

            /*
             * Refund Response Array:
             * [type] => refund
             * [transaction_id] => PAY-2LNHLGLA
             * [sale_id] => 3S9722015SK8475143G
             * [refund_id] => 9M66524LCG116754P
             * [amount] => 99.00
             * [status] => completed
             */

            // Provide the $prep settings again just in case
            // they aren't returned from the payment system.
            // Overwrite the local $prep with the $response.
            $prep = array_merge($prep, $response);

            insert_transaction($prep);

            update_transaction($prep);

            $status = select_transaction_status($prep);

            update_promote($status);

            list($sponsor_id) = fetch_row("SELECT `sponsor` FROM `users`
                                              WHERE `userid` = '" . $prep['userid'] . "'
                                              LIMIT 1 ");

            update_refer($sponsor_id);

            update_refer($prep['userid']);

            if (isset($prep['user_step'])) {
                display_payment_status($status, $prep['user_step']);
            }
        }
    }

    return $status;
}
