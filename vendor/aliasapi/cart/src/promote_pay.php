<?php


function promote_pay($session)
{
    $prep = prep_transaction();

    $prep = check_transaction_parameters($prep);

    if (!isset($prep['error'])) {
        $response = call_aliasapi($prep);

        echo "
        <a href=" . $response['approvalUrl'] . ">";
        display_view($session['route'] . '.html');
        echo "</a>";
    }
}
