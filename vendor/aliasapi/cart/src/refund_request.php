<?php


function refund_request()
{
    $prep = prep_transaction();

    $status = select_transaction_status( $prep );

    $prep = array_merge( $prep, $status );

    $prep = check_transaction_parameters( $prep );

    // sayd( 'prep',  $prep, 'status', $status, '$_GET', $_GET );

    if( !isset( $prep['error'] )
        && isset( $prep['display_refund-1_link'] ) && $prep['display_refund-1_link'] == 'yes' )
    {
        // Display the entire page with the refund link
        display_view( $prep['page'] . '.html' );
        echo "<center>
        <a href=" . $prep['website'] . "?L=" . $prep['login'] . "&sale_id=" . $prep['sale_id'] . "&sku=" . $prep['sku'] . "&refund=yes>I quit.</a></center>";
    }
    // If the user has clicked the refund link
    elseif( !isset( $prep['error'] )
        && isset( $prep['process_refund-1'] ) && $prep['process_refund-1'] == 'yes'
        && ( !isset( $prep['refund_id'] ) || $prep['refund_id'] == '' )
        && isset( $prep['type'] ) && $prep['type'] !== 'refund'
        && isset( $prep['sale_id'] ) && $prep['sale_id'] !== ''
        && isset( $prep['status'] ) && $prep['status'] == 'completed' )
    {
        $status = process_payment_WRAPPER();
    }
}


?>
