<?php


function select_transaction_status( $prep )
{
    // Initialize vars
    $status = array();

    if( isset( $prep['userid'] ) && !empty( $prep['userid'] )
            && isset( $prep['sell'] ) && !empty( $prep['sell'] ) )
    {
        $search_query = " `uid` = '" . $prep['userid'] . "' AND `step` = '" . $prep['sell'] . "' ";

        if( isset( $prep['paymentId'] ) && !empty( $prep['paymentId'] ) )
        {
            $search_query .= " AND `transactionid` = '" . $prep['paymentId'] . "' ";
        }
    }

    if( isset( $search_query ) )
    {
        // Select the most recent payment status
        list( $pid, $userid, $merchant, $step, $type, $transaction_id, $amount,
              $t_status, $sale_id, $refund_id, $datetime ) = fetch_row("SELECT
              `pid`, `uid`, `merchant`, `step`, `type`, `transactionid`,
              `amount`, `status`, `saleid`, `refundid`, `datetime`
              FROM `user_payments`
              WHERE $search_query
              ORDER BY `pid` DESC
              LIMIT 1 ");

        if( isset( $pid ) && !empty( $pid ) )
        {
            // Reverse the refund negative amount setting
            $amount = ( $amount < 0 ) ? $amount * -1 : $amount;

            $status['pid'] = $pid;
            $status['userid'] = $userid;
            $status['merchant'] = $merchant;
            $status['step'] = $step;
            $status['type'] = $type;
            $status['transaction_id'] = $transaction_id;
            $status['amount'] = $amount;
            $status['status'] = $t_status;
            $status['sale_id'] = $sale_id;

            if( !empty( $refund_id ) )
            {
                $status['refund_id'] = $refund_id;
            }
            $status['datetime'] = $datetime;
        }
    }

    return $status;
}


?>
