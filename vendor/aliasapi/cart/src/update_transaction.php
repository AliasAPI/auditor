<?php


function update_transaction( $prep )
{
    // UPDATE TRANSACTION ---------------------------------------------------------------------------------//
    if( isset( $prep['transaction_id'] ) && !empty( $prep['transaction_id'] ) )
    {
        // Check to see if the transaction has already been recorded
        list( $trans_id ) = fetch_row( "SELECT `transactionid` FROM `user_payments`
                                        WHERE `transactionid` = '" . $prep['transaction_id'] . "'
                                        LIMIT 1 " );

        if( !empty( $trans_id ) )
        {
            $refund_id = ( isset( $prep['refund_id'] ) ) ? $prep['refund_id'] : '';

            query( "UPDATE `user_payments` SET
                    `status` = '" . $prep['status'] . "',
                    `refundid` = '" . $refund_id . "',
                    `datetime` = NOW()
                    WHERE `type` = '" . $prep['type'] . "'
                    AND `transactionid` = '" . $prep['transaction_id'] . "' " );
        }
    }
}


?>
