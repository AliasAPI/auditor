<?php

namespace AliasAPI\Client;

use AliasAPI\Api as API;
use AliasAPI\Alias as Alias;
use AliasAPI\Autoload as Autoload;
use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

function pipeline($settings)
{
    Autoload\pipeline(
        $settings['basepath'],
        $settings['load_paths'],
        $settings['load_types'],
        $settings['extra_files'],
        $settings['max_depth']
    );

    API\set_domains();

    $alias_attributes = Alias\pipeline($settings);

    $R = CrudTable\connect_orm($alias_attributes['database_config']);

    $request['actionS'] = 'create user';
    $request['ally']['alias'] = 'UserServer';
    $request['ally']['client'] = 'StepsTEAM';
    $request['email'] = 'demo@stepsteam.com';


    // list($code, $response) = Messages\request(USERSERVER, $request);
    // sayd($response);
    return $request;
}
