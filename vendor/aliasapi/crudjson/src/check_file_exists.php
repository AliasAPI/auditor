<?php

namespace AliasAPI\CrudJson;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

function check_file_exists(string $file_path): bool
{
    $exists = false;

    $adapter = new Local('/');

    $filesystem = new Filesystem($adapter);

    $exists = $filesystem->has($file_path);

    return $exists;
}
