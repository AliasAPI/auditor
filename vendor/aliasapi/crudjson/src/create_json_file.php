<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

function create_json_file($file_path, $data_array): bool
{
    $response = false;

    $adapter = new Local('/');

    $filesystem = new Filesystem($adapter);

    $json = \json_encode($data_array, JSON_PRETTY_PRINT);

    $json = \utf8_encode($json);

    $response = $filesystem->write($file_path, $json);

    return $response;
}
