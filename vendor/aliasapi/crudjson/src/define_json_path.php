<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\Messages as Messages;

function define_json_path(bool $mandate = false, string $path = ''): ?string
{
    if (!defined('JSON_PATH')) {
        $json_path = '';

        $default_path = dirname(dirname(dirname(dirname(__DIR__)))) . '/jsondata';

        if ($mandate == true && $path == '' && ! \is_writeable($default_path)) {
            Messages\respond(501, ["The default json_path is not writable."]);
        } elseif ($mandate == true && $path !== '' && ! \is_writable($path)) {
            Messages\respond(501, ["The specified json_path is not writable."]);
        } elseif ($path !== '' && \is_writable($path)) {
            // Use the specified path as the jsondata folder
            $json_path = $path;
        } elseif ($path == '' && \is_writeable($default_path)) {
            $json_path = $default_path;
        } elseif (\is_writable(\ini_get('upload_tmp_dir'))) {
            // Use the lampp (/opt/lampp/temp/) directory
            $json_path = \ini_get('upload_tmp_dir');
        } elseif (\is_writable(\sys_get_temp_dir())) {
            // Use the /tmp directory
            $json_path = \sys_get_temp_dir();
        } else {
            $whoami = \AliasAPI\CrudJson\find_whoami();
            Messages\respond(501, ["User [ $whoami ] cannot write to the filesystem."]);
        }

        if ($json_path !== '') {
            $json_path = \rtrim($json_path, DIRECTORY_SEPARATOR);
            \define('JSON_PATH', $json_path);
        }
    }

    return $json_path;
}
