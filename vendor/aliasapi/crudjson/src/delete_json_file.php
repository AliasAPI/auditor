<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

function delete_json_file($file_path): bool
{
    $adapter = new Local('/');

    $filesystem = new Filesystem($adapter);

    $bool = $filesystem->delete($file_path);

    return $bool;
}
