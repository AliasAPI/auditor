<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson as CrudJson;

function find_request_tag(array $request): string
{
    $tag = '';

    if (isset($request['tag'])) {
        $tag = $request['tag'];
    } elseif (isset($request['params']['tag'])) {
        $tag = $request['params']['tag'];
    } elseif (isset($request['body']['tag'])) {
        $tag = $request['body']['tag'];
    }

    return $tag;
}
