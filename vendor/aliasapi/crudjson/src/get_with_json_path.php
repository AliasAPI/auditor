<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\Messages as Messages;

function get_with_json_path($file_path): string
{
    if (\defined('JSON_PATH') && \file_exists(JSON_PATH)) {
        $file_path = JSON_PATH . '/' . $file_path;
        $file_path = str_replace('//', '/', $file_path);
        $file_path = str_replace('/', DIRECTORY_SEPARATOR, $file_path);

        return $file_path;
    } else {
        Messages\respond(501, ["The JSON_PATH is not set yet."]);
    }
}
