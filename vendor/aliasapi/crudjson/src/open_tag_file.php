<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson as CrudJson;

function open_tag_file(array $request, string $tag): array
{
    if (isset($tag) && ! empty($tag)) {
        $file_path = get_with_json_path($tag);

        $exists = CrudJson\check_file_exists($file_path);

        if ($exists) {
            $request['tag_file'] = CrudJson\read_json_file($file_path);
        }
    }

    return $request;
}
