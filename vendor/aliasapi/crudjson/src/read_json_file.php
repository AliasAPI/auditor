<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

function read_json_file($file_path): array
{
    $adapter = new Local('/');

    $filesystem = new Filesystem($adapter);

    $json = $filesystem->read($file_path);

    $file_array = \json_decode($json, true);

    return $file_array;
}
