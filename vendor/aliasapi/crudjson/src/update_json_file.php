<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

function update_json_file($file_path, $data_array): bool
{
    $adapter = new Local('/');

    $filesystem = new Filesystem($adapter);

    $json = $filesystem->read($file_path);

    $before_array = (array) \json_decode($json);

    // 2DO+++ add JSON error checking here

    $after_array = \array_merge($before_array, $data_array);

    $json = \json_encode($after_array, JSON_PRETTY_PRINT);

    $json = \utf8_encode($json);

    $response = $filesystem->update($file_path, $json);

    return $response;
}
