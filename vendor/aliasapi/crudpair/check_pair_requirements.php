<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\Messages as Messages;

function check_pair_requirements(array $request, array $alias_attributes)
{
    if ($request['body']['actionS'] == 'create alias pair' ||
        $request['body']['actionS'] == 'read alias pair' ||
        $request['body']['actionS'] == 'update alias pair' ||
        $request['body']['actionS'] == 'delete alias pair') {
        if (! \defined('JSON_PATH') || ! \file_exists(JSON_PATH)) {
            Messages\respond(501, ["The JSON_PATH does not exist."]);
        }

        // required functions
        // CrudJson\create_json_file($pair['path'], $pair);

        if (empty($alias_attributes)) {
            Messages\respond(501, ["The alias_attributes is not set."]);
        }

        if (!isset($alias_attributes['api_keys'])) {
            Messages\respond(501, ["The api_keys is not set for the alias."]);
        }

        if (!isset($alias_attributes['api_keys']['api_pass'])) {
            Messages\respond(501, ["The api_pass is not set for the alias."]);
        }

        if (!isset($alias_attributes['api_keys']['private_key'])) {
            Messages\respond(501, ["The private_key is not set for the alias."]);
        }

        if (!isset($alias_attributes['api_keys']['public_key'])) {
            Messages\respond(501, ["The api_pass is not set for the alias."]);
        }

        if (!isset($request['body']['ally'])) {
            Messages\respond(400, ["The ally is not set in the request."]);
        }

        if (!isset($request['body']['ally']['alias'])) {
            Messages\respond(400, ["The ally alias is not set in the request."]);
        }

        if (!isset($request['body']['ally']['client'])) {
            Messages\respond(400, ["The ally client is not set in the request."]);
        }

        if (!isset($request['body']['tag'])) {
            Messages\respond(400, ["The tag is not set in the request."]);
        }

        if (!isset($request['body']['client_url'])) {
            Messages\respond(400, ["The client_url is not set in the request."]);
        }

        if (!isset($request['body']['public_key'])) {
            Messages\respond(400, ["The public_key is not set in the request."]);
        }
    }
}
