<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\CrudJson as CrudJson;
use AliasAPI\Crypto as Crypto;
use AliasAPI\Messages as Messages;

function create_pair_file(array $request, array $alias_attributes, string $pair_file_path)
{
    if ($request['body']['actionS'] == 'create alias pair' || $request['body']['actionS'] == 'update alias pair') {
        $exists = CrudJson\check_file_exists($pair_file_path);

        if (! $exists) {
            $pair = [];

            // ally client alias         $request['body']['ally']['client']
            $pair['client_alias'] = $request['body']['ally']['client'];

            // server alias              $request['body']['ally']['alias']
            $pair['server_alias'] = $alias_attributes['alias'];

            // client_url                $request['body']['client_url']
            $pair['client_url'] = $request['body']['client_url'];

            // client public key         $request['body']['public_key']
            $pair['client_public_key'] = $request['body']['public_key'];

            // server public key         $alias_attributes['api_keys']['public_key'];
            $pair['server_public_key'] = $alias_attributes['api_keys']['public_key'];

            // shared symmetric key      $symmetric key
            $pair['shared_key'] = Crypto\derive_paseto_shared_key($pair['client_public_key'], $pair['server_public_key']);

            // Create the subdirectory if needed and write the file
            $created = CrudJson\create_json_file($pair_file_path, $pair);

            if ($created == true) {
                $response = $pair;
                Messages\respond(201, $response);
            } else {
                Messages\respond(500, ["There was a problem saving the pair file."]);
            }
        }
    }
}
