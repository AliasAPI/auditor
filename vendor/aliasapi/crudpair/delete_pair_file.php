<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\CrudJson as CrudJson;
use AliasAPI\Messages as Messages;

function delete_pair_file(array $request, string $pair_file_path)
{
    if ($request['body']['actionS'] == 'update alias pair' || $request['body']['actionS'] == 'delete alias pair') {
        $exists = CrudJson\check_file_exists($pair_file_path);

        if ($exists) {
            CrudJson\delete_json_file($pair_file_path);

            $exists = CrudJson\check_file_exists($pair_file_path);

            if ($exists) {
                Messages\respond(500, ["There was a problem deleting the alias pair file."]);
            } elseif ($request['body']['actionS'] == 'delete alias pair') {
                Messages\respond(200, ["The alias pair file has been deleted."]);
            }
        }
    }
}
