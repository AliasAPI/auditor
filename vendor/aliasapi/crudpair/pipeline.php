<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\CrudPair as CrudPair;

function pipeline(array $request, array $alias_attributes)
{
    CrudPair\check_pair_requirements($request, $alias_attributes);

    $pair_file_path = CrudPair\set_pair_file_path($request, $alias_attributes);

    CrudPair\delete_pair_file($request, $pair_file_path);

    CrudPair\create_pair_file($request, $alias_attributes, $pair_file_path);

    CrudPair\update_pair_file($request, $alias_attributes, $pair_file_path);

    $pair_file_array = CrudPair\read_pair_file($pair_file_path);

    return $pair_file_array;
}
