<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\CrudJson as CrudJson;
use AliasAPI\Messages as Messages;

function read_pair_file(array $request, string $pair_file_path)
{
    $pair_file_array = [];

    $exists = CrudJson\check_file_exists($pair_file_path);

    if ($exists) {
        $pair_file_array = CrudJson\read_json_file($pair_file_path);
    }

    if ($exists && $request['body']['actionS'] == 'read alias pair' || $request['body']['actionS'] == 'update alias pair') {
        Messages\respond(200, $pair_file_array);
    } elseif (! $exists && $request['body']['actionS'] == 'read alias pair' || $request['body']['actionS'] == 'update alias pair') {
        Messages\respond(501, ["The key pair file does not exist."]);
    } else {
        return $pair_file_array;
    }
}
