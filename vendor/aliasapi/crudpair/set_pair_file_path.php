<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\CrudJson as CrudJson;

function set_pair_file_path(array $request, array $alias_attributes): ?string
{
    $filename = $request['body']['ally']['client'] .'.'. $alias_attributes['alias'];

    $pair_file_path = CrudJson\get_with_json_path('/alias_pairs/'. $filename);

    return $pair_file_path;
}
