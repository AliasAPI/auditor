<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\CrudJson as CrudJson;

function update_pair_file(array $request, array $alias_attributes, string $pair_file_path)
{
    if ($request['body']['actionS'] == 'update alias pair') {
        $exists = CrudJson\check_file_exists($pair_file_path);

        if ($exists) {
            $bool = delete_pair_file($request, $pair_file_path);
            create_pair_file($request, $alias_attributes, $pair_file_path);
            // read and respond
            CrudPair\read_pair_file($request, $pair_file_path);
        }
    }
}
