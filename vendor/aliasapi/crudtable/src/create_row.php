<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;
use RedBeanPHP\Facade as R;

function create_row(string $table, array $key_pairs_row)
{
    $binding_array = [];

    try {
        // Update the table name with RedBean rules.
        $table_name = CrudTable\sanitize_name($table);

        // Build up the bindings . . .
        foreach ($key_pairs_row as $column => $value) {

            // Update the column name with RedBean rules.
            $column = CrudTable\sanitize_name($column);

            // Correct the column names.
            $binding_array[$column] = $value;
        }

        // sayd('table_name', $table_name, 'clean_array', $clean_array);

        // Only insert a new transaction row if 1 does not exist
        $record = (array) R::findOrCreate($table_name, $binding_array);

        return $record;
    } catch (\Exception $ex) {
        // CAUTION: Displaying an error here is only visible to the payment gateway
        $error[] = "There was an error while inserting a transaction row.";
        $error[] = "Exception Type: " . get_class($ex) . "";
        $error[] = "Error Message: " . $ex->getMessage() . "";
        Messages\respond(500, $error);
    }
}
