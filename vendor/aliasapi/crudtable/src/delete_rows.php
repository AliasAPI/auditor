<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;
use RedBeanPHP\Facade as R;

function delete_rows(string $table, array $key_pairs_array)
{
    try {
        // Select all the rows that match the key pair values
        $recorded_rows = CrudTable\read_rows($table, $key_pairs_array);

        // If there are rows to delete
        if (\count($recorded_rows) > 0 && !empty($recorded_rows)) {

            // Update the table name with RedBean rules.
            $table_name = CrudTable\sanitize_name($table);

            // Loop through the rows and collect the ids
            foreach ($recorded_rows as $index => $row) {
                $ids[] = $row['id'];
            }

            // Delete the rows based on id, return void
            R::trashBatch($table_name, $ids);
        }
    } catch (\Exception $ex) {
        // CAUTION: Displaying an error here is only visible to the payment gateway
        $error[] = "There was an error while updating a row.";
        $error[] = "Exception Type: " . get_class($ex) . "";
        $error[] = "Error Message: " . $ex->getMessage() . "";
        Messages\respond(500, $error);
    }
}
