<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;
use RedBeanPHP\Facade as R;

function read_rows(string $table, array $key_pairs_array): array
{
    $binding_string = '';
    $binding_array = array();

    try {
        // Update the table name with RedBean rules.
        $table_name = CrudTable\sanitize_name($table);
        // print_r($key_pairs_array);
        // Build up the bindings . . .
        foreach ($key_pairs_array as $column => $value) {
            // Update the column name with RedBean rules.
            $column = CrudTable\sanitize_name($column);

            $bind = ':' . $column;
            $binding_string .= "AND $column = $bind ";
            $binding_array["$bind"] = $value;
        }

        // Remove beginning or ending "AND"
        $binding_string = trim($binding_string, "AND ");

        $prepared_query = "SELECT * FROM $table_name WHERE $binding_string";

        $read_rows = (array) R::getAll(
            $prepared_query,
            $binding_array
        );

        // sayd('column', $column, 'bind', $bind, 'binding_string', $binding_string,
        // 'binding_array', $binding_array, 'prepared_query', $prepared_query, 'read_rows', $read_rows);

        return $read_rows;
    } catch (\Exception $ex) {
        // CAUTION: Displaying an error here is only visible to the payment gateway
        $error[] = "There was an error while reading rows.\n";
        $error[] = "Exception Type: " . get_class($ex) . "\n";
        $error[] = "Error Message: " . $ex->getMessage() . "\n";
        Messages\respond(500, $error);
    }
}
