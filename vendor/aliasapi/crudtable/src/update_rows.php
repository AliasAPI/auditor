<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;
use RedBeanPHP\Facade as R;

function update_rows(string $table, array $key_pairs_array, $update_pairs_array)
{
    $ids = '';
    $binding_string = '';

    try {
        // Only update a row if there is exactly one result
        $recorded_rows = CrudTable\read_rows($table, $key_pairs_array);

        if (\count($recorded_rows) > 0 && !empty($recorded_rows)) {

            // Update the table name with RedBean rules.
            $table_name = CrudTable\sanitize_name($table);

            // Get all of the ids of the rows to update
            foreach ($recorded_rows as $index => $row) {
                // $ids[] = $row['id'];
                $ids .= ", '". $row['id'] . "'";
            }

            $ids = trim($ids, ', ');

            // Make sure the key to be updated exists in the table
            foreach ($update_pairs_array as $column => $value) {
                // Update the column name with RedBean rules.
                $column = CrudTable\sanitize_name($column);

                // Only update the bean values with pre-existing properties
                // if (\array_key_exists($column, $recorded_rows[0])) {
                $bind = ':' . $column;
                $binding_string .= ", $column = $bind ";
                $binding_array["$bind"] = $value;
                // }
            }

            $binding_string = trim($binding_string, ', ');

            // Create the query
            $query = "UPDATE $table_name SET $binding_string WHERE id IN ( $ids )";

            // sayd('query', $query, 'binding_array', $binding_array);

            $affected_rows = R::exec($query, $binding_array);

            return $affected_rows;
        }
    } catch (\Exception $ex) {
        // CAUTION: Displaying an error here may only be visible to the payment gateway
        $error[] = "There was an error while updating a row.";
        $error[] = "Exception Type: " . get_class($ex) . "";
        $error[] = "Error Message: " . $ex->getMessage() . "";
        Messages\respond(500, $error);
    }
}
