<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\Messages as Messages;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\Version2\AsymmetricPublicKey;
use ParagonIE\Paseto\Parser;
use ParagonIE\Paseto\Purpose;
use ParagonIE\Paseto\Rules\NotExpired;
use ParagonIE\Paseto\ProtocolCollection;

function check_paseto_token(string $local_private_key, string $received_token)
{
    echo "check_paseto_token()<br>";

    try {
        $local_private_key_bin = Base64UrlSafe::decode($local_private_key);
        $local_private_key_obj = new AsymmetricPublicKey($local_private_key_bin);

        $parser = (new Parser())
            ->setPurpose(Purpose::public())
            // Use the server's private key to decode the token
            ->setKey($local_private_key_obj, true)
            // Adding rules to be checked against the token
            ->addRule(new NotExpired)
            // Only allow Version 2 of the Paseto Program
            ->setAllowedVersions(ProtocolCollection::v2());

        $parsed_token_obj = $parser->parse($received_token);

        $token_claims = $parsed_token_obj->getClaims();

        echo "token_claims [ " . print_r($token_claims, true) . " ]<hr>";

        return $token_claims;
    } catch (PasetoException $ex) {
        Messages\respond(400, ["$ex->getMessage()"]);
    }
}
