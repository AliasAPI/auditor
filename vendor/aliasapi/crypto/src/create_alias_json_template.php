<?php

namespace AliasAPI\Crypto;

if (isset($_GET['create_alias_json_template'])) {
    create_alias_json_template();
}

function create_alias_json_template(): void
{
    require_once(realpath(dirname(__FILE__) . '/../../../autoload.php'));
    require_once('generate_paseto_keypair.php');
    require_once('derive_paseto_shared_key.php');
    require_once('sign_paseto_message.php');
    require_once('verify_paseto_message.php');
    require_once('encrypt_paseto_message.php');
    require_once('decrypt_paseto_message.php');
    require_once('build_paseto_token.php');
    require_once('check_paseto_token.php');
    require_once('convert_timezone.php');
    require_once('create_random_token.php');

    $alias = [];

    list($private_key, $public_key) = generate_paseto_keypair();

    $api_pass = Crypto\create_random_token(20, '');

    $salt = create_random_token(20, '');

    $alias["REPLACE_WITH_ALIAS"] = [
        "alias" => "REPLACE_WITH_ALIAS",
        "api_keys" => [
            "api_pass" => "$api_pass",
            "private_key" => "$private_key",
            "public_key" => "$public_key",
            "salt" => "$salt"
        ],
        "authorized_actions" => [
            "get public key"
        ],
        "database_config" => [
            "dsn" => "mysql:host=localhost;dbname=REPLACE_WITH_DATABASE_NAME",
            "username" => "REPLACE_WITH_DATABASE_USERNAME",
            "password" => "REPLACE_WITH_DATABASE_PASSWORD",
            "frozen" => false,
            "partial" => false
        ],
        "debug_config" => [
            "detail_level" => 1,
            "demo_mode" => true
        ]
    ];

    header('Content-Type: application/json; charset=UTF-8');

    $alias_json = json_encode($alias, JSON_PRETTY_PRINT);

    echo $alias_json;
}
