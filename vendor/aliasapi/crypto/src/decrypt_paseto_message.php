<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\Messages as Messages;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\Version2\SymmetricKey;
use ParagonIE\Paseto\Protocol\Version2;

function decrypt_paseto_message(string $encrypted_message, string $shared_key): string
{
    echo "decrypt_message()<br>";

    try {
        $shared_key_bin = Base64UrlSafe::decode($shared_key);
        $shared_key_obj = new SymmetricKey($shared_key_bin);

        $decrypted_message = Version2::decrypt($encrypted_message, $shared_key_obj);

        echo "decrypted_message [ $decrypted_message ]<hr>";

        return $decrypted_message;
    } catch (PasetoException $ex) {
        Messages\respond(400, ["$ex->getMessage()"]);
    }
}
