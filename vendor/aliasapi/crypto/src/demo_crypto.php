<?php

namespace AliasAPI\Crypto;

// demo_crypto();

function demo_crypto()
{
    require_once(realpath(dirname(__FILE__) . '/../../../autoload.php'));
    require_once('generate_paseto_keypair.php');
    require_once('derive_paseto_shared_key.php');
    require_once('sign_paseto_message.php');
    require_once('verify_paseto_message.php');
    require_once('encrypt_paseto_message.php');
    require_once('decrypt_paseto_message.php');
    require_once('build_paseto_token.php');
    require_once('check_paseto_token.php');
    require_once('convert_timezone.php');
    require_once('create_random_token.php');

    echo "CLIENT<br>";
    list($client_private_key, $client_public_key) = generate_paseto_keypair();
    echo "SERVER<br>";
    list($server_private_key, $server_public_key) = generate_paseto_keypair();

    echo "SHARED_KEY";
    $shared_key = derive_paseto_shared_key($client_public_key, $server_public_key);
    // Running this again to demonstrate how both servers can get the same result.
    $shared_key = derive_paseto_shared_key($server_public_key, $client_public_key);

    $client_message = array('Here' => 'is a', 'test' => 'message', 'in' => 'a signed message');
    $signed_message = sign_paseto_message($client_message, $client_private_key);

    $encrypted_message = encrypt_paseto_message($signed_message, $shared_key);

    $signed_message = decrypt_paseto_message($encrypted_message, $shared_key);

    $verified_message = verify_paseto_message($signed_message, $client_public_key);

    $expiration_interval = '+1 day';
    $extra_claims = array('alias' => 'SandboxRest', 'user' => 'UUID', 'cart' => 'promote-1');

    $paseto_token = build_paseto_token($server_private_key, $expiration_interval, $extra_claims);

    $token_claims = check_paseto_token($server_public_key, $paseto_token);

    $datetime = convert_timezone($token_claims['exp']);
}
