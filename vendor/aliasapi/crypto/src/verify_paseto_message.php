<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\Messages as Messages;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\Version2\AsymmetricPublicKey;
use ParagonIE\Paseto\Protocol\Version2;

function verify_paseto_message(string $signed_message, string $client_public_key)
{
    echo "verify_paseto_message()<br>";

    try {
        $client_public_key_bin = Base64UrlSafe::decode($client_public_key);
        $client_public_key_obj = new AsymmetricPublicKey($client_public_key_bin);

        $verified_message = Version2::verify($signed_message, $client_public_key_obj);

        echo "verified_messasge [ $verified_message ]<hr>";

        return $verified_message;
    } catch (PasetoException $ex) {
        echo "balling " . $ex->getMessage();
        Messages\respond(400, ["$ex->getMessage()"]);
        die();
    }
}
