<?php

declare(strict_types=1);

namespace AliasAPI\Demo;

function configure_demo_array()
{
    $da = array();

    $da[] = array('table' => 'user_steps', 1, 'startdate', '2017-07-26 03:53:38');

    $da[] = array('table' => 'user_steps', 1, 'lastlogin', '2017-07-28 12:45:36');

    $da[] = array('table' => 'user_steps', 1, 'lastcontact', '0000-00-00 00:00:00');

    $da[] = array('table' => 'user_payments',
                'userid' => 1,
                'merchant' => 1,
                'sell' => 'promote-1',
                'type' => 'buy',
                'transaction_id' => '1st_transaction',
                'amount' => 99,
                'status' => 'completed',
                'sale_id' => '1st_sale_id',
                'datetime' => '2017-08-02 03:36:18');

    $da[] = array('table' => 'user_steps', 1, 'promote-1', '2017-08-01 02:36:18');

    $da[] = array('table' => 'user_steps', 1, 'training-1', '2017-07-26 03:53:45');

    $da[] = array('table' => 'user_steps', 1, 'company', '2017-07-26 03:53:46');

    $da[] = array('table' => 'user_steps', 1, 'product', '2017-07-26 03:53:46');

    $da[] = array('table' => 'user_steps', 1, 'pay-plan', '2017-07-26 03:53:46');

    $da[] = array('table' => 'user_payments',
                'userid' => 1,
                'merchant' => 1,
                'sell' => 'promote-1',
                'type' => 'refund',
                'transaction_id' => '1st_transaction',
                'amount' => 99,
                'status' => 'completed',
                'sale_id' => '1st_sale_id',
                'refund_id' => '1st_refund_id',
                'datetime' => '2017-08-02 04:44:18');

    $da[] = array('table' => 'user_steps', 1, 'refund-1', '2017-07-26 05:03:46');

    $da[] = array('table' => 'user_steps', 1, 'register', '2017-07-28 12:45:36');

    $da[] = array('table' => 'user_steps', 1, 'star', '2017-07-28 12:46:36');

    $da[] = array('table' => 'users',
                'email' => 'second@stepsteam.com',
                'refer' => 1,
                'capture_merchant' => 1 );

    $da[] = array('table' => 'user_payments',
                'userid' => 2,
                'merchant' => 2,
                'sell' => 'promote-1',
                'type' => 'buy',
                'transaction_id' => '2nd_transaction',
                'amount' => 99,
                'status' => 'completed',
                'sale_id' => '2nd_sale_id',
                'datetime' => '2017-08-02 03:36:18');

    $da[] = array('table' => 'user_steps', 2, 'promote-1', '2017-08-02 03:36:18');

    $da[] = array('table' => 'user_steps', 1, 'refer-1', '2017-08-01 02:36:18');

    $da[] = array('table' => 'user_steps', 1, 'proof-1', '2017-08-01 02:36:18');

    $da[] = array('table' => 'user_steps', 1, 'crowdfunding', '2017-07-28 12:47:36');

    $da[] = array('table' => 'user_steps', 1, 'co-op-marketing', '2017-07-28 12:48:36');

    $da[] = array('table' => 'user_steps', 1, 'promote-2', '2017-08-01 02:36:18');


    $da[] = array('table' => 'users',
                'email' => 'third@stepsteam.com',
                'capture_merchant' => 2 );

    $da[] = array('table' => 'user_payments',
                'userid' => 3,
                'merchant' => 3,
                'sell' => 'promote-1',
                'type' => 'buy',
                'transaction_id' => '3rd_transaction',
                'amount' => 99,
                'status' => 'completed',
                'sale_id' => '3rd_sale_id',
                'datetime' => '2017-08-02 03:36:18');

    $da[] = array('table' => 'user_steps', 3, 'promote-1', '2017-08-02 03:36:18');

    $da[] = array('table' => 'user_steps', 1, 'refer-2', '2017-08-03 04:36:18');

    $da[] = array('table' => 'user_steps', 1, 'proof-2', '2017-08-03 04:36:18');

    $da[] = array('table' => 'user_steps', 1, 'upgrade-to-bronze', '2017-08-01 02:36:18');

    $da[] = array('table' => 'user_steps', 1, 'send-docs', '2017-08-03 04:36:18');

    $da[] = array('table' => 'user_steps', 1, 'disc', '2017-08-03 04:36:32');

    $da[] = array('table' => 'user_steps', 1, 'exchange', '2017-08-03 04:36:50');

    $da[] = array('table' => 'user_steps', 1, 'upgrade-to-silver', '2017-08-03 04:37:18');

    $da[] = array('table' => 'user_steps', 1, 'promote-3', '2017-08-03 04:37:18');


    $da[] = array('table' => 'users',
                'email' => 'forth@stepsteam.com',
                'capture_merchant' => 2 );

    $da[] = array('table' => 'user_payments',
                'userid' => 4,
                'merchant' => 4,
                'sell' => 'promote-1',
                'type' => 'buy',
                'transaction_id' => '4th_transaction',
                'amount' => 99,
                'status' => 'completed',
                'sale_id' => '4th_sale_id',
                'datetime' => '2017-08-02 03:36:18');

    $da[] = array('table' => 'user_steps', 4, 'promote-1', '2017-08-02 03:36:18');

    $da[] = array('table' => 'user_steps', 1, 'refer-3', '2017-08-03 04:37:18');

    $da[] = array('table' => 'user_steps', 1, 'proof-3', '2017-08-03 04:37:18');

    $da[] = array('table' => 'user_steps', 1, 'purchase-product', '2017-08-03 04:37:18');

    $da[] = array('table' => 'user_steps', 1, 'upgrade-to-gold', '2017-08-03 04:37:18');

    $da[] = array('table' => 'user_steps', 1, 'promote-4', '2017-08-03 04:37:18');


    $da[] = array('table' => 'users',
                'email' => 'fifth@stepsteam.com',
                'capture_merchant' => 2 );

    $da[] = array('table' => 'user_payments',
                'userid' => 5,
                'merchant' => 5,
                'sell' => 'promote-1',
                'type' => 'buy',
                'transaction_id' => '5th_transaction',
                'amount' => 99,
                'status' => 'completed',
                'sale_id' => '5th_sale_id',
                'datetime' => '2017-08-02 03:36:18');

    $da[] = array('table' => 'user_steps', 5, 'promote-1', '2017-08-05 04:36:18');

    $da[] = array('table' => 'user_steps', 1, 'refer-4', '2017-08-03 04:37:18');

    $da[] = array('table' => 'user_steps', 1, 'proof-4', '2017-08-03 04:37:18');

    $da[] = array('table' => 'user_steps', 1, 'upgrade-to-vip', '2017-08-03 04:37:18');


    $da[] = array('table' => 'users',
                'email' => 'sixth@stepsteam.com',
                'capture_merchant' => 2 );

    $da[] = array('table' => 'user_payments',
                'userid' => 6,
                'merchant' => 6,
                'sell' => 'promote-1',
                'type' => 'buy',
                'transaction_id' => '6th_transaction',
                'amount' => 99,
                'status' => 'completed',
                'sale_id' => '6th_sale_id',
                'datetime' => '2017-08-02 03:36:18');

    $da[] = array('table' => 'user_steps', 6, 'promote-1', '2017-08-02 03:36:18');

    $da[] = array('table' => 'user_steps', 1, 'recruit-1', '2017-08-03 04:37:18');

    $da[] = array('table' => 'user_steps', 1, 'praise-1', '2017-08-03 04:37:18');

    // echo "<pre>";
    // print_r( $da );

    return $da;
}
