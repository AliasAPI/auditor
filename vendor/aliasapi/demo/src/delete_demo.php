<?php

declare(strict_types=1);

namespace AliasAPI\Demo;

function delete_demo()
{
    if (defined('MODE') && MODE == 'demo') {
        // If the user is calling the demo account.
        // Count the number of users
        list($count) = fetch_row("SELECT COUNT( userid ) FROM `users` ");

        if ($count > 0 && $count < 10) {
            query("DROP TABLE `usersteps` ");
            query("DROP TABLE `user_payments` ");
            query("DROP TABLE `disc_questions` ");
            query("DROP TABLE `disc_results` ");
            query("DELETE FROM `users` WHERE `userid` > 1 ");
            query("ALTER TABLE `users` AUTO_INCREMENT = 1 ");
        }
    }

    logout();
}
