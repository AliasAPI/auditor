<?php

// MOVE the messages to the views server

declare(strict_types=1);

namespace AliasAPI\Demo;

function demo($post, $session)
{
    $style = "<p style=\"font-size:15px; color:red;\">";

    if (isset($_GET['s'])
        // The user wasnts to use demo mode
        && $_GET['s'] == 'ddemo'
        // and they are on the front page ( for the first time )
        && isset($session['route'])) {
        if ($session['route'] == 'access'
                // and they have NOT logged in, forrealz
                && !isset($session['username'])) {
            echo "<center>" . $style . "<br>
                      To see the site in demo mode, click \"Get Access!\"<br>
                      PLEASE READ the red text at the top of each page.</p></center>";

            // Delete the existing data so that the demo will run right;
            delete_demo($session);
        }
        // If the user was logged into the demo account
        elseif (isset($session['username']) && $session['username'] == 'ddemo') {
            header("location: " . WEBSITE . "?s=ddemo&logout=1");
        }
    }

    // If the user is in demo mode for any other step
    if (isset($session['username'])
        // The user wasnts to use demo mode
        && $session['username'] == 'ddemo'
        // and they are on the front page ( for the first time )
        && isset($session['route'])
        && $session['route'] !== 'access') {
        if ($session['route'] == 'promote-1' && !isset($post['action']) && !isset($_GET['sku'])) {
            echo "<center><a href=index.php?page=promote-1-pay>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will contain the long form sales copy letter.<br>
                  The visitor is sold on using StepsTEAM.com to start and grow an online business.<br>
                  The visitor is asked to pay a $99 one time fee for access to the support system.<br>
                  </p></center>";
        }

        if ($session['route'] == 'promote-1-pay' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_promote-1>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will ask the visitor to pay a $99 lifetime membership fee.<br>
                  Sell the user on paying $99 just for access to the support system.<br>
                  The fee will actually be used to grow a single downline <i>indefinitely</i>.<br>
                  Click the Paypal button and enter test@drew.com and Taste@123 to proceed</p></center>";
        }
        // Note: Paypal sets the $_GET.
        if ($session['route'] == 'promote-1' && isset($_GET['sku'])) {
            echo "<center><a href=index.php?demo=complete_promote-1>" . $style . "Click here to proceed</p></a>
                  " . $style . "This is the Thank You! page.<br>
                  It only displays in the browser immediately after the member pays the membership fee.</p><br></center>";
        }

        if ($session['route'] == 'training-1' && isset($post['action']) && $post['action'] == 'promote-1') {
            echo "<center><a href=index.php?demo=complete_promote-1>" . $style . "Click here to proceed</p></a>
                  " . $style . "This is the Thank You page.<br>
                  It will only display right after a successful payment.</p></center>";
        }

        if ($session['route'] == 'training-1' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_training-1>" . $style . "Click here to proceed</p></a>
                  " . $style . "On this page, the member will be trained.<br>
                  It will tell the member the purpose of StepsTEAM.com. The instructions are simple:<br>
                  Complete the steps as quickly as possible to make the most money.</p></center>";
        }

        if ($session['route'] == 'company' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_company>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will teach the user about affiliate programs.<br>
                  Next, it will teach the member the basics about Karatbars.</p></center>";
        }

        if ($session['route'] == 'product' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_product>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will teach the member about gold.<br>
                  Next, it will teach the member about Karatabars gold specifically.</p></center></center>";
        }

        if ($session['route'] == 'pay-plan' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_pay-plan>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will teach the Karatbars compensation plan.<br>
                  The compensation plan has been written out in paragraph form.</p></center>";
        }

        if ($session['route'] == 'refund-1-request' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_refund-1-returned>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page lets the user get a refund in one click.<br>
                  Discourage refunds and recommend that the user complete the steps.<br>
                  If the user gets a refund, they are redirected to repurchase the membership.<br>
                  To avoid repeated refunds, our system will NOT offer refund to the user again.<br>
                  </p></center>";
        }

        if ($session['route'] == 'refund-1' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_refund-1>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page gives the user the opportunity to quit.<br>
                  Display a small link to the refund-1-request page.</p></center>";
        }

        if ($session['route'] == 'register' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_register>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will help the member register as a Karatbars affiliate.<br>
                  StepsTEAM.com automatically assigns the member a Karatbars sponsor.<br>
                  Their account is audited to make sure they registered correctly.<br>
                  Please enter ddemo and the password provideded orally.</p></center>";
        }

        if ($session['route'] == 'star' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_star>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page defines a STAR.<br>
                  It also foreshadows the STAR system.</p></center>";
        }

        if ($session['route'] == 'refer-1' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_refer-1>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page explains StepsTEAM.com is referring leads to the member's Karatbars link.<br>
                  This page is displayed until a new recruit registers in the member's downline and pays $99.</p></center>";
        }

        if ($session['route'] == 'proof-1' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_proof-1>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page proves that StepsTEAM.com recruited and registered a new affiliate.<br>
                  The member is instructed to check their Karatbars backoffice for proof.</p></center>";
        }

        if ($session['route'] == 'crowdfunding' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_crowdfunding>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page illustrates how ongoing crowdfunding is done.<br>
                  The member will learn how we perpetually build their Karatbars team.</p></center>";
        }

        if ($session['route'] == 'co-op-marketing' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_co-op-marketing>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page lists the 30 services included in the co-op advertising package.<br>
                  The member will learn how little they pay compared to the total cost.</p></center>";
        }

        if ($session['route'] == 'promote-2' && !isset($post['action'])) {
            echo "<center><a href=index.php?page=promote-2-pay>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will ask the member to pay a $125 one time advertising fee. </p></center>";
        }

        if ($session['route'] == 'promote-2-pay' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_promote-2>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will ask the member to pay a $125 one time advertising fee.<br>
                  The fee grows and manages a team indefinitely.</p></center>";
        }

        if ($session['route'] == 'refer-2' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_refer-2>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page explains StepsTEAM.com is referring leads to the member's Karatbars link.<br>
                  This page is displayed until a new recruit registers in the member's downline and pays $99.</p></center>";
        }

        if ($session['route'] == 'proof-2' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_proof-2>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page proves that StepsTEAM.com recruited and registered a new affiliate.<br>
                  The member is instructed to check their Karatbars backoffice for proof.<br>
                  This means the second team has been started.</p></center>";
        }

        if ($session['route'] == 'upgrade-to-bronze' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_upgrade-to-bronze>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page asks the member to purchase the Bronze package from Karatbars.<br>
                  The member is instructed to buy it in their Karatbars backoffice.</p></center>";
        }

        if ($session['route'] == 'send-docs' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_send-docs>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page instructs the member on submitting their KYC documents.<br>
                  The member is instructed to use the Karatbars backoffice.</p></center>";
        }

        if ($session['route'] == 'disc' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_disc>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page profiles the member's personality.</p></center>";
        }

        if ($session['route'] == 'exchange' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_exchange>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page teaches business owners and other affiliates all about the K-exchange.<br>
                  They will know the same information and why they should use it.</p></center>";
        }

        if ($session['route'] == 'upgrade-to-silver' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_upgrade-to-silver>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page asks the member to purchase the Silver package from Karatbars.<br>
                  The member is instructed to buy it in their Karatbars backoffice.</p></center>";
        }

        if ($session['route'] == 'promote-3' && !isset($post['action'])) {
            echo "<center><a href=index.php?page=promote-3-pay>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will ask the member to pay a $175 one time advertising fee.<br>
                  The fee grows and manages a team indefinitely.</p></center>";
        }

        if ($session['route'] == 'promote-3-pay' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_promote-3>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will ask the member to pay a $175 one time advertising fee.<br>
                  The fee grows and manages a team indefinitely.</p></center>";
        }

        if ($session['route'] == 'refer-3' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_refer-3>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page explains StepsTEAM.com is referring leads to the member's Karatbars link.<br>
                  This page is displayed until a new recruit registers in the member's downline and pays $99.</p></center>";
        }

        if ($session['route'] == 'proof-3' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_proof-3>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page proves that StepsTEAM.com recruited and registered a new affiliate.<br>
                  The member is instructed to check their Karatbars backoffice for proof.<br>
                  This means the second team has been started.</p></center>";
        }

        if ($session['route'] == 'purchase-product' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_purchase-product>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page proves that StepsTEAM.com recruited and registered a new affiliate.<br>
                  The member is instructed to check their Karatbars backoffice for proof.<br>
                  This means the second team has been started.</p></center>";
        }

        if ($session['route'] == 'upgrade-to-gold' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_upgrade-to-gold>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page asks the member to purchase the Gold package from Karatbars.<br>
                  The member is instructed to buy it in their Karatbars backoffice.</p></center>";
        }

        if ($session['route'] == 'promote-4' && !isset($post['action'])) {
            echo "<center><a href=index.php?page=promote-4-pay>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will ask the member to pay a $200 one time advertising fee.<br>
                  Click the Paypal button and enter test@drew.com and Taste@123 to proceed<br>
                  The member is instructed to buy it in their Karatbars backoffice.</p></center>";
        }

        if ($session['route'] == 'promote-4-pay' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_promote-4>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page will ask the member to pay a $200 one time advertising fee.<br>
                  Click the Paypal button and enter test@drew.com and Taste@123 to proceed<br>
                  The member is instructed to buy it in their Karatbars backoffice.</p></center>";
        }

        if ($session['route'] == 'refer-4' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_refer-4>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page explains StepsTEAM.com is referring leads to the member's Karatbars link.<br>
                  This page is displayed until a new recruit registers in the member's downline and pays $99.</p></center>";
        }

        if ($session['route'] == 'proof-4' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_proof-4>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page proves that StepsTEAM.com recruited and registered a new affiliate.<br>
                  The member is instructed to check their Karatbars backoffice for proof.<br>
                  This means the second team has been started.</p></center>";
        }

        if ($session['route'] == 'upgrade-to-vip' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_upgrade-to-vip>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page asks the member to purchase the VIP package from Karatbars.<br>
                  The member is instructed to buy it in their Karatbars backoffice.</p></center>";
        }

        if ($session['route'] == 'recruit-1' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_recruit-1>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page asks the member to recruit another member (who pays $99).<br>
                  The member will use a sponsor link like http://StepsTEAM.com?s=member</p></center>";
        }

        if ($session['route'] == 'praise-1' && !isset($post['action'])) {
            echo "<center><a href=index.php?demo=complete_praise-1>" . $style . "Click here to proceed</p></a>
                  " . $style . "This page praises the member for recruiting a paid member.</p></center>";
        }

        if ($session['route'] && !isset($post['action'])) {
            echo "<hr color=#000000>";
        }

        // Update the database to move through the demo
        if (isset($_GET['demo'])) {
            update_demo($_GET['demo']);
            refresh();
        }
    }
}
