<?php

declare(strict_types=1);

namespace AliasAPI\Demo;

function update_demo($demo)
{
    if (defined('MODE') && MODE == 'demo') {
        $i = 0;
        $stop = 'no';

        $demo = str_replace('complete_', '', $demo);

        $demo_array = configure_demo_array();

        $total_inserts = count($demo_array);

        do {
            $in = $demo_array[$i];

            if (isset($in['table']) && $in['table'] == 'user_steps') {
                insert_user_step($in[0], $in[1], $in[2]);
            } elseif (isset($in['table']) && $in['table'] == 'user_payments') {
                if ($demo == 'refund-1-returned'
                    && isset($in['type']) && $in['type'] == 'refund') {
                    $demo = str_replace('-returned', '', $demo);
                    insert_transaction($in);
                    delete_user_step(1, 'promote-1');
                } elseif ($in['type'] !== 'refund') {
                    list($last_type) = fetch_row("SELECT `type` FROM `user_payments`
                                                     WHERE `uid` = '1'
                                                     AND `step` = 'promote-1'
                                                     ORDER BY `pid` DESC
                                                     LIMIT 1 ");
                    if ($last_type !== 'buy') {
                        insert_transaction($in);
                    }
                }
            } elseif (isset($in['table']) && $in['table'] == 'users') {
                insert_user($in['email'], '', $in['capture_merchant']);
                update_sponsor(2, 'ddemo');
            }

            list($done) = fetch_row("SELECT `step` FROM `usersteps`
                                       WHERE `step` = '" . $demo . "'
                                       AND `uid` = '1'
                                       LIMIT 1 ");

            if (!$done) {
                query("$q");
                $i++;
            }

            if ($done == $demo) {
                $stop = 'yes';
            }

            if ($i == $total_inserts) {
                $stop = 'yes';
            }
        } while ($stop !== 'yes');

        refresh();
    }
}
