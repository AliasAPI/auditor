<?php

declare(strict_types=1);

if (!function_exists('decode_json')) {
    function decode_json($json)
    {
        $json = trim($json);

        $json = utf8_encode($json);

        // Remove BOM characters
        $json = str_replace("\xEF\xBB\xBF", '', $json);

        // Convert the JSON into an array
        $array = (array) json_decode($json, true);

        return $array;
    }
}
