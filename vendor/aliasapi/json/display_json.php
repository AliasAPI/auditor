<?php

declare(strict_types=1);

if (!function_exists('display_json')) {
    function display_json($json)
    {
        // This may already be done in the set_initial_settings();
        if (! headers_sent()) {
            header('Content-Type: application/json; charset=UTF-8');
        }

        // Display the json response.
        echo $json;

        // Don't allow any more processing so the displayed json is valid.
    }
}
