<?php

declare(strict_types=1);

if (!function_exists('encode_json')) {
    function encode_json($array, $pretty=true)
    {
        if ($pretty) {
            // Display formatted JSON that is easier for humans to read
            $json = json_encode($array, JSON_PRETTY_PRINT);
        } else {
            // Remove all the extra lines and spaces to reduce output
            $json = json_encode($array);
            $json = preg_replace('/\\n/', ' ', $json);
            $json = preg_replace('!\s+!', ' ', $json);
        }

        $json = utf8_encode($json);

        return $json;
    }
}
