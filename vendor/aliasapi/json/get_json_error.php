<?php

declare(strict_types=1);

if (!function_exists('get_json_error')) {
    function get_json_error()
    {
        $error_key = json_last_error();

        static $json_error_list = array(
     // The current error codes are as follows:
      JSON_ERROR_NONE             => '',
      JSON_ERROR_DEPTH            => 'JSON_ERROR_DEPTH: Maximum stack depth exceeded',
      JSON_ERROR_STATE_MISMATCH   => 'JSON_ERROR_STATE_MISMATCH: Underflow or the modes mismatch',
      JSON_ERROR_CTRL_CHAR        => 'JSON_ERROR_CTRL_CHAR: Unexpected control character found',
      JSON_ERROR_SYNTAX           => 'JSON_ERROR_SYNTAX: Syntax error, malformed JSON',
      JSON_ERROR_UTF8             => 'JSON_ERROR_UTF8: Malformed UTF-8 characters, possibly incorrectly encoded'
    );

        if ($error_key == 0) {
            return false;
        } elseif ($error_key && array_key_exists($error_key, $json_error_list)) {
            return $json_error_list[$error_key];
        } else {
            return "Unknown JSON Error [$error_key]";
        }
    }
}
