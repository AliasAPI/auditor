<?php

declare(strict_types=1);

if (!function_exists('humanize_json')) {
    function humanize_json($json)
    {
        $array = decode_json($json);

        // Format the json with pretty line returns
        $new_json = encode_json($array, 'pretty');

        // Strip out the ugly characters
        $new_json = preg_replace("/\ \[\r/", "\r", $new_json);
        $new_json = preg_replace("/  ]\ /", "\r", $new_json);
        $new_json = str_replace('{', '', $new_json);
        $new_json = str_replace('},', '', $new_json);
        $new_json = str_replace("}", '', $new_json);
        $new_json = str_replace("\"", '', $new_json);
        $new_json = str_replace(",", '', $new_json);
        $new_json = stripslashes($new_json);
        // Added 03-05-19
        $new_json = preg_replace("/\[\n/", "\r", $new_json);
        $new_json = preg_replace("/]\n/", "\r", $new_json);
        $new_json = preg_replace("/]/", "\r", $new_json);

        return $new_json;
    }
}
