<?php
// 2DO+++ Implement sanitize_json()j
// https://stackoverflow.com/questions/25983090/is-sanitizing-json-necessary
// pass expected list of properties and optional maxLen
// returns obj or null
/*

declare(strict_types=1);

if (!function_exists('humanize_json')) {
function sanitize_json($str, $propArray, $maxLen) {
    var parsedObj, safeObj = {};
    try {
        if (maxLen && str.length > maxLen) {
            return null;
        } else {
            parsedObj = JSON.parse(str);
            if (typeof parsedObj !== "object" || Array.isArray(parsedObj)) {
                safeObj = parseObj;
            } else {
                // copy only expected properties to the safeObj
                propArray.forEach(function(prop) {
                    if (parsedObj.hasOwnProperty(prop)) {
                        safeObj[prop] = parseObj[prop];
                    }
                });
            }
            return safeObj;
        }
    } catch(e) {
        return null;
    }
}
}
*/
