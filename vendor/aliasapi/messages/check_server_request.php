<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

function check_server_request($request)
{
    if (empty($request)) {
        respond(501, ["The server request is not set."]);
    }
}
