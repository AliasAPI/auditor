<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use GuzzleHttp\Psr7\ServerRequest;

// method, headers, version,  params, cookie, body
function get_server_request(): array
{
    $request = ServerRequest::fromGlobals();

    // $auth = $request->getHeaderLine('auth');
    $method = $request->getMethod();
    $headers = $request->getHeaders();
    $version = $request->getProtocolVersion();
    $cookie = $request->getCookieParams();
    $params = $request->getQueryParams();
    $body = (string) $request->getBody();

    if (isset($headers['accept'][0]) && $headers['accept'][0] == 'application/json') {
        $body = (array) json_decode($body, true);
    }

    return [
        'method' => $method,
        'headers' => $headers,
        'version' => $version,
        'params' => $params,
        'cookie' => $cookie,
        'body' => $body
    ];
}
