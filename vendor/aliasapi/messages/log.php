<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

function log(array $message, int $debug_level): void
{
    global $only_set_in_log_function;

    // The higher the debug_detail_level, the more detailed the comments get.
    if (\defined('DEBUG_DETAIL_LEVEL') && DEBUG_DETAIL_LEVEL > 0 && DEBUG_DETAIL_LEVEL >= $debug_detail) {
        if (! isset($only_set_in_log_function)) {
            $only_set_in_log_function = [];
        }

        // Get the referring class method or function
        $source = backtrace(2);
        $message = (string) \print_r($message, true);
        $log_message = "$source $message";

        // Just load into memory until processing is done
        $only_set_in_log_function[] = $log_message;
    }
}
