<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

// use GuzzleHttp\Psr7\Request;

 use GuzzleHttp\Client;

 function request(string $url, array $payload)
 {
     $ch = curl_init($url);

     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

     // curl_setopt($ch, CURLOPT_POST, 1);

     // curl_setopt($ch, CURLOPT_URL, $url);

     $payload = json_encode($payload, JSON_PRETTY_PRINT);

     curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

     $result = trim(curl_exec($ch));

     sayd($result);
     curl_close($ch);

     return array($http_code, $body);
 }
