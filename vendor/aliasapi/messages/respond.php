<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use GuzzleHttp\Psr7\Response;

// https://stackoverflow.com/questions/33304790/emit-a-response-with-psr-7
// $status = 200,
// array $headers = [],
// $body = null,
// $version = '1.1',
// $reason = null
function respond(int $status_code, array $body)
{
    $headers = [
        'Content-Type' => 'application/json; charset=UTF-8',
        'Cache-Control' => 'no-cache, must-revalidate'
    ];
    // https://github.com/pulledbits/response/blob/master/src/Factory.php
    $finfo = new \finfo(FILEINFO_MIME);
    // 2DO+++ replace this

    // 2DO+++ Add the word "ERROR" if there is an error
    
    // 2DO+++ Add the tag to the response

    // convert to json
    $body = (string) json_encode($body, JSON_PRETTY_PRINT);
    // sign
    // encrypt

    $response = new Response($status_code, $headers, $body, '1.1', $reason = null);

    if (headers_sent()) {
        throw new RuntimeException('Headers were already sent. The response could not be emitted!');
    }

    // Step 1: Send the "status line".
    $statusLine = sprintf(
        'HTTP/%s %s %s',
        $response->getProtocolVersion(),
        $response->getStatusCode(),
        $response->getReasonPhrase()
    );

    header($statusLine, true); /* The header replaces a previous similar header. */

    // Step 2: Send the response headers from the headers list.
    foreach ($response->getHeaders() as $name => $values) {
        $responseHeader = sprintf(
        '%s: %s',
        $name,
        $response->getHeaderLine($name)
    );
        header($responseHeader, false); /* The header doesn't replace a previous similar header. */
    }

    // Step 3: Output the message body.
    // https://stackoverflow.com/questions/33304790/emit-a-response-with-psr-7
    echo $response->getBody();
    exit();
}
