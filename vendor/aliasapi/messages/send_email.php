<?php


function send_email( $email )
{
    /*
     * CONCEPT:
     * Sending mail from hosting accounts and localhost is problematic.
     * send_email() sends the email using CURL through mailgun.com
     * The MAILGUN_API_KEY is defined in BASEPATH . /config.php
     * The MAILGUN_API_URL is defined in BASEPATH . /config.php
     *
     * EXAMPLE:
     *
     * CAUTION:
     * Each person that uses this code should have their own account.
     * The limits are 300 emails per day and 10,000 emails per month.
     */


    // Send email is YES or NO (so that sending email can be suspended for development purposes)
    check_enumeration( 'sendemail', SEND_EMAIL, array('no', 'yes') );

    if( SEND_EMAIL === 'yes' )
    {
        /*  check_regular_expression( 'from_email', $email[ 'from' ], '/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/' );
          check_regular_expression( 'to_email', $email[ 'to' ], '/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/' );
          check_regular_expression( 'reply-to', $email[ 'reply-to' ], '/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/' ); */

        // You can just define it here for now.
        /* if( !define( MAILGUN_API_KEY ) )
          {
          echo ( "The Mailgun API key is not set in the config file." );
          } */

        /* if( !define( MAILGUN_API_URL ) )
          {
          echo ( "The Mailgun API URL is not set in the config file." );
          } */

        $search = array('[login_link]', '[message]');
        $replace = array($email[ 'login_link' ], $email[ 'message' ]);

        // Include the html for the email.
        $email_body = file_get_contents( $email[ 'html' ], TRUE );
        $email_body = str_replace( $search, $replace, $email_body );

        $email[ 'h:Reply-To' ] = $email[ 'reply-to' ];
        $options[ 'html' ] = $email_body;
        $options[ 'text' ] = $email[ 'message' ];
        $options[ 'subject' ] = $email[ 'subject' ];
        $options[ 'to' ] = $email[ 'to' ];
        $options[ 'from' ] = $email[ 'sender' ] . " <" . $email[ 'from' ] . ">";

        // print_r( $email ); die();

        $curl = curl_init();

        curl_setopt( $curl, CURLOPT_URL, MAILGUN_API_URL );
        curl_setopt( $curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
        curl_setopt( $curl, CURLOPT_USERPWD, 'api:' . MAILGUN_API_KEY );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $curl, CURLOPT_CONNECTTIMEOUT, 10 );
        curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, 0 );
        curl_setopt( $curl, CURLOPT_POST, TRUE );
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $options );

        $json = curl_exec( $curl );
        // if($errno = curl_errno($curl)) {
        // $error_message = curl_error($curl);
        // echo "cURL error ({$errno}):\n {$error_message}";
        //   }
        curl_close( $curl );

        // echo "<hr>json result = $json<hr>";

        return $json;
    }
    else
    {
        echo "<center>Warning: SEND_EMAIL is set to [ " . SEND_EMAIL . " ]</center>";
    }
}


?>
