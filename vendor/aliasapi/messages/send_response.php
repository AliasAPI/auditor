<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use Psr\Http\Message\ResponseInterface;

// use Psr\Http\Message\ResponseInterface;
// use Psr\Http\Message\ServerRequestInterface;
// use Zend\Diactoros\Response;
// use Zend\Diactoros\Response\EmitterInterface;
// use Zend\Diactoros\Response\JsonResponse;
// use Zend\Diactoros\Response\SapiEmitter;
// use Zend\Diactoros\Response\SapiStreamEmitter;
// * - Protocol version
// * - Status code and reason phrase
// * - Headers
// * - Message body

/**
 * Send the response to the client
 *
 * @param ResponseInterface $response
 */
function send_response(ResponseInterface $response)
{
    // $response->withHeader('Location', (string)$to)->withStatus($status);
    // $response = $response->withStatus(418, "I'm a teapot");

    // Send response
    if (!headers_sent()) {
        // Headers
        foreach ($response->getHeaders() as $name => $values) {
            foreach ($values as $value) {
                header(sprintf('%s: %s', $name, $value), false);
            }
        }
        // Set the status _after_ the headers, because of PHP's "helpful" behavior with location headers.
        // Status
        header(sprintf(
            'HTTP/%s %s %s',
            $response->getProtocolVersion(),
            $response->getStatusCode(),
            $response->getReasonPhrase()
        ));
    }
    // Body
    if (!$this->isEmptyResponse($response)) {
        $body = $response->getBody();
        if ($body->isSeekable()) {
            $body->rewind();
        }
        $settings       = $this->container->get('settings');
        $chunkSize      = $settings['responseChunkSize'];
        $contentLength  = $response->getHeaderLine('Content-Length');
        if (!$contentLength) {
            $contentLength = $body->getSize();
        }
        if (isset($contentLength)) {
            $amountToRead = $contentLength;
            while ($amountToRead > 0 && !$body->eof()) {
                $data = $body->read(min($chunkSize, $amountToRead));
                echo $data;
                $amountToRead -= strlen($data);
                if (connection_status() != CONNECTION_NORMAL) {
                    break;
                }
            }
        } else {
            while (!$body->eof()) {
                echo $body->read($chunkSize);
                if (connection_status() != CONNECTION_NORMAL) {
                    break;
                }
            }
        }
    }
}
