
AliasAPI\Money

Overview
    AliasAPI is a microservice that facilitates payments
    and refunds between customers and Service Providers.
    It uses the Omnipay multi-gateway payment processing
    library. Providers may use several payment gateways.
    AliasAPI dramatically reduces the risks related to
    having a centralized account and it allows Providers
    to get paid immediately and directly for services.

Limitations
    Customers pay Providers directly using a link
    Customers can easily cancel an order prior to paying
    Customers can get refunds by clicking a link
    Customers cannot get refunds after the Refund step
    Refunds are only paid on prior successful payments
    Payment status can be retrieved by the client server
    All payments are one-time payments and do not recur

Benefits
    Providers get paid up front to make subsequent sales
    Providers keep pay in excess of customer acquisition cost
    Sanitized transaction data is automatically backed up
    Providers have no risk of late payments by the Client    
    The Client can refer additional customers to the Marketer   
    The Marketer account is immune problems in other accounts
    All AliasAPI transactions can be exported for paying taxes
    The Marketer avoids providing an SSN/EIN to the Client
    AliasAPI can be upgraded to handle 30+ payment processors
    AliasAPI is designed to automatically scale with services
    The Client agrees to automatically purchase more services
    A frontend is provided for payment processor approval

Security
    The code is strictly limited to required functionality
    Sensitive data is removed before data is transmitted
    Transactions are recorded using customer alias ids
    The Client server signs data to prove authenticity
    The Client server encrypts data to prevent reading
    The Client server transmits using SSL encryption
    The AliasAPI server is only accessed by Providers
    All passwords are located in the .alias.json file
    The AliasAPI server decrypts the transmitted data
    The AliasAPI server checks the password of the Alias
    The AliasAPI checks the authorization of the request
    The AlaisAPI checks to prevent replay attacks
    The AliasAPI uses very little code to improve security
    The AliasAPI uses clear procedural code for easy review
    AliasAPI removes sensitive data before it is transmitted
    AliasAPI signs the data to prove it is authentic
    AliasAPI encrypts the data to prevent reading
    AliasAPI transmits the data using SSL encryption
    Each transaction requires the following 10 secret keys:
        client key pair for signing (2)
        server key pair for signing (2)
        derived symmetric key for encrypting (1)
        keypair for SSL (secure socket layer) sending (2)
        payment gateway username password for orders (2)
        api password (1)
        No order or refund can be completed without all 10.

Liability
    The Marketer is protected using standard disclaimers


          HERE IS HOW THE PURCHASE ORDER FLOW WORKS

Payments
    The Customer clicks a link to buy services on the Client website
    The Customer is redirected to the AliasAPI of the Service Provider
    The AliasAPI performs a lot of security checks and data validations
    AliasAPI prepares the purchase order data for the Payment Gateway
    AliasAPI creates an order on the Payment Gateway
    AliasAPI redirects the Customer to the Payment Gateway's website
    The Customer reviews and confirms the purchase order
    The Payment Gateway redirects the Customer back to the AliasAPI
    The AliasAPI completes the order with the Payment Gateway
    The AliasAPI records the transaction data and sends it to backup
    The AliasAPI redirects the Customer back to the Client Website
    The Customer is provided with a Thank You page for purchasing

Refunds
    In a later step, the Customer can click a link to get a refund
    The Customer cannot get a refund once they pass the Refund step

                    TOUR THE ALIASAPI\MONEY CODE BASE

Code Structure
    The AliasAPI\Money module uses the Pipeline Design Pattern:
    https://medium.com/@aaronweatherall/the-pipeline-pattern-for-fun-and-profit-9b5f43a98130
    https://github.com/thephpleague/pipeline
    It makes the code extremely easy to follow, comprehend, and review.
    In short, the data is processed in a sequence like an assembly line.
    A "Payload" object is passed from one class throughout the process.
    The Payload validates and accumulates data as it gets processed.
    Search the code for "tour:Payload" to review the payload object class.
    Search the code for "tour:Pipeline" to review the pipeline sequence.

    The Pipeline file shows how the functionality is strictly limited.
        AliasAPI\Money can only:
            Create Customer Purchase Orders (tour:CreatePurchase)
            Cancel Customer Purchase Orders (tour:CancelPurchase)
            Complete Customer Purchase Orders (tour:CompletePurchase)
            Refund Customer Purchase Orders (tour:RefundPurchase)
            Update a Customer Transaction (tour:UpdateTransaction)

        AliasAPI\Money cannot make purchases using the Provider's account.

If you have any questions, please do not hesitate to contact me:
Drew Brown drewbrownjr@gmail.com
