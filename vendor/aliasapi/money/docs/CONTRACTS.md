
GET PUBLIC KEY #################################################################

    REQUEST
        METHOD
            POST
            
        HEADER
            {
                "username": "SandboxRest",
                "password": "api_pass"
            }
        BODY
            {
                "actionS": "get public key",
                "tag": "KEYTAG",
                "api_pass": "api_pass",
                "client_url": "http://localhost/dogpatch",
                "public_key": "JupkgBUHtnv8DtF-_zmPGgPrG_rrAMHjv0A7uLTh_-s="
            }

    RESPONSE
        HEADER
            {
                "Content-Type" => "application/json; charset=utf-8",
            }
        BODY
            {
                "status": "201",
                "tag": "KEYTAG",
                "server_url": "http://alias.api/api",
                "public_key": "tTsSfdTh4f0xSvD4TfnEAbqL8rXBA0j4UOEfuchwuSw="
            }








CREATE PURCHASE ################################################################

    REQUEST
        actionS = create purchase
        tag

        ally
            alias
            user
            cart

        purchase
            amount
            currency
            description

        environment
            server_url

        gateway_config
            gateway
            client_user
            client_pass
            test_mode

        database_config
            dsn
            username
            password
            frozen
            partial

    RESPONSE
        error
        tag


CANCEL PURCHASE ################################################################a



COMPLETE PURCHASE ##############################################################

    REQUEST
        actionS = complete purchase
        paymentId
        PayerID
        environment
            tag

    RESPONSE
        error
        tag


REFUND PURCHASE ################################################################

    REQUEST
        actionS = refund purchase
        ally
            alias
            user
            cart
        tag

    RESPONSE
        error
        tag


REQUEST ALLY ###################################################################

    REQUEST
        actionS = select ally
        ally
            alias
            user
            ?cart
        tag

    RESPONSE
        transactions
        error
        tag
