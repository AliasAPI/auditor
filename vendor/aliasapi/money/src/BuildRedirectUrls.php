<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class BuildRedirectUrls
{
    public function __invoke(Payload $payload)
    {
        try {
            // Get the purchase details from the Payload class
            $order = $payload->getOrder();
            $server_url = $payload->getServerUrl();
            $tag = $payload->getTag();

            // The client_url is already set.

            $cancel_url = $server_url;
            $cancel_url .= "/cancel/";
            $cancel_url .= '?tag=' . $tag;

            $return_url = $server_url;
            $return_url .= '/complete/';
            $return_url .= '?tag=' . $tag;

            // Add the redirect_urls to the $order

            $order['cancel_url'] = $cancel_url;
            $order['return_url'] = $return_url;

            $payload->setStatusCode(201);
            $payload->log("Redirect Urls Built.", 4);
            
            // Update the purchase details with the redirect_urls
            $payload->setOrder($order);

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error while Building URLs.";
            $error[] = "Exception Type: " . get_class($ex) . " ";
            $error[] = "Error Message: " . $ex->getMessage() . " ";
            $payload->setError(500, $error);
            return $payload;
        }
    }
}
