<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class CompletePurchase
{
    public function __invoke(Payload $payload)
    {
        // Complete the payment after the user pays at the purchase at payment_gateway
        // https://github.com/thephpleague/omnipay-paypal/blob/master/src/Message/RestCompletePurchaseRequest.php

        try {
            $tag = $payload->getTag();

            $gateway = $payload->getGateway();

            $paymentid = $payload->getPaymentId();

            $payerid = $payload->getPayerID();

            // tour:CompletePurchase
            $purchase = $gateway->completePurchase(
                array(
                    'transactionReference' => $paymentid,
                    'payerId'             => $payerid
                )
            );

            $reply = $purchase->send();

            if ($reply->isSuccessful()) {
                // The customer has successfully paid.
                $data = $reply->getData();

                // Transactions table columns
                // id alias user cart tag type transactionid amount currency fee status saleid refundid created updated redirect_url
                $row = array();
                $row['transactionid'] = $data['id'];
                $row['type'] = $data['intent'];
                $row['amount'] = $data['transactions'][0]['amount']['total'];
                $row['currency'] = $data['transactions'][0]['amount']['currency'];
                $row['fee'] = $data['transactions'][0]['related_resources'][0]['sale']['transaction_fee']['value'];

                $row['status'] = $data['transactions'][0]['related_resources'][0]['sale']['state'];
                $row['saleid'] = $data['transactions'][0]['related_resources'][0]['sale']['id'];
                $row['refundid'] = '';

                $row['created'] = $data['transactions'][0]['related_resources'][0]['sale']['create_time'];

                $datetime = new \DateTime('now', new \DateTimeZone('UTC'));
                $row['updated'] = $datetime->format('Y-m-d H:i:sP');
                $row['redirect_url'] = '';
                // $row['description'] = $data['transactions'][0]['description'];

                $payload->setStatusCode(201);
                $payload->log("Tag [ " . $tag . " ] Transaction Completed.", 7);

                // Add to Payload
                $payload->setTransaction($row);
            } else {
                $payload->setError(424, [$reply->getMessage()]);
            }

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error while Completing a Purchase.\n";
            $error[] = "Exception Type: " . get_class($ex) . "\n";
            $error[] = "Error Message: " . $ex->getMessage() . "\n";
            $payload->setError(424, $error);
            return $payload;
        }
    }
}
