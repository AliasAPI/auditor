<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

class ConnectORM
{
    public function __invoke(Payload $payload)
    {
        try {
            // 2DO+++ Learn how to check to see  if the class is already declared.
            // Then consider using this to declare it globally:
            // class_alias('\RedBeanPHP\Facade', '\R');
            if (!defined("REDBEAN_MODEL_PREFIX")) {
                define('REDBEAN_MODEL_PREFIX', '');
            }

            $database_config = $payload->getDatabaseConfig();

            if (! isset($database_config['dsn'])) {
                sayd($database_config);
            }

            $connected = CrudTable\check_connect_orm();

            if (! $connected) {
                $RedBean = CrudTable\connect_orm($database_config);
            }

            $payload->setStatusCode(201);
            $payload->log("ORM Connected.", 4);

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error while connecting to the ORM.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            $payload->setError(500, $error);
            return $payload;
        }
    }
}
