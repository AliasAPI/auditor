<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use Omnipay\Omnipay;

class CreateGateway
{
    public function __invoke(Payload $payload)
    {
        try {
            $gateway_config = $payload->getGatewayConfig();

            // Create the gateway for the PayPal RestGateway
            // PayPal_Rest is the one that works for the PayPal REST API
            $gateway = \Omnipay\Omnipay::create($gateway_config['gateway']);
            // echo "<pre>"; var_dump($gateway);

            $payload->setGateway($gateway);

            $payload->setStatusCode(201);
            $payload->log("Gateway Created.", 4);

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error while Creating an Omnipay Gateway.";
            $error[] = "Exception Type: " . get_class($ex) . " ";
            $error[] = "Error Message: " . $ex->getMessage() . " ";
            $payload->setError(500, $error);
            return $payload;
        }
    }
}
