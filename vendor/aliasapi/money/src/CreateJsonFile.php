<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudJson as CJ;

class CreateJsonFile
{
    public function __invoke(Payload $payload)
    {
        try {
            // Transactions table columns
            // id alias user cart tag type transactionid amount currency fee status saleid refundid created updated redirect_url
            $transaction_row = $payload->getTransactionRow();

            $exists = CJ\check_file_exists($transaction_row['tag']);

            if ($exists) {
                CJ\delete_json_file($transaction_row['tag']);
            }
            $file_path = CJ\get_with_json_path($transaction_row['tag']);
            // Store the transaction details so that when the user returns, the related transaction is found.
            $result = CJ\create_json_file($file_path, $transaction_row);

            if ($result) {
                $payload->setStatusCode(201);
                $payload->log("Json File Created.", 3);
            }

            return $payload;
        } catch (\Exception $ex) {
            // CAUTION: Displaying an error here is only visible to the payment gateway
            $error[] = "There was an error while creating a JSON file.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            $payload->setError(424, $error);
            return $payload;
        }
    }
}
