<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\Money as Money;

class CreatePayload
{
    public function __invoke($request)
    {
        $payload = new Money\Payload();
        $payload->setOkay(true);
        $payload->setRequest($request);

        $payload->setStatusCode(201);
        $payload->log("Payload Created.", 3);

        return $payload;
    }
}
