<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class CreatePurchase
{
    public function __invoke(Payload $payload)
    {
        try {
            $gateway = $payload->getGateway();

            $order = $payload->getOrder();
            // tour::CreatePurchase
            $purchase_order = $gateway->purchase(
                array(
                    'amount' =>  "" . $order['amount'] . "",
                    'currency' => "" . $order['currency'] . "",
                    'description' => "" . $order['description'] . "",
                    'cancelUrl' => "" . $order['cancel_url'] . "",
                    'returnUrl' => "" . $order['return_url'] . "",
                )
            );

            // sayd($purchase_order);
             Messages\log("Successfully created an Omnipay Purchase", 1);
            $payload->setPurchaseOrder($purchase_order);

            $payload->setStatusCode(201);
            $payload->log("Created Purchase.", 7);

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "CreatePurchase: There was an error while creating an Omnipay Purchase order.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            $jsson_error = json_encode($error, JSON_PRETTY_PRINT);
            $payload->setError(424, $error);
            return $payload;
        }
    }
}
