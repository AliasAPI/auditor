<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudJson as CJ;

class DeleteJsonFile
{
    public function __invoke(Payload $payload)
    {
        try {
            $tag = $payload->getTag();

            $exists = CJ\check_file_exists($tag);
            if ($exists) {
                CJ\delete_json_file($tag);

                $payload->setStatusCode(205);
                $payload->log("Json File Deleted.", 4);
            } else {
                $payload->log("No Json File Deleted.", 4);
            }

            return $payload;
        } catch (\Exception $ex) {
            // CAUTION: Displaying an error here is only visible to the payment gateway
            $error[] = "There was an error while deleting a JSON file.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            $payload->setError(424, $error);
            return $payload;
        }
    }
}
