<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

class DeletePurchase
{
    public function __invoke(Payload $payload)
    {
        try {
            $transaction = $payload->getTransaction();

            if ($transaction['status'] !== 'completed') {
                // Insert a new transaction row if it does not exist
                $result = CrudTable\delete_rows('transactions', $transaction);

                $payload->setStatusCode(205);
                $payload->log("Incomplete Transaction Deleted.", 7);
            } else {
                $payload->log("No Transaction Deleted.", 7);
            }

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error while Deleting a Purchase.\n";
            $error[] = "Exception Type: " . get_class($ex) . "\n";
            $error[] = "Error Message: " . $ex->getMessage() . "\n";
            $payload->setError(424, $error);
            return $payload;
        }
    }
}
