<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class GetOkay
{
    public function __invoke(Payload $payload)
    {
        $okay = $payload->getOkay();

        return $okay;
    }
}
