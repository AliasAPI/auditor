<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class InitializeGateway
{
    public function __invoke(Payload $payload)
    {
        try {
            // Initialise the gateway
            $gateway = $payload->getGateway();

            $gateway_config = $payload->getGatewayConfig();
            //  sayd($gateway_config, $gateway);
            $gateway->initialize(
                array(
                    'clientId' => $gateway_config['client_user'],
                    'secret'   => $gateway_config['client_pass'],
                    'testMode' => $gateway_config['test_mode']
                    )
            );

            $payload->setStatusCode(201);
            $payload->log("Gateway Initialized.", 4);

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error while initializing an Omnipay Gateway.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            $payload->setError(424, $error);
            return $payload;
        }
    }
}
