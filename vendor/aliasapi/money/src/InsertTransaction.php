<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

class InsertTransaction
{
    public function __invoke(Payload $payload)
    {
        try {
            // Transactions table columns
            // id alias user cart tag type transactionid amount currency fee status saleid refundid created updated redirect_url
            $transaction_row = $payload->getTransactionRow();

            $row['alias'] = $transaction_row['alias'];
            $row['user'] = $transaction_row['user'];
            $row['cart'] = $transaction_row['cart'];
            $row['tag'] = $transaction_row['tag'];

            $row['type'] = $transaction_row['type'];
            $row['transactionid'] = $transaction_row['transactionid'];
            $row['amount'] = $transaction_row['amount'];
            $row['currency'] = $transaction_row['currency'];
            $row['fee'] = $transaction_row['fee'];

            $row['status'] = $transaction_row['status'];
            $row['saleid'] = $transaction_row['saleid'];
            $row['refundid'] = $transaction_row['refundid'];
            $row['created'] = $transaction_row['created'];
            $row['updated'] = $transaction_row['updated'];
            $row['redirect_url'] = $transaction_row['redirect_url'];

            // Insert a new transaction row if it does not exist
            $record = CrudTable\create_row('transactions', $row);

            $payload->setStatusCode(201);
            $payload->log("Tag [" . $row['tag'] . "] transaction recorded.", 5);

            return $payload;
        } catch (\Exception $ex) {
            // CAUTION: Displaying an error here is only visible to the payment gateway
            $error[] = "There was an error while creating a transaction row.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            $payload->setError(501, $error);
            return $payload;
        }
    }
}
