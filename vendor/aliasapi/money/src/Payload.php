<?php

namespace AliasAPI\Money;

class Payload
{
    protected $actionS;
    protected $ally;
    protected $alias_attributes;
    protected $complete_purchase_ok = false;
    protected $create_purchase_ok = false;
    protected $refund_purchase_ok = false;
    protected $update_purchase_ok = false;
    protected $gateway;
    protected $gateway_config = array();
    protected $order = array();
    protected $purchase_order;
    protected $transaction = array();
    protected $database_config = array();
    protected $transaction_row;
    protected $log = [];
    protected $okay;
    protected $request;
    protected $error;
    protected $status_code;
    protected $redirect_url;
    protected $paymentid;
    protected $payerid;
    protected $process_tag;
    protected $client_url;
    protected $server_url;
    protected $tag;
    public $response = [];

    // https://codereview.stackexchange.com/questions/59051/validating-basic-data-objects

    public function setActionS(string $actionS)
    {
        // Alias, User, Cart
        $this->actionS = $actionS;
        return $this;
    }

    public function getActionS()
    {
        return $this->actionS;
    }


    public function setAliasAttributes(array $alias_attributes)
    {
        if (empty($alias_attributes)) {
            $this->setError(500, ["The payload is empty."]);
        } elseif (!isset($alias_attributes['database_config'])) {
            $this->setError(400, ["The database_config is not set in the request."]);
        } elseif (!isset($alias_attributes['database_config']['dsn'])) {
            $this->setError(400, ["The database_config dsn is not set in the request."]);
        } elseif (!isset($alias_attributes['database_config']['username'])) {
            $this->setError(400, ["The database_config username is not set in the request."]);
        } elseif (!isset($alias_attributes['database_config']['password'])) {
            $this->setError(400, ["The database_config password is not set in the request."]);
        } elseif (!isset($alias_attributes['database_config']['frozen'])) {
            $this->setError(400, ["The database_config frozen is not set in the request."]);
        } elseif (!isset($alias_attributes['database_config']['partial'])) {
            $this->setError(400, ["The database_config partial is not set in the request."]);
        } elseif (!isset($alias_attributes['gateway_config'])) {
            $this->setError(400, ["The gateway_config is not set in the request."]);
        } elseif (!isset($alias_attributes['gateway_config']['gateway'])) {
            $this->setError(400, ["The gateway_config gateway is not set in the request."]);
        } elseif (!isset($alias_attributes['gateway_config']['client_user'])) {
            $this->setError(400, ["The gateway_config client_user is not set in the request."]);
        } elseif (!isset($alias_attributes['gateway_config']['client_pass'])) {
            $this->setError(400, ["The gateway_config client_pass is not set in the request."]);
        } elseif (!isset($alias_attributes['gateway_config']['test_mode'])) {
            $this->setError(400, ["The gateway_config test_mode is not set in the request."]);
        } else {
            $this->alias_attributes = $alias_attributes;
        }

        return $this;
    }

    public function getAliasAttributes()
    {
        return $this->alias_attributes;
    }


    public function setAlly($ally)
    {
        // Alias, User, Cart
        $this->ally = $ally;
        return $this;
    }

    public function getAlly()
    {
        return $this->ally;
    }


    public function setClientUrl(string $client_url)
    {
        // Alias, User, Cart
        $this->client_url = $client_url;
        return $this;
    }

    public function getClientUrl()
    {
        return $this->client_url;
    }

    public function setCompletePurchaseOK(array $request)
    {
        // Only check to make sure all required values are provided initially
        // The validation checking is done later in the Payload class setters
        if (empty($request)) {
            $this->setError(500, ["The request is empty."]);
        } elseif (!isset($request['actionS'])) {
            $this->setError(501, ["The actionS is not set in the request."]);
        } elseif ($request['actionS'] !== 'complete purchase') {
            $this->setError(501, ["The actionS is not set to [ complete purchase ]."]);
        } elseif (! isset($request['alias_attributes']['gateway_config']['gateway'])) {
            $this->setError(501, ["The gateway is not set in the request."]);
        } elseif ($request['alias_attributes']['gateway_config']['gateway'] == 'PayPal_Rest'
            && !isset($request['paymentid'])) {
            $this->setError(501, ["The paymentid is not set in the request."]);
        } elseif ($request['alias_attributes']['gateway_config']['gateway'] == 'PayPal_Rest'
            && !isset($request['payerid'])) {
            $this->setError(501, ["The payerid is not set in the request."]);
        } elseif (!isset($request['tag'])) {
            $this->setError(501, ["The tag is not set in the request."]);
        } else {
            $this->setCompletePurchaseOK = true;
        }

        return $this;
    }

    public function getCompletePurchaseOK()
    {
        return $this->complete_purchase_ok;
    }

    public function setCreatePurchaseOK(array $request)
    {
        if (empty($request)) {
            $this->setError(500, ["The request is empty."]);
        } elseif (!isset($request['actionS'])) {
            $this->setError(400, ["The actionS is not set in the request."]);
        } elseif ($request['actionS'] !== 'create purchase') {
            $this->setError(400, ["The actionS is not set to [ create purchase ]."]);
        // ally
        } elseif (!isset($request['ally']['alias'])) {
            $this->setError(400, ["The Alias is not set in the request."]);
        } elseif (!isset($request['ally']['user'])) {
            $this->setError(400, ["The user is not set in the request."]);
        } elseif (!isset($request['ally']['cart'])) {
            $this->setError(400, ["The carts is not set in the request."]);
        // order
        } elseif (!isset($request['order']['amount'])) {
            $this->setError(400, ["The order amount is not set in the request."]);
        } elseif (!isset($request['order']['currency'])) {
            $this->setError(400, ["The order currency is not set in the request."]);
        } elseif (!isset($request['order']['description'])) {
            $this->setError(400, ["The order description is not set in the request."]);
        // environment
        } elseif (!isset($request['environment'])) {
            $this->setError(400, ["The environment is not set in the request."]);
        } elseif (!isset($request['environment']['server_url'])) {
            $this->setError(400, ["The environment server_url is not set in the request."]);
        // tag
        } elseif (!isset($request['tag'])) {
            $this->setError(400, ["The tag is not set in the request."]);
        } else {
            $this->create_purchase_ok = true;
        }

        return $this;
    }

    public function getCreatePurchaseOK(): bool
    {
        return $this->create_purchase_ok;
    }

    public function log(string $message, int $level)
    {
        return $this->log[] = [$message, $level];
    }

    public function getLog()
    {
        return $this->log;
    }


    public function setRefundPurchaseOK(array $request, array $last_transaction)
    {
        if (empty($request)) {
            $this->setError(500, ["The request is empty."]);
        } elseif (! isset($request['actionS'])) {
            $this->setError(400, ["The actionS is not set in the request."]);
        } elseif ($request['actionS'] !== 'refund purchase') {
            $this->setError(400, ["The actionS is not set to [ refund purchase ]."]);
        } elseif (! isset($request['alias_attributes']['database_config']['dsn'])) {
            $this->setError(400, ["The alias_attributes database_config dsn is not set."]);
        } elseif (! isset($request['alias_attributes']['database_config']['username'])) {
            $this->setError(400, ["The alias_attributes database_config username is not set."]);
        } elseif (! isset($request['alias_attributes']['database_config']['password'])) {
            $this->setError(400, ["The alias_attributes database_config password is not set."]);
        } elseif (! isset($request['tag'])) {
            $this->setError(400, ["The tag is not set in the request."]);
        } elseif (! isset($last_transaction['amount'])) {
            $this->setError(501, ["The last_transaction amount is not set."]);
        } elseif (! isset($last_transaction['currency'])) {
            $this->setError(501, ["The last_transaction currency is not set."]);
        } elseif ($last_transaction['currency'] == '') {
            $this->setError(501, ["The last_transaction currency is not set."]);
        } elseif (! isset($last_transaction['type'])) {
            $this->setError(501, ["There is no last_transaction type."]);
        } elseif ($last_transaction['type'] !== 'sale') {
            $this->setError(501, ["The last_transaction type is not sale."]);
        } elseif (! isset($last_transaction['refundid'])) {
            $this->setError(501, ["The last_transaction refundid is not set."]);
        } elseif ($last_transaction['refundid'] !== '') {
            $this->setError(400, ["The last_transaction type is not blank."]);
        } elseif (! isset($last_transaction['saleid'])) {
            $this->setError(501, ["The last_transaction saleid is not set."]);
        } elseif ($last_transaction['saleid'] == '') {
            $this->setError(400, ["The last_transaction saleid is blank."]);
        } elseif (! isset($last_transaction['status'])) {
            $this->setError(501, ["The last_transaction status is not set."]);
        } elseif ($last_transaction['status'] !== 'completed') {
            $this->setError(400, ["The last_transaction status is completed."]);
        } else {
            $this->refund_purchase_ok = true;
        }

        return $this;
    }

    public function getRefundPurchaseOK()
    {
        return $this->refund_purchase_ok;
    }


    public function setUpdatePurchaseOK(array $request)
    {
        if (empty($request)) {
            $this->setError(500, ["The request is empty."]);
        } elseif (!isset($request['actionS'])) {
            $this->setError(400, ["The actionS is not set in the request."]);
        } elseif ($request['actionS'] !== 'update purchase') {
            $this->setError(400, ["The actionS is not set to [ create purchase ]."]);
        // ally
        } elseif (!isset($request['ally']['alias'])) {
            $this->setError(400, ["The Alias is not set in the request."]);
        } elseif (!isset($request['ally']['user'])) {
            $this->setError(400, ["The user is not set in the request."]);
        } elseif (!isset($request['ally']['cart'])) {
            $this->setError(400, ["The carts is not set in the request."]);
        // environment
        } elseif (!isset($request['environment'])) {
            $this->setError(400, ["The environment is not set in the request."]);
        } elseif (!isset($request['environment']['server_url'])) {
            $this->setError(400, ["The environment server_url is not set in the request."]);
        } elseif (!isset($request['environment']['client_url'])) {
            $this->setError(400, ["The environment client_url is not set in the request."]);
        // tag
        } elseif (!isset($request['tag'])) {
            $this->setError(400, ["The tag is not set in the request."]);
        } elseif (!isset($request['process_tag'])) {
            $this->setError(400, ["The process_tag is not set in the request."]);
        } else {
            $this->update_purchase_ok = true;
        }

        return $this;
    }

    public function getUpdatePurchaseOK(): bool
    {
        return $this->update_purchase_ok;
    }


    public function setRequest(array $request)
    {
        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }


    public function setStatusCode(int $status_code)
    {
        // Alias, User, Cart
        $this->status_code = $status_code;
        return $this;
    }

    public function getStatusCode()
    {
        return $this->status_code;
    }


    public function setError(int $status_code, array $error)
    {
        // Make InterruptibleProcessor skip the rest of the Pipeline
        $this->setOkay(false);

        $this->status_code = $status_code;

        $this->error = $error;
        return $this;
    }

    public function getError()
    {
        return $this->error;
    }

    public function setOkay(bool $okay)
    {
        // Alias, User, Cart
        $this->okay = $okay;
        return $this;
    }

    public function getOkay()
    {
        return $this->okay;
    }

    public function setParameters(array $request)
    {
        if (isset($request['actionS'])) {
            $this->status_code = 102;
            $this->setActionS($request['actionS']);
            $this->log("actionS [ " . $request['actionS'] . " ] pipe processing.", 7);
        }

        if (isset($request['ally'])) {
            $this->setAlly($request['ally']);
        }

        if (isset($request['alias_attributes']['database_config'])) {
            $this->setDatabaseConfig($request['alias_attributes']['database_config']);
        }

        if (isset($request['alias_attributes']['gateway_config'])) {
            $this->setGatewayConfig($request['alias_attributes']['gateway_config']);
        }

        if (isset($request['order'])) {
            $this->setOrder($request['order']);
        }

        if (isset($request['paymentid'])) {
            $this->setPaymentId($request['paymentid']);
        }

        if (isset($request['payerid'])) {
            $this->setPayerID($request['payerid']);
        }

        if (isset($request['refundid'])) {
            $this->setRefundId($request['refundid']);
        }

        if (isset($request['saleid'])) {
            $this->setSaleId($request['saleid']);
        }

        if (isset($request['tag'])) {
            $this->setTag($request['tag']);
        }

        if (isset($request['process_tag'])) {
            $this->setProcessTag($request['process_tag']);
        } elseif (isset($request['tag'])) {
            $this->setProcessTag($request['tag']);
        }

        if (isset($request['environment']['server_url'])) {
            $this->setServerUrl($request['environment']['server_url']);
        }

        if (isset($request['environment']['client_url'])) {
            $this->setClientUrl($request['environment']['client_url']);
        }

        return $this;
    }

    public function getParameters()
    {
        return $this->okay;
    }


    public function setGateway($gateway)
    {
        $this->gateway = $gateway;
        return $this;
    }

    public function getGateway()
    {
        return $this->gateway;
    }

    public function setGatewayConfig(array $gateway_config)
    {
        $this->gateway_config = $gateway_config;
        return $this;
    }

    public function getGatewayConfig()
    {
        return $this->gateway_config;
    }

    public function setOrder(array $order)
    {
        $this->order = $order;
        return $this;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setPurchaseOrder($purchase_order)
    {
        $this->purchase_order = $purchase_order;
        return $this;
    }

    public function getPurchaseOrder()
    {
        return $this->purchase_order;
    }


    public function setTransaction(array $transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }

    public function getTransaction()
    {
        return $this->transaction;
    }

    public function setDatabaseConfig(array $database_config)
    {
        if (! isset($database_config['dsn'])) {
            $this->setError(501, ["The database_config dsn is not set."]);
        } elseif (! isset($database_config['username'])) {
            $this->setError(501, ["The database_config username is not set."]);
        } elseif (! isset($database_config['password'])) {
            $this->setError(501, ["The database_config password is not set."]);
        } elseif (! isset($database_config['frozen'])) {
            $this->setError(501, ["The database_config frozen is not set."]);
        } elseif (! isset($database_config['partial'])) {
            $this->setError(501, ["The database_config partial is not set."]);
        }

        $this->database_config = $database_config;
        return $this;
    }

    public function getDatabaseConfig()
    {
        return $this->database_config;
    }

    public function setTransactionRow($transaction_row)
    {
        $this->transaction_row = $transaction_row;
        return $this;
    }

    public function getTransactionRow()
    {
        return $this->transaction_row;
    }

    public function setRedirectUrl($redirect_url)
    {
        $this->redirect_url = $redirect_url;
        return $this;
    }

    public function getRedirectUrl()
    {
        return $this->redirect_url;
    }

    public function setServerUrl(string $server_url)
    {
        $this->server_url = $server_url;
        return $this;
    }

    public function getServerUrl()
    {
        return $this->server_url;
    }

    public function setTag(string $tag)
    {
        if (strlen($tag) < 3) {
            $this->setError(400, ["The tag is too short."]);
        }

        $this->tag = $tag;
        return $this;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function setPaymentId($paymentid)
    {
        $this->paymentid = $paymentid;
        return $this;
    }

    public function getPaymentId()
    {
        return $this->paymentid;
    }

    public function setPayerID($payerid)
    {
        $this->payerid = $payerid;
        return $this;
    }

    public function getPayerID()
    {
        return $this->payerid;
    }

    public function setSaleId($saleid)
    {
        $this->saleid = $saleid;
        return $this;
    }

    public function getSaleId()
    {
        return $this->saleid;
    }


    public function setProcessTag(string $process_tag)
    {
        if (strlen($process_tag) < 3) {
            $this->setError(400, ["The process_tag is too short."]);
        }

        // Alias, User, Cart
        $this->process_tag = $process_tag;
        return $this;
    }

    public function getProcessTag()
    {
        return $this->process_tag;
    }


    public function setRefundId($refundid)
    {
        $this->refundid = $refundid;
        return $this;
    }

    public function getRefundId()
    {
        return $this->refundid;
    }


    public function setResponse($response)
    {
        if (empty($response)) {
            $this->setError(501, ["The response is empty."]);
        } elseif (!isset($response['status_code'])) {
            $this->setError(400, ["The status_code is not set in the response."]);
        } elseif (!isset($response['tag'])) {
            $this->setError(400, ["The tag is not set in the response."]);
        } elseif (!isset($response['transaction'])) {
            $this->setError(400, ["The transaction is not set in the response."]);
        } elseif (!isset($response['log'])) {
            $this->setError(400, ["The log is not set in the response."]);
        } elseif (!isset($response['redirect_url'])) {
            $this->setError(400, ["The redirect_url is not set in the response."]);
        } else {
            $this->response = $response;
        }

        return $this;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
