<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use League\Pipeline as PL;

// tour:Pipeline
class PipelineUpdateTransaction
{
    public function __invoke(array $request)
    {
        // tour:UpdateTransaction
        if ($request['actionS'] == 'update transaction') {
            $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetAliasAttributes)
                    ->pipe(new SetUpdateTransactionOK)
                    ->pipe(new SetParameters)
                    ->pipe(new ConnectORM)
                    ->pipe(new SelectTransaction)
                    ->pipe(new DeleteJsonFile)
                    ->pipe(new SetRedirectUrl)
                    ->pipe(new SetResponse);
        }

        try {
            $result = $pipeline->process($request);
            return $result->response;
        } catch (\Exception $ex) {
            $error[] = "There was an error while processing a Pipeline.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            return [500, $error];
        }
    }
}
