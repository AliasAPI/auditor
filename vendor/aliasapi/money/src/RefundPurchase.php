<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class RefundPurchase
{
    public function __invoke(Payload $payload)
    {
        try {
            $process_tag = $payload->getProcessTag();

            $gateway = $payload->getGateway();

            $refund_purchase_ok = $payload->getRefundPurchaseOK();

            $last_transaction = $payload->getTransaction();

            if ($refund_purchase_ok == true) {
                // tour:RefundPurchase
                $refund = $gateway->refund(
                    array(
                    'amount'    => $last_transaction['amount'],
                    'currency'  => $last_transaction['currency'],
                    )
                );

                $refund->setTransactionReference($last_transaction['saleid']);

                $reply = $refund->send();

                if (true == $reply->isSuccessful()) {
                    $data = $reply->getData();

                    // Transactions table columns
                    // id alias user cart tag type transactionid amount currency fee status saleid refundid created updated redirect_url
                    $row['type'] = 'refund';
                    $row['transactionid'] = $data['parent_payment'];

                    $row['amount'] = $data['total_refunded_amount']['value'];
                    $row['fee'] = $data['refund_from_transaction_fee']['value'];
                    $row['currency'] = $data['total_refunded_amount']['currency'];

                    $row['status'] = $data['state'];
                    $row['saleid'] = $data['sale_id'];
                    $row['refundid'] = $data['id'];

                    $row['created'] = $data['create_time'];

                    $payload->setStatusCode(205);
                    $payload->log("Tag [ " . $process_tag . " ] transaction refunded.", 6);

                    $payload->setTransaction($row);
                } else {
                    $payload->setError(424, [$reply->getMessage()]);
                }
            }

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error returned by RefundPurchase.\n";
            $error[] = "Exception Type: " . get_class($ex) . "\n";
            $error[] = "Error Message: " . $ex->getMessage() . "\n";
            $payload->setError(424, $error);
            return $payload;
        }
    }
}
