<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

class SelectTransaction
{
    public function __invoke(Payload $payload)
    {
        try {
            // Transactions table columns
            // id alias user cart tag type transactionid amount currency fee status saleid refundid created updated redirect_url
            $request = $payload->getRequest();

            // If the transaction is a refund, select the parent sale transaction using the tag
            // $tag = (isset($request['process_tag'])) ? $request['process_tag'] : $request['tag'];

            $process_tag = $payload->getProcessTag();

            if ($process_tag) {
                // Select based on column names and values
                $key_pairs_array = [ 'tag' => $process_tag ];

                // Select the unique transaction based on the transaction tag.
                $transaction = CrudTable\read_rows('transactions', $key_pairs_array);

                $transaction = (isset($transaction[0])) ? $transaction[0] : $transaction;

                $payload->setStatusCode(201);
                $payload->log("Tag [ " . $process_tag . " ] Transaction Selected.", 3);

                $payload->setTransaction($transaction);
            } else {
                $payload->setError(400, ["The tag was not set for Select Transaction"]);
            }

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error while Selecting a Transaction.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            $payload->setError(500, $error);
            return $payload;
        }
    }
}
