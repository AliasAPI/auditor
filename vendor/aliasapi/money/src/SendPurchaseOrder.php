<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class SendPurchaseOrder
{
    public function __invoke(Payload $payload)
    {
        try {
            $tag = $payload->getTag();

            $purchase_order = $payload->getPurchaseOrder();

            // Transmit the purchase order to the payment gateway immediately
            $reply = $purchase_order->send();

            if ($reply->isSuccessful()) {
                 Messages\log("Omnipay PayPal Rest Purchase Order Successfully Sent!", 1);

                // Get Token and Redirect to PayPal's Off-site Rest Payment Gateway
                $transaction['transactionid'] = $reply->getTransactionReference();

                $data = $reply->getData();
                // sayd($data);

                $transaction['description'] = $data['transactions'][0]['description'];
                $transaction['type'] = $data['intent'];
                $transaction['amount'] = $data['transactions'][0]['amount']['total'];
                $transaction['status'] = $data['state'];
                $transaction['created'] = $data['create_time'];

                $transaction['redirect_url'] = $reply->getRedirectUrl();
                // sayd($transaction);

                // secret("raw data = [ " . print_r($data, true) . " ]", 1);
                // secret("response = [ " . print_r($transaction, true) . " ]", 1);
                if (isset($transaction['redirect_url'])) {
                    $payload->setRedirectUrl($transaction['redirect_url']);
                }

                $payload->setStatusCode(203);
                $payload->log("Tag [ " . $tag . " ] Purchase Sent.", 6);

                $payload->setTransaction($transaction);
            } else {
                // 2DO+++ Look into Payum's idea of looking for more error messages
                $payload->setError(424, [$reply->getMessage()]);
            }

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "Error while sending the purchase order to the payment gateway.";
            $error[] = "Exception Type: " . get_class($ex) . " ";
            $error[] = "Error Message: " . $ex->getMessage() . " ";
            $payload->setError(424, $error);
            return $payload;
        }
    }
}
