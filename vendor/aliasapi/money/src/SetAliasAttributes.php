<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class SetAliasAttributes
{
    public function __invoke(Payload $payload)
    {
        $request = $payload->getRequest();

        // Only check to make sure all required values are provided initially
        // The validation checking is done later in the Payload class setters
        $payload->setAliasAttributes($request['alias_attributes']);

        $payload->log("Alias Attributes Set.", 3);

        return $payload;
    }
}
