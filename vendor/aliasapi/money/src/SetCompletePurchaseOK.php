<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class SetCompletePurchaseOK
{
    public function __invoke(Payload $payload)
    {
        // Retrieve the Request to allow overwriting
        $request = $payload->getRequest();

        $payload->setCompletePurchaseOK($request);

        $payload->log("Set Complete Purchase to OK.", 5);

        return $payload;
    }
}
