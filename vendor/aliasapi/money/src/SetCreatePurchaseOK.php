<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class SetCreatePurchaseOK
{
    public function __invoke(Payload $payload)
    {
        $request = $payload->getRequest();

        $payload->setCreatePurchaseOK($request);

        $payload->log("Set Create Purchase to OK.", 5);

        return $payload;
    }
}
