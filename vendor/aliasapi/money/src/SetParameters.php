<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class SetParameters
{
    public function __invoke(Payload $payload)
    {
        // Retrieve the request to allow overwriting values
        $request = $payload->getRequest();
        // say($request);
        $payload->setParameters($request);

        $payload->log("Parameters Set.", 4);

        return $payload;
    }
}
