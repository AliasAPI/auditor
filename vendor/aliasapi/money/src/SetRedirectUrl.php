<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class SetRedirectUrl
{
    public function __invoke(Payload $payload)
    {
        try {
            $actions = $payload->getActionS();

            // actions = create purchase | The redirect_url is set in SendPurchaseOrder

            // The following action sends the browser back to the Client website
            if ($actions == 'complete purchase'
                || $actions == 'cancel purchase'
                || $actions == 'refund purchase'
                || $actions == 'update purchase'
            ) {
                $client_url = $payload->getClientUrl();
                $payload->setRedirectUrl($client_url);
            }

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error while Creating the Response.\n";
            $error[] = "Exception Type: " . get_class($ex) . "\n";
            $error[] = "Error Message: " . $ex->getMessage() . "\n";
            $payload->setError(424, $error);
            return $payload;
        }
    }
}
