<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class SetResponse
{
    public function __invoke(Payload $payload)
    {
        try {
            // Http Status Code required
            $response['status_code'] = $payload->getStatusCode();

            // Error
            $error = $payload->getError();
            if ($error) {
                $response['error'] = $error;
            }

            // Tag required
            $response['tag'] = $payload->getTag();

            // Process Tag (actions on another row)
            $process_tag = $payload->getProcessTag();
            if ($process_tag) {
                $response['process_tag'] = $process_tag;
            }

            // Transaction required
            $response['transaction'] = $payload->getTransaction();

            // Log Messages
            $response['log'] = $payload->getLog();

            // Redirect URL required
            $response['redirect_url'] = $payload->getRedirectUrl();

            $payload->setResponse($response);

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error while Creating the Response.\n";
            $error[] = "Exception Type: " . get_class($ex) . "\n";
            $error[] = "Error Message: " . $ex->getMessage() . "\n";
            $payload->setError(424, $error);
            return $payload;
        }
    }
}
