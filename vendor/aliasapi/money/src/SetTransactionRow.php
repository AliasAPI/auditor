<?php

declare(strict_types=1);

namespace AliasAPI\Money;

class SetTransactionRow
{
    public function __invoke(Payload $payload)
    {
        try {
            // CAUTION: Do not use column names ending in _id or ID.
            // Redbean uses _id and will change the nature of the column.
            // Somehow it overwrites ID and changes it to _id as well.
            // $payload['alias'], $payload['user'], $payload['cart'],

            // Transactions table columns
            // id alias user cart tag type transactionid amount currency fee status saleid refundid created updated redirect_url
            $trans = $payload->getTransaction();
            $ally = $payload->getAlly();
            $tag = $payload->getTag();
            $actionS = $payload->getActionS();

            $row = array();
            $row['alias'] = $ally['alias'];
            $row['user'] = $ally['user'];
            $row['cart'] = $ally['cart'];
            $row['tag'] = $tag;

            $row['type'] = (isset($trans['type'])) ? $trans['type'] : '';
            $row['transactionid'] = (isset($trans['transactionid'])) ? $trans['transactionid'] : '';

            // Store refunds as negative values to make queries and math easier
            if (isset($trans['type']) && isset($trans['amount'])) {
                if ($trans['type'] == 'refund') {
                    $amount = $trans['amount'] * -1;
                } else {
                    $amount = $trans['amount'];
                }
            }

            $row['amount'] = (isset($amount)) ? $amount : 0;

            $row['currency'] = (isset($trans['currency'])) ? $trans['currency'] : '';
            $row['fee'] = (isset($trans['fee'])) ? $trans['fee'] : 0;

            $row['status'] = (isset($trans['status'])) ? $trans['status'] : '';
            $row['saleid'] = (isset($trans['saleid'])) ? $trans['saleid'] : '';
            $row['refundid'] = (isset($trans['refundid'])) ? $trans['refundid'] : '';

            $row['created'] = (isset($trans['created'])) ? $trans['created'] : '';

            // Current date/time in the specified time zone.
            $datetime = new \DateTime('now', new \DateTimeZone('UTC'));
            $row['updated'] = $datetime->format('Y-m-d H:i:sP');

            $row['redirect_url'] = (isset($trans['redirect_url'])) ? $trans['redirect_url'] : '';

            $payload->setStatusCode(201);
            $payload->setTransactionRow($row);

            $payload->log("Transaction Row Set.", 4);

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "SetTransactionRow: There was an error setting the transaction row.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            $payload->setError(500, $error);
            return $payload;
        }
    }
}
