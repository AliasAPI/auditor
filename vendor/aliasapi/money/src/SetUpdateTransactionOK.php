<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

class SetUpdateTransactionOK
{
    public function __invoke(Payload $payload)
    {
        try {
            $request = $payload->getRequest();

            $last_transaction = $payload->getTransaction();

            $payload->SetUpdatePurchaseOK($request);

            $payload->log("Update Purchase Set to OK.", 5);

            return $payload;
        } catch (\Exception $ex) {
            $error[] = "There was an error while checking if Updating is OK.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            $payload->setError(500, $error);
            return $payload;
        }
    }
}
