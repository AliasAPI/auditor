<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;
use RedBeanPHP\Facade as R;

class UpdateTransaction
{
    public function __invoke(Payload $payload)
    {
        $result_row = null;

        try {
            // Transactions table columns
            // id alias user cart tag type transactionid amount currency fee status saleid refundid created updated redirect_url
            $result_row = $payload->getTransactionRow();

            // Get the unique tag for the transaction
            $tag = $result_row['tag'];
            // Select the row from database
            $key_pairs_array = CrudTable\read_rows('transactions', ['tag' => $tag]);
            // print_r($key_pairs_array); die();

            // Set the values that can be updated (not including id, of course)
            $update_pairs['alias'] = $result_row['alias'];
            $update_pairs['user'] = $result_row['user'];
            $update_pairs['cart'] = $result_row['cart'];
            $update_pairs['tag'] = $result_row['tag'];

            $update_pairs['type'] = $result_row['type'];
            $update_pairs['transactionid'] = $result_row['transactionid'];
            $update_pairs['amount'] = $result_row['amount'];
            $update_pairs['currency'] = $result_row['currency'];
            $update_pairs['fee'] = $result_row['fee'];

            $update_pairs['status'] = $result_row['status'];
            $update_pairs['saleid'] = $result_row['saleid'];
            $update_pairs['refundid'] = $result_row['refundid'];
            $update_pairs['created'] = $result_row['created'];
            $update_pairs['updated'] = $result_row['updated'];
            $update_pairs['redirect_url'] = $result_row['redirect_url'];
            // print_r($key_pairs_array); die();

            // tour:UpdateTransaction
            $affected_rows = CrudTable\update_rows('transactions', $key_pairs_array[0], $update_pairs);

            $payload->setStatusCode(205);
            $payload->log("[ " . $affected_rows . " ] Transaction Updated.", 6);

            return $payload;
        } catch (\Exception $ex) {
            // CAUTION: Displaying an error here is only visible to the payment gateway
            $error[] = "There was an error while inserting a transaction row.";
            $error[] = "Exception Type: " . get_class($ex) . "";
            $error[] = "Error Message: " . $ex->getMessage() . "";
            $payload->setError(500, $error);
            return $payload;
        }
    }
}
