<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

function alter_step(array $request, array $steps): array
{
    // 2DO+++ replace the $_GET
    $page = (isset($request['params']['page'])) ? htmlspecialchars($request['params']['page']) : 'access';

    if ($page === 'support') {
        $steps['page'] = 'support';
    // If the user is NOT logged in . . .
    } elseif ($steps['page'] === 'access') {
        $steps['page'] = 'access';
    }

    // Alter the route based on the user_steps
    elseif ($steps['page'] === 'promote-1' && $page === 'promote-1-pay') {
        $steps['page'] = 'promote-1-pay';
    } elseif ($steps['page'] === 'promote-2' && $page === 'promote-2-pay') {
        $steps['page'] = 'promote-2-pay';
    } elseif ($steps['page'] === 'promote-3' && $page === 'promote-3-pay') {
        $steps['page'] = 'promote-3-pay';
    } elseif ($steps['page'] === 'promote-4' && $page === 'promote-4-pay') {
        $steps['page'] = 'promote-4-pay';
    } elseif ($steps['page'] === 'refund-1' && $page === 'refund-1-request') {
        $steps['page'] = 'refund-1-request';
    }

    return $steps;
}
