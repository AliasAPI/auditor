<?php

declare(strict_types=1);

namespace AliasAPI\StepsTEAM;

function check_page(string $page): void
{
    if (! defined('BASEPATH')) {
        emergency("The BASEPATH is not defined.", 1);
    }

    if (! file_exists(BASEPATH . '/steps/' . $page . '.php')) {
        emergency("The $page file does not exist.", 2);
    }
}
