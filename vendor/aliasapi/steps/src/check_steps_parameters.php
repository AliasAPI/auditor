<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

function check_steps_parameters(array $steps): void
{
    if (! isset($steps['ally'])
        || ! is_array($steps['ally'])) {
        die('check_steps_parameters error ally');
    } elseif (! isset($steps['ally']['alias'])) {
        die('check_steps_parameters error ally alias');
    } elseif (! isset($steps['ally']['uuid'])) {
        die('check_steps_parameters error ally uuid');
    } elseif (! isset($steps['error'])) {
        die('check_steps_parameters error error');
    } elseif (! isset($steps['success'])) {
        die('check_steps_parameters error success');
    } elseif (! isset($steps['page'])) {
        die('check_steps_parameters error page');
    } elseif (! isset($steps['page_url'])) {
        die('check_steps_parameters error page_url');
    } elseif (! isset($steps['sponsor'])) {
        die('check_steps_parameters error s');
    }
}
