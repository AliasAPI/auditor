<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

function configure_total_steps(): array
{
    $total_steps[] = [ 'step' => 'promote-1', 'desc' => 'Purchase a lifetime membership' ];
    $total_steps[] = [ 'step' => 'training-1', 'desc' => 'Learn about the steps' ];
    $total_steps[] = [ 'step' => 'company', 'desc' => 'Learn about the business' ];
    $total_steps[] = [ 'step' => 'product', 'desc' => 'Learn about the product' ];
    $total_steps[] = [ 'step' => 'pay-plan', 'desc' => 'Learn about the compensation plan' ];
    $total_steps[] = [ 'step' => 'refund-1', 'desc' => 'Decide whether to request a refund' ];
    $total_steps[] = [ 'step' => 'register', 'desc' => 'Register a free online affiliate business' ];
    $total_steps[] = [ 'step' => 'star', 'desc' => 'Learn what it means to be a STAR' ];
    // $total_steps[] = [ 'step' => 'forum', 'desc' => 'Join the secret and exclusive forum' ];
    $total_steps[] = [ 'step' => 'refer-1', 'desc' => 'Learn how to refer and review the support page' ];
    $total_steps[] = [ 'step' => 'proof-1', 'desc' => 'See proof that StepsTEAM.com will work for you' ];
    $total_steps[] = [ 'step' => 'crowdfunding', 'desc' => 'Learn how the TEAM crowdfunds your business' ];
    $total_steps[] = [ 'step' => 'co-op-marketing', 'desc' => 'Learn all about the co-op marketing services' ];

    $total_steps[] = [ 'step' => 'winner', 'desc' => 'Complete the Course (during Prelaunch)' ];
    /*
    $total_steps[] = [ 'step' => 'promote-2', 'desc' => 'Hire experts to build your business' ];
    $total_steps[] = [ 'step' => 'refer-2', 'desc' => 'Wait as the experts build your team' ];
    $total_steps[] = [ 'step' => 'proof-2', 'desc' => 'See proof that your business grew' ];
    $total_steps[] = [ 'step' => 'upgrade-to-bronze', 'desc' => 'Upgrade to increase your commission percentage' ];
    $total_steps[] = [ 'step' => 'send-docs', 'desc' => 'Send your KYC documents to receive commissions' ];
    $total_steps[] = [ 'step' => 'disc', 'desc' => 'Learn your personality type' ];
    // $total_steps[] = [ 'step' => 'exchange', 'desc' => 'Learn about the K-exchange'];

    $total_steps[] = [ 'step' => 'promote-3', 'desc' => 'Hire experts to build your business' ];
    $total_steps[] = [ 'step' => 'refer-3', 'desc' => 'Wait as the experts build your team' ];
    $total_steps[] = [ 'step' => 'proof-3', 'desc' => 'See proof that your business grew' ];
    $total_steps[] = [ 'step' => 'purchase-product', 'desc' => 'Purchase a product on a monthly basis' ];

    $total_steps[] = [ 'step' => 'promote-4', 'desc' => 'Hire experts to build your business' ];
    $total_steps[] = [ 'step' => 'refer-4', 'desc' => 'Wait as the experts build your team' ];
    $total_steps[] = [ 'step' => 'proof-4', 'desc' => 'See proof that your business grew' ];

    $total_steps[] = [ 'step' => 'recruit-1', 'desc' => 'Build your own business one time' ];
    $total_steps[] = [ 'step' => 'praise-1', 'desc' => 'See proof that you built your business' ];

    $total_steps[] = [ 'step' => 'upgrade-to-vip', 'desc' => 'Upgrade to increase your commission percentage' ];
    // $total_steps[] = [ 'step' => 'agency', 'desc' => 'Learn how to build your business much faster' ];
    $total_steps[] = [ 'step' => 'winner', 'desc' => 'Complete the Course' ];
    */

    return $total_steps;
}
