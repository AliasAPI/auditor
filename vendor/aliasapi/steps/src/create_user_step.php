<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

function create_user_step(array $request, array $steps): array
{
    // What condition?
    if (isset($request['actionS'])
        && $request['actionS'] == 'create user step'
        && isset($request['uuid'])
        && isset($request['step'])
        && $request['step'] !== ''
        && isset($request['wise'])) {

        // $uid, $step, $datetime = null
        $key_pairs_row = [
            "uuid" => "" .  $request['uuid'] . "",
            "step" => "" . $request['step']. "",
            "wise" => "" . $request['wise']. ""
        ];

        $exists = CrudTable\read_rows('usersteps', $key_pairs_row);

        if ($exists == false) {
            $datetime_stamp = (new DateTime())->format('Y-m-d H:i:s');

            $key_pairs_row["datetime"] = $datetime_stamp;

            create_row('usersteps', $key_pairs_row);
        }
    }

    return $steps;
}
