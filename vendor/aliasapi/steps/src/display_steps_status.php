<?php


function display_steps_status()
{
    $total_steps = configure_total_steps();

    echo "<center><table style=\"border: 0px solid black;\"><tr><td NOWRAP><br>";

    foreach ($total_steps as $step) {
        echo "<p style=\"font-size:130%; font-weight:bold;\">STEP: ";

        echo $step['desc'];

        if ($step['step'] == $_SESSION['user_step']) {
            echo " <font color=#cc0000>[You are here!]</font></p>";
        } else {
            echo "</p>";
        }

        echo "<br>";

        if ($step['step'] == 'send-docs') {
            echo "<center><p style=\"font-size:130%; font-weight:bold; color:cc0000;\">You can qualify to start receiving commissions here!</p></center><br>";
        }
    }

    echo "</td></tr></table></center>";
}
