<?php


logout();


function logout()
{
    if( isset( $_GET['logout'] ) && $_GET['logout'] == 1 )
    {
        session_start();

        if( ini_get( "session.use_cookies" ) )
        {
            $params = session_get_cookie_params();

            setcookie( session_name(), '', time() - 42000, $params[ "path" ], $params[ "domain" ], $params[ "secure" ], $params[ "httponly" ] );
        }

        session_destroy();

        unset( $_SESSION );

        if( isset( $_GET['s'] ) && $_GET['s'] == 'ddemo' )
        {
            header( "location: " . WEBSITE . "?s=ddemo" );
        }
        else
        {
            header( "location: " . WEBSITE . "?page=support" );
        }
    }
}


?>
