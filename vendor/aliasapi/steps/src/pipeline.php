<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

use AliasAPI\Messages as Messages;
use AliasAPI\Steps as Steps;

function pipeline(array $request, array $user): array
{
    $total_steps = Steps\configure_total_steps();

    $steps = Steps\read_user_steps($request, $user);

    $steps = Steps\create_user_step($request, $steps);

    // $steps = Steps\update_user_step($request, $steps);

    // $steps = Steps\delete_user_step($request, $step);

    // 2DO+++ Use a flag to determine if there should be a second read
    // It's only necessary IF there has been a create, update, or delete
    $steps = Steps\read_user_steps($request, $user);

    $steps = Steps\set_page($total_steps, $steps);

    $steps = Steps\set_error($request, $steps);

    $steps = Steps\set_marketer($user, $steps);

    $steps = Steps\set_sponsor($user, $steps);

    $steps = Steps\set_success($request, $steps);

    $steps = Steps\alter_step($request, $steps);

    $steps = Steps\set_page_url($request, $steps);
    sayd($steps);

    // Steps\check_steps_parameters($steps);

    // 2DO+++ Remove. This has to do with the views server
    list($steps['http_code'], $steps['display']) =
    Messages\post_array($steps['page_url'], $steps);
    // sayd($steps);

    return $steps;
}
