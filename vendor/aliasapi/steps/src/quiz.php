<?php


function quiz($post, $session, $step)
{
    if (isset($post[ 'action' ]) && $post[ 'action' ] !== $step) {
        // Display the success message for the previous step
        display_view($post[ 'action' ] . '-success.html');
    } elseif (!isset($post[ 'action' ])) {
        display_view($step . '.html');

        if (file_exists(BASEPATH . 'steps/' . $step . '-extra.php')) {
            require_once(BASEPATH . 'steps/' . $step . '-extra.php');
        }

        // If the quiz exists
        if (check_webpage(VIEWS . $step . '-quiz.html')) {
            display_view($step . '-quiz.html');
        }
    } else {
        if (isset($post[ 'question1' ]) && isset($post[ 'question2' ])) {
            // If the user answers both questions correctly
            if ($post[ 'question1' ] == $post[ 'answer1' ] && $post[ 'question2' ] == $post[ 'answer2' ]) {
                insert_user_step($session[ 'userid' ], $step, '');

                step_up();
            } else {
                // If the user gets the wrong answer.
                display_view($step . '-fail.html');
            }
        }
        // If the user did NOT make a payment or get a refund
        elseif (! isset($post['transaction_success'])) {
            // If the user just clicks the button
            // or if the quiz is not set up right
            display_view($step . '-fail.html');
        }
    }
}
