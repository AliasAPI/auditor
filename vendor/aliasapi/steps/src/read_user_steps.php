<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

use AliasAPI\CrudTable as CrudTable;

function read_user_steps(array $request, array $user): array
{
    $steps = [];

    if (! empty($user)
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $key_pairs_array = [ 'uuid' => $user['uuid'] ];

        $rows = CrudTable\read_rows('usersteps', $key_pairs_array);

        // sid uuid step wise datetime
        foreach ($rows as $index => $step) {
            $user_steps[ $step['step'] ] = [
                'sid' => $step['sid'],
                'wise' => $step['wise'],
                'datetime' => $step['datetime']
            ];
        }

        $steps['user_steps'] = $user_steps;
    }

    return $steps;
}
