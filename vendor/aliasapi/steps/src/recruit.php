<?php


function recruit( $post, $session, $step )
{
    if( isset( $post[ 'action' ] ) && $post[ 'action' ] !== $step )
    {
        // Display the success message for the previous step
        display_view( $post[ 'action' ] . '-success.html' );
        display_view( 'footer.html' );
        die();
    }
    elseif( !isset( $post[ 'action' ] ) )
    {
        $recruits_owed = update_recruit( $session['userid'], $step );

        if( $recruits_owed > 0 )
        {
            display_view( $step . '.html' );
        }
        else
        {
            step_up();
        }
    }
}


?>
