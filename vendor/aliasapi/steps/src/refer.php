<?php


function refer($post, $session, $step)
{
    if (isset($post[ 'action' ]) && $post[ 'action' ] !== $step) {
        // Display the success message for the previous step
        display_view($post[ 'action' ] . '-success.html');
        display_view('footer.html');
        die();
    } elseif (!isset($post[ 'action' ])) {
        $refer_owed = update_refer($session['userid'], $step);

        if ($refer_owed > 0) {
            display_view($step . '.html');
        } else {
            step_up();
        }
    }
}
