<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

function set_error(array $request, array $steps): array
{
    $steps['error'] = '';

    return $steps;
}
