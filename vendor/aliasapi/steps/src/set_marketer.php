<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

function set_marketer(array $user, array $steps): array
{
    if (isset($user['marketer'])) {
        $steps['marketer'] = $user['marketer'];
    }

    return $steps;
}
