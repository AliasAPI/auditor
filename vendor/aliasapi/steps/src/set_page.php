<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

function set_page(array $total_steps, array $steps): array
{
    // If the user is NOT logged in, send them to the login page
    $steps['page'] = 'access';

    // If the user IS logged in . . .
    if (isset($steps['user_steps'])
        && is_array($steps['user_steps'])
        && ! empty($steps['user_steps'])) {
        foreach ($total_steps as $step) {
            if (! array_key_exists($step['step'], $steps['user_steps'])) {
                $steps['page'] = $step['step'];
                break;
            }
        }
    }

    return $steps;
}
