<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

function set_page_url(array $request, array $steps): array
{
    if (defined('VIEWS')) {
        $steps['page_url'] = VIEWS;
    }

    return $steps;
}
