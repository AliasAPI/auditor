<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

function set_sponsor(array $user, array $steps): array
{
    if (isset($user['sponsor'])) {
        $steps['sponsor'] = $user['sponsor'];
    }

    sayd($user);
    return $steps;
}
