<?php

declare(strict_types=1);

namespace AliasAPI\Steps;

function set_success(array $request, array $steps): array
{
    $steps['success'] = '';

    return $steps;
}
