<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_merchant(array $request, array $user): array
{
    // database
    if (isset($user['merchant'])) {
        $user['merchant'] = filter_var($user['merchant'], FILTER_SANITIZE_STRING);
    }

    // request update
    if (isset($request['merchant'])) {
        $request['params'] = array_change_key_case($request['params'], CASE_LOWER);
        $user['merchant'] = filter_var($request['params']['m'], FILTER_SANITIZE_STRING);
    }

    // neither set
    if (! isset($user['merchant'])) {
        $user['merchant'] = '';
    }

    return $user;
}
