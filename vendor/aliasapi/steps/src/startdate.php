<?php

declare(strict_types=1);

namespace AliasAPI\StepsTEAM;

function startdate(array $request, array $user): array
{
    // database
    if (isset($user['startdate'])) {
        $user['startdate'] = filter_var($user['startdate'], FILTER_SANITIZE_SPECIAL_CHARS);
    }

    // request update
    if (isset($request['startdate'])) {
        $user['startdate'] = filter_var($request['startdate'], FILTER_SANITIZE_SPECIAL_CHARS);
    }

    // neither set
    if (! isset($user['startdate']) || $user['startdate'] == '') {
        if (isset($user['login']) && $user['login'] !== '') {
            $user['startdate'] = date('Y-m-d H:i:s', time());
        } else {
            $user['startdate'] = '';
        }
    }

    return $user;
}
