<?php


function support($req, $session)
{
    echo "<table cellpadding=10 cellspacing=0 width=100% border=0><tr><td>";

    // If the user is not logged in, redirect to home
    if (!isset($session[ 'email' ]) && empty($session[ 'email' ])) {
        echo "<br><br><br><br><br><center><a href=index.php>Please click here and enter your best email address to log in.</center>";
        echo "<br><br><br><br><br><br>";
        die();
    }

    echo "<center><a href=index.php>Home</a></center>";

    $openhelp = "<center><br><br>";
    $closehelp = "</center><br><br><center><a href='index.php?page=support'>[close answer]</a></center><hr width=50%>";


    //-------------------------------------------------------------------------------------------------------------
    echo "<br><br>Where am I in the step by step process? [<a href=index.php?page=support&see=steps-status>answer</a>]";

    if (isset($req[ 'see' ]) && $req[ 'see' ] == 'steps-status') {
        echo $openhelp;
        display_steps_status();
        echo $closehelp;
    }


    //-------------------------------------------------------------------------------------------------------------
    echo "<br><br>How do I sponsor a new member? [<a href=index.php?page=support&see=sponsor>answer</a>]";

    if (isset($req[ 'see' ]) && $req[ 'see' ] == 'sponsor') {
        echo $openhelp;

        if (isset($session[ 'username' ])
            && !empty($session[ 'username' ])
            && is_array($session['user_steps'])
            && isset($session['user_steps']['register'])) {
            echo "Use the following link: " . WEBSITE . "?s=" . $session[ 'username' ] . " ";
        } else {
            echo "You need to <a href=index.php?page=support&see=steps-status>complete more steps</a> before you can personally sponsor others.";
        }

        echo $closehelp;
    }


    //-------------------------------------------------------------------------------------------------------------
    echo "<br><br>When will I earn commissions? [<a href=index.php?page=support&see=earnings-disclaimer>answer</a>]";

    if (isset($req[ 'see' ]) && $req[ 'see' ] == 'earnings-disclaimer') {
        echo $openhelp;
        display_view('earnings-disclaimer.html');
        echo $closehelp;
    }


    //-------------------------------------------------------------------------------------------------------------
    echo "<br><br>What are the Terms of Use? [<a href=index.php?page=support&see=terms-of-use>answer</a>]";

    if (isset($req[ 'see' ]) && $req[ 'see' ] == 'terms-of-use') {
        echo $openhelp;
        display_view('terms-of-use.html');
        echo $closehelp;
    }

    //-------------------------------------------------------------------------------------------------------------
    echo "<br><br>What is the Investment Policy? [<a href=index.php?page=support&see=policy-investment>answer</a>]";

    if (isset($req[ 'see' ]) && $req[ 'see' ] == 'policy-investment') {
        echo $openhelp;
        display_view('policy-investment.html');
        echo $closehelp;
    }


    //-------------------------------------------------------------------------------------------------------------
    echo "<br><br>What is the Liability Policy? [<a href=index.php?page=support&see=policy-liability>answer</a>]";

    if (isset($req[ 'see' ]) && $req[ 'see' ] == 'policy-liability') {
        echo $openhelp;
        display_view('policy-liability.html');
        echo $closehelp;
    }


    //-------------------------------------------------------------------------------------------------------------
    echo "<br><br>What is the Refund Policy? [<a href=index.php?page=support&see=policy-refund>answer</a>]";

    if (isset($req[ 'see' ]) && $req[ 'see' ] == 'policy-refund') {
        echo $openhelp;
        display_view('policy-refund.html');
        echo $closehelp;
    }


    //-------------------------------------------------------------------------------------------------------------
    echo "<br><br>What is the Privacy Policy? [<a href=index.php?page=support&see=policy-privacy>answer</a>]";

    if (isset($req[ 'see' ]) && $req[ 'see' ] == 'policy-privacy') {
        echo $openhelp;
        display_view('policy-privacy.html');
        echo $closehelp;
    }

    //-------------------------------------------------------------------------------------------------------------
    echo "<br><br>What is the DMCA Policy? [<a href=index.php?page=support&see=policy-dmca>answer</a>]";

    if (isset($req[ 'see' ]) && $req[ 'see' ] == 'policy-dmca') {
        echo $openhelp;
        display_view('policy-dmca.html');
        echo $closehelp;
    }

    //-------------------------------------------------------------------------------------------------------------
    if (is_array($session)
        && !empty($session)
        && is_array($session['user_steps'])
        && isset($session['user_steps']['disc'])) {
        echo "<br><br>What are my DISC results? [<a href=index.php?page=support&see=disc>answer</a>]";

        if (isset($req[ 'see' ]) && $req[ 'see' ] == 'disc') {
            echo $openhelp;
            display_disc_results($session);
            echo $closehelp;
        }
    }


    //-------------------------------------------------------------------------------------------------------------
    echo "<br><br>How do I log out? [<a href=index.php?page=support&see=logout>answer</a>]";

    if (isset($req[ 'see' ]) && $req[ 'see' ] == 'logout') {
        echo $openhelp;
        echo "<a href=" . WEBSITE . "?logout=1>Please click here to logout.</a>";
        echo $closehelp;
    }

    echo "<br><br><br></td></tr></table><br><br>";
}
