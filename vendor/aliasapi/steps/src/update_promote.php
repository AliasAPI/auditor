<?php


function update_promote( $status )
{
    if( isset( $status['type'] ) && !empty( $status['status'] )
        && isset( $status['status'] ) && !empty( $status['status'] ) )
    {
        if( $status['type'] == 'buy' && $status['status'] == 'completed' )
        {
            // Record it in the `usersteps` table
            insert_user_step( $status['userid'], $status['step'], NULL );
        }
        elseif( $status['type'] == 'buy' && $status['status'] !== 'completed' )
        {
            // If the transaction is failed, declined, expired, in-progress, processed,
            // partially_refunded, pending, refunded, reversed, voided, delete the step
            delete_user_step( $status['userid'], $status['step'] );
        }
        elseif( $status['type'] == 'refund' && $status['status'] == 'completed' )
        {
            // Record it in the `usersteps` table
            insert_user_step( $status['userid'], 'refund-1', NULL );

            // Also delete the promote-1 step completion
            delete_user_step( $status['userid'], 'promote-1' );
        }
        elseif( $status['type'] == 'refund' && $status['status'] !== 'completed' )
        {
            delete_user_step( $status['userid'], 'refund-1' );
        }
    }
}


?>
