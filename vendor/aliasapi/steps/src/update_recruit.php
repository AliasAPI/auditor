<?php


function update_recruit( $userid, $step )
{
    $recruit_owed = 0;

    // sayd( 'userid', $userid, 'step', $step );
    // Select the username
    list( $username ) = fetch_row( "SELECT `username` FROM `users`
                                    WHERE `userid` = '" . $userid . "'
                                    AND `username` != ''
                                    LIMIT 1 " );
// sayd( 'username', $username );
    if( $username )
    {
        $recruits = array();

        // Select the userids for all of the users the site recruited
        $result = query( "SELECT `userid` FROM `users` " .
                         "WHERE `sponsor` = '" . $username . "' " .
                         // Do not count the users that were personally recruited
                         "AND `recruited` = 'Y' " );
//sayd($result);
        // Loop through the users . . .
        while( list( $recruit_id ) = mysqli_fetch_array( $result ) )
        {
            // 2DO++ Add an if here for the referral_id
//sayd( 'referral_id', $referral_id );

            $startas = update_promote( $recruit_id, 'promote-1' );
//          sayd('startas', $startas );

            // Add the referrals to an array
            if( $startas !== NULL )
            {
                $recruits[] = $startas;
            }
        }

        if( is_array( $recruits ) && !empty( $recruits ) )
        {
            // Determine how many recruits the member has recruited
            $unique_recruits = count( array_unique( $recruits ) );
        }
        else
        {
            $unique_recruits = 0;
        }
// sayd( 'recruits', $recruits, 'unique_recruits', $unique_recruits );

        // Determine how many paid recruits the member OWES
        if( $step == 'recruit-1' )
        {
            $required_recruits = 1;
        }
        elseif( $step == 'recruit-2' )
        {
            $required_recruits = 2;
        }
        elseif( $step == 'recruit-3' )
        {
            $required_recruits = 3;
        }
        else
        {
            emergency("The step should be recruit-1, recruit-2, or recruit-3", 1 );
        }

        $recruits_owed = $required_recruits - $unique_recruits;

        // NOTE:
        // The recruit_id does NOT matter.
        // All that matters is the number of paid reecruits
        // that are credited to the specific $userid member
        // that is currently being processed.

        // Insert refer steps
        for( $i = 1; $i <= $unique_recruits; $i++ )
        {
            $recruit_step = 'recruit-' . $i;
            insert_user_step( $userid, $step );
        }

        // Delete refer steps
        for( $d = $unique_recruits; $d > $unique_recruits; $d-- )
        {
            $refer_step = 'recruit-' . $d;
            delete_user_step( $userid, $step );
        }
    }

    return $recruits_owed;
}


?>