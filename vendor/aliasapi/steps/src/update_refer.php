<?php


function update_refer( $userid )
{
    $referrals = array();
    $refer_owed = 0;
    $refer_paid = 0;
    $unique_referrals = 0;

    // Select the username
    list( $username ) = fetch_row( "SELECT `username` FROM `users`
                                    WHERE `userid` = '" . $userid . "'
                                    AND `username` != ''
                                    ORDER BY `userid`
                                    LIMIT 1 " );

    if( $username )
    {
        // Determine how many times the member paid for promote services
        list( $promotes_paid ) = fetch_row( "SELECT COUNT(`sid`) FROM `usersteps`
                                             WHERE `uid` = '" . $userid . "'
                                             AND `step` LIKE 'promote-%' " );

        // Select the userids for all of the users the site recruited
        $result = query( "SELECT `userid` FROM `users` " .
                         "WHERE `sponsor` = '" . $username . "' " .
                         // Do not count the users that were personally recruited
                         "AND `recruited` != 'Y' " .
                         "ORDER BY `userid` " );

        // Loop through the users . . .
        while( list( $referral_id ) = mysqli_fetch_array( $result ) )
        {
            // Add the referrals to an array
            $referrals[] = $referral_id;
        }

        if( is_array( $referrals ) && !empty( $referrals ) && count( $referrals ) > 0 )
        {
            $count_referrals = implode( ',', $referrals );

            // Count the referrals that have paid for promote-1
            // This matters for refunds and other problematic transactions
            list( $refer_paid ) = fetch_row( "SELECT COUNT(`sid`) FROM `usersteps`
                                              WHERE `uid` IN ( " . $count_referrals . " )
                                              AND `step` = 'promote-1' " );
        }

        // Subtract the referrals that paid from the payments
        $refer_owed = $promotes_paid - $refer_paid;

        // Update the refer value
        query( "UPDATE `users` SET `refer` = '" . $refer_owed . "'
                WHERE `userid` = '" . $userid . "'
                LIMIT 1 ");

        // Insert refer steps
        for( $i = 1; $i <= $refer_paid; $i++ )
        {
            $refer_step = 'refer-' . $i;
            insert_user_step( $userid, $refer_step );
        }

        // Delete refer steps
        for( $d = $refer_paid; $d > $refer_paid; $d-- )
        {
            $refer_step = 'refer-' . $d;
            delete_user_step( $userid, $refer_step );
        }
    }

    return $refer_owed;
}


?>
