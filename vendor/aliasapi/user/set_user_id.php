<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_id(array $request, array $user): array
{
    // database
    if (isset($user['id'])) {
        $user['id'] = filter_var($user['id'], FILTER_SANITIZE_INT);
    }

    // don't trust outside settings
    // if (isset($request['id'])) {
    //     $user['id'] = filter_var($request['id'], FILTER_SANITIZE_INT);
    // }

    // Do not set an invalid email

    return $user;
}
