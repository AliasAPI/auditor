<?php

declare(strict_types=1);

function access(array $request, $user)
{
    // If they have clicked the register button and there are no errors
    if (isset($_POST[ 'action' ]) && $_POST[ 'action' ] == "access") {
        sayd('1yoyuo');
        // Check the email
        list($email, $email_error) = check_email($_POST[ 'email' ]);

        $_POST[ 'email_error' ] = $email_error;

        // sayd( 'session', $_SESSION, 'POST', $_POST, 'email', $email, 'email_error', $_POST['email_error'] );
        // Prepare the fullname and email for storing
        // $email = check_storing($email, '');
        if (!$email_error) {
            // Generate the login link

            // Send the email

            // See if the login link is in the users table

            list($userid, $email_exists, $login_exists) = select_login($email, '');

            // sayd('userid', $userid,  'email', $email, 'email_error', $email_error, 'email_exists', $email_exists );
            // Otherwise if the email is NOT there, add it to the database
            // if( empty( $email_exists ) || !isset( $email_exists ) )
            if (empty($email_exists) || !isset($email_exists)) {
                // Record the sponsor and merchant references
                list($userid, $login_exists) =
                    insert_user($email, $_POST['capture_sponsor'], $_POST['capture_merchant']);
            }

            if (isset($userid) && !empty($userid)) {
                insert_user_step($userid, 'startdate');
                insert_user_step($userid, 'lastlogin', '0000-00-00 00:00:00');
                insert_user_step($userid, 'lastcontact', '0000-00-00 00:00:00');
            }

            if (empty($login_exists) || !isset($login_exists)) {
                list($login_link, $login_code) = generate_login_link($email);
                update_login($email, $login_code);
            }

            list($userid, $email_exists, $login_exists) = select_login($email, '');
            $_POST[ 'email_exists' ] = $email_exists;

            // If the account is all da way turnt up . . .
            if (!empty($email_exists) && !empty($login_exists)) {
                // sayd('signup_success', $signup_success, 'registration_error', $registration_error, 'login_link', $login_link, 'login_code', $login_code );
                // Define the file that will be used for the email
                $email = array();
                $email[ 'sender' ] = 'StepsTEAM';
                $email[ 'from' ] = SITE_EMAIL;
                $email[ 'reply-to' ] = SITE_EMAIL;
                $email[ 'to' ] = $email_exists;
                $email[ 'subject' ] = 'Your Login Link';
                $email[ 'html' ] = VIEWS . 'access-email.html';
                $email[ 'login_link' ] = WEBSITE . '?L=' . $login_exists;
                $email[ 'message' ] = 'This is how we do it';

                if (defined('MODE') && MODE == 'demo' && $email_exists == 'demo@stepsteam.com') {
                    echo "<center><a href=" . $email[ 'login_link' ] . "><font color=red>Click this Login Link to log in</font></a><br><br>
                          <font color=red>You are in the demo account mode.<br>
                          Therefore, the login link will not be emailed.</font></center>";
                    echo "<br><hr color=#000000>";
                }

                // Direct them to log in with the link
                display_view('access-success.html');

                send_email($email);
            }
        }
    }

    // If the user filled out the form, do not show the sales copy.
    if (!isset($_POST[ 'email_exists' ]) && !isset($_POST[ 'email_error' ])) {
        display_view('access.html');
    }

    if (isset($_POST[ 'email_error' ])) {
        echo "<br><br><br><br><br><br>";
    }
}
