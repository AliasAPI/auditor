<?php


function assign_sponsor($userid)
{
    $assigned_sponsor = '';

    if (isset($userid) && !empty($userid)) {
        // Pull the sponsor column to make sure the member does not already have a sponsor
        list($assigned_sponsor) = fetch_row("SELECT `sponsor` FROM `users`
                                                WHERE `userid` = '" . $userid . "'
                                                LIMIT 1 ");
    } else {
        emergency("No userid was set!", 1);
    }

    // If the user doesn't already have a sponsor, select a qualified one
    if (!$assigned_sponsor) {
        list($assigned_sponsor) = fetch_row("SELECT * FROM
                                                ( SELECT `username` FROM `users`
                                                  WHERE refer >= 1
                                                  AND `email` != ''
                                                  AND `username` != ''
                                                  ORDER BY userid
                                                  LIMIT 12 ) AS owed
                                                ORDER BY RAND()
                                                LIMIT 1 ");

        // If there is no sponsor, emergency()
        if (!$assigned_sponsor) {
            emergency("No sponsors where found to assign!", 1);
        }

        if (isset($userid) && !empty($userid)) {
            // Save the assigned sponsor to force the users to accept the sponsor.
            query("UPDATE `users` SET `sponsor` = '" . $assigned_sponsor . "'
                    WHERE `userid` = '" . $userid . "'
                    LIMIT 1 ");
        }
    }

    return $assigned_sponsor;
}
