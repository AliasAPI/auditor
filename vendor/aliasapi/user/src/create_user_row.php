<?php

declare(strict_types=1);

namespace AliasAPI\User;

use AliasAPI\CrudTable as CrudTable;

function create_user_row(array $identity): void
{
    $user = [];
    // 2DO+++ Make the request['actionS'] == create user
    if (isset($identity['email']) && ! empty($identity['email'])) {
        $user_row = [
            'email' => $identity['email'],
            'uuid' => '',
            'fullname' => '',
            'phone' => 0,
            'sponsor' => '',
            'recruited' => '',
            'username' => '',
            'refer' => 0,
            'referrals' => 0,
            'recommends' => 0,
            'recruits' => 0,
            'lastlogin' => '0000-00-00 00:00:00',
            'quitdate' => '',
            'login' => ''
        ];
        // Find the email or create a row with it
        $worthless_result = CrudTable\create_row('users', $user_row);
    }
}
