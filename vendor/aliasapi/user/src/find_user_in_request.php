<?php

declare(strict_types=1);

namespace AliasAPI\User;

function find_user_in_request(array $request): array
{
    $identity = [];

    // Possible user column keys: login, email, uuid, uuid, username
    if (isset($request['params']['L']) && ! empty($request['params']['L'])) {
        $identity = [ 'login' => filter_var($request['params']['L'], FILTER_SANITIZE_STRING) ];
    } elseif (isset($request['params']['login']) && ! empty($request['params']['login'])) {
        $identity = [ 'login' => filter_var($request['params']['login'], FILTER_SANITIZE_STRING) ];
    } elseif (isset($request['params']['email']) && ! empty($request['params']['email'])) {
        $identity = [ 'email' => filter_var($request['params']['email'], FILTER_SANITIZE_EMAIL) ];
    } elseif (isset($request['uuid']) && ! empty($request['uuid'])) {
        $identity = [ 'uuid' => filter_var($request['uuid'], FILTER_SANITIZE_STRING) ];
    } elseif (isset($request['body']['uuid']) && ! empty($request['body']['uuid'])) {
        $identity = [ 'uuid' => filter_var($request['body']['uuid'], FILTER_SANITIZE_STRING) ];
    } elseif (isset($request['ally']['user']) && ! empty($request['ally']['user'])) {
        $identity = [ 'uuid' => filter_var($request['ally']['user'], FILTER_SANITIZE_STRING) ];
    } elseif (isset($request['body']['ally']['user']) && ! empty($request['body']['ally']['user'])) {
        $identity = [ 'uuid' => filter_var($request['body']['ally']['user'], FILTER_SANITIZE_STRING) ];
    } elseif (isset($request['params']['username']) && ! empty($request['params']['username'])) {
        $identity = [ 'username' => filter_var($request['params']['username'], FILTER_SANITIZE_STRING) ];
    } elseif (isset($request['params']['s']) && $request['params']['s'] == 'ddemo') {
        $identity = [ 'email' => 'demo@stepsteam.com' ];
    }

    return $identity;
}
