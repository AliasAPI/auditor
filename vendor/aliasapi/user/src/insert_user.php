<?php


function insert_user( $email, $capture_sponsor, $capture_merchant )
{

    // If the sponsor is validated during the initial registration
    if( isset( $capture_sponsor ) && !empty( $capture_sponsor ) )
    {
        // . . . then they were recruited.
        $recruited = 'Y';
    }
    else
    {
        $recruited = '';
    }

    // Get the new userid
    list( $userid ) = fetch_row( "SELECT `userid` FROM `users`
                                  WHERE `email` = '" . $email . "'
                                  LIMIT 1 " );

    if( !$userid )
    {
        // Record the contact information
        query( "INSERT IGNORE INTO `users` SET
               `email` = '" . $email . "',
               `sponsor` = '" . $capture_sponsor . "',
               `recruited` = '" . $recruited . "',
               `merchant` = '" . $capture_merchant . "',
               `notify` = '2' " );
    }

    // Get the new userid
    list( $userid, $login ) = fetch_row( "SELECT `userid`, `login` FROM `users`
                                          WHERE `email` = '" . $email . "'
                                          LIMIT 1 " );

    return array( $userid, $login );
}


?>
