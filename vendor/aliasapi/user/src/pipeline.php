<?php

declare(strict_types=1);

namespace AliasAPI\User;

use AliasAPI\User as User;

// id fullname email phone sponsor recruited username refer lastlogin quitdate login
function pipeline(array $request)
{
    $identity = User\find_user_in_request($request);

    User\create_user_row($identity);

    $user_start = User\read_user_row($identity);

    $user = User\set_user_email($request, $user_start);

    $user = User\set_user_uuid($request, $user);

    $user = User\set_user_fullname($request, $user);

    $user = User\set_user_phone($request, $user);

    $user = User\set_user_sponsor($request, $user);

    $user = User\set_user_recruited($request, $user);

    $user = User\set_user_username($request, $user);

    $user = User\read_user_sponsored($request, $user);

    $user = User\set_user_refer($request, $user);

    $user = User\set_user_referrals($request, $user);

    $user = User\set_user_recommends($request, $user);

    $user = User\set_user_recruits($request, $user);

    $user = User\set_user_lastlogin($request, $user);

    $user = User\set_user_quitdate($request, $user);

    $user = User\set_user_login($request, $user);

    $user = User\set_user_row($request, $user);

    User\update_user_row($user_start, $user);

    $user = User\set_user_loginlink($request, $user);

    User\set_user_session($request, $user);

    return $user;
}
