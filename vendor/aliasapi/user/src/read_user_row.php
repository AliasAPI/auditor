<?php

declare(strict_types=1);

namespace AliasAPI\User;

use AliasAPI\CrudTable as CrudTable;

function read_user_row(array $key_pairs_array): array
{
    $user = [];

    // Possible user column keys: login, email, userid, uuid, username
    if (isset($key_pairs_array) && ! empty($key_pairs_array)) {
        // Return the entire user row
        $result = CrudTable\read_rows('users', $key_pairs_array);

        // Remove the array that holds all the records and return the first result
        $user = (isset($result[0]) && ! empty($result[0])) ? $result[0] : array();
    }

    return $user;
}
