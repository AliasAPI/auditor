<?php

declare(strict_types=1);

namespace AliasAPI\User;

use AliasAPI\CrudTable as CrudTable;

function read_user_sponsored(array $request, array $user): array
{
    if (isset($user['username']) && $user['username'] !== '') {
        $key_pairs_array = [ 'sponsor' => $user['username'] ];
        $referral_rows = CrudTable\read_rows('users', $key_pairs_array);

        foreach ($referral_rows as $index => $ref) {
            // The presence of the username indicates the member has paid and registered
            if ($ref['recruited'] == 'Y' && $ref['username'] !== '') {
                $user['recruits'][] = [ 'username' => $ref['username'], 'email' => $ref['email'] ];
            // The recruited = Y without a username has been sponsored, but not a paid registered member
            } elseif ($ref['recruited'] == 'Y') {
                $user['recommends'][] = [ 'email' => $ref['email'] ];
            // The sponsored user with recruited = '' and  username != '' was referred by StepsTEAM
            } elseif ($ref['recruited'] !== 'Y' && $ref['username'] !== '') {
                $user['referrals'][] = [ 'username' => $ref['username'], 'email' => $ref['email'] ];
            } else {
                // The sponsored user with blank recruited and blank username is being referred by StepsTEAM
                $ref['referred'][] = [ 'email' => $ref['email'] ];
            }
        }
    }

    return $user;
}
