<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_email(array $request, array $user): array
{
    // database
    if (isset($user['email'])) {
        $user['email'] = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
    }

    // request update
    if (isset($request['email'])) {
        $user['email'] = filter_var($request['email'], FILTER_SANITIZE_EMAIL);
    }

    // Do not set an invalid email

    return $user;
}
