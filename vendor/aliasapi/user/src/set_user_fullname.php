<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_fullname(array $request, array $user): array
{
    // database
    if (isset($user['fullname'])) {
        $user['fullname'] = filter_var($user['fullname'], FILTER_SANITIZE_STRING);
    }

    // request update
    if (isset($request['fullname'])
        && isset($user['email'])
        && $user['email'] !== '') {
        $user['fullname'] = filter_var($request['fullname'], FILTER_SANITIZE_STRING);
    }

    // neither set
    if (! isset($user['fullname'])
        && isset($user['email'])
        && $user['email'] !== '') {
        $user['fullname'] = '';
    }

    return $user;
}
