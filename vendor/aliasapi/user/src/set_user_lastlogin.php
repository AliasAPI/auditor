<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_lastlogin(array $request, array $user): array
{
    // database
    if (isset($user['lastlogin'])) {
        $user['lastlogin'] = filter_var($user['lastlogin'], FILTER_SANITIZE_SPECIAL_CHARS);
    }

    // request update
    if (isset($request['lastlogin'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['lastlogin'] = filter_var($request['lastlogin'], FILTER_SANITIZE_SPECIAL_CHARS);
    }

    // Update the login if the user is present and used their login link
    if (isset($user['lastlogin'])
        && ((isset($request['params']['L']) && $request['params']['L'] !== '')
        || (isset($request['params']['l']) && $request['params']['l'] !== ''))) {
        $user['lastlogin'] = date('Y-m-d H:i:s', time());
    }

    // default
    if (! isset($user['lastlogin'])) {
        $user['lastlogin'] = 0;
    }

    return $user;
}
