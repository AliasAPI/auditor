<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_login(array $request, array $user): array
{
    // database
    if (isset($user['login'])) {
        $user['login'] = filter_var($user['login'], FILTER_SANITIZE_STRING);
    }

    // request update
    if (isset($request['login'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['login'] = filter_var($request['login'], FILTER_SANITIZE_STRING);
    }

    // neither set
    if (! isset($user['login'])
        && isset($user['email'])
        && $user['email'] !== ''
        && defined('ALIAS_SALT')) {
        $user['login'] = sha1($user['email'] . ALIAS_SALT);
    }

    return $user;
}
