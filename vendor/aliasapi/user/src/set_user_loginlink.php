<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_loginlink(array $request, array $user): array
{
    if (defined('WEBSITE') && isset($user['login']) && $user['login'] !== '') {
        $user['login_link'] = WEBSITE . '?L=' . $user['login'];
    }

    return $user;
}
