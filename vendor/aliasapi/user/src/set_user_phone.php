<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_phone(array $request, array $user): array
{
    // database
    if (isset($user['phone'])) {
        $user['phone'] = filter_var($user['phone'], FILTER_SANITIZE_NUMBER_INT);
    }

    // request update
    if (isset($request['phone'])) {
        $request['phone'] = str_replace(' ', '-', $request['phone']);
        $request['phone'] = str_replace('.', '-', $request['phone']);
        $user['phone'] = filter_var($request['phone'], FILTER_SANITIZE_NUMBER_INT);
    }

    // neither set
    if (! isset($user['phone'])
        && isset($user['email'])
        && $user['email'] !== '') {
        $user['phone'] = 0;
    }

    return $user;
}
