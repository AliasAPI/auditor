<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_quitdate(array $request, array $user): array
{
    // database
    if (isset($user['quitdate'])) {
        $user['quitdate'] = filter_var($user['quitdate'], FILTER_SANITIZE_SPECIAL_CHARS);
    }

    // request update
    if (isset($request['quitdate'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['quitdate'] = filter_var($request['quitdate'], FILTER_SANITIZE_SPECIAL_CHARS);
    }

    // neither set
    if (! isset($user['quitdate'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['quitdate'] = '';
    }

    return $user;
}
