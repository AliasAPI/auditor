<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_recommends(array $request, array $user): array
{
    // database
    if (isset($user['recommends']) && is_array($user['recommends']) && ! empty($user['recommends'])) {
        $referral_count = count($user['recommends']);
    }

    // request update
    if (isset($request['recommends'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['recommends'] = filter_var($request['recommends'], FILTER_SANITIZE_INT);
    }

    // neither set
    if (! isset($user['recommends'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['recommends'] = 0;
    }

    return $user;
}
