<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_recruited(array $request, array $user): array
{
    // database
    if (isset($user['recruited'])) {
        $user['recruited'] = filter_var($user['recruited'], FILTER_SANITIZE_STRING);
    }

    // request update
    if (isset($request['recruited'])) {
        $user['recruited'] = filter_var($request['recruited'], FILTER_SANITIZE_STRING);
    }

    // neither set
    if (! isset($user['recruited'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['recruited'] = '';
    }

    return $user;
}
