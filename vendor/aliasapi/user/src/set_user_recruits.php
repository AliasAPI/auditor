<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_recruits(array $request, array $user): array
{
    // database
    if (isset($user['recruits']) && is_array($user['recruits']) && ! empty($user['recruits'])) {
        $referral_count = count($user['recruits']);
    }

    // request update
    if (isset($request['recruits'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['recruits'] = filter_var($request['recruits'], FILTER_SANITIZE_INT);
    }

    // neither set
    if (! isset($user['recruits'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['recruits'] = 0;
    }

    return $user;
}
