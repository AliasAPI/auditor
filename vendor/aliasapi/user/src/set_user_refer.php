<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_refer(array $request, array $user): array
{
    // database
    if (isset($user['refer'])) {
        $user['refer'] = filter_var($user['refer'], FILTER_VALIDATE_INT);
    }

    // request update
    if (isset($request['refer'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['refer'] = filter_var($request['refer'], FILTER_VALIDATE_INT);
    }

    // neither set
    if (! isset($user['refer'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['refer'] = 0;
    }

    return $user;
}
