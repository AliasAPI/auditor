<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_referrals(array $request, array $user): array
{
    // database
    if (isset($user['referrals']) && is_array($user['referrals']) && ! empty($user['referrals'])) {
        $referral_count = count($user['referrals']);
    }

    // request update
    if (isset($request['referrals'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['referrals'] = filter_var($request['referrals'], FILTER_SANITIZE_INT);
    }

    // neither set
    if (! isset($user['referrals'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['referrals'] = 0;
    }

    return $user;
}
