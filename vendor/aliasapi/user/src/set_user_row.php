<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_row(array $request, array $user): array
{
    $user_row = [];
    // Specify what keys to store (and ignore any other keys set in $user)
    // id fullname email phone merchant sponsor recruited username refer lastlogin quitdate login
    if (isset($user['uuid'])) {
        $user_row = [
            'id' => $user['id'],
            'email' => $user['email'],
            'uuid' => $user['uuid'],
            'fullname' => $user['fullname'],
            'phone' => $user['phone'],
            'sponsor' => $user['sponsor'],
            'recruited' => $user['recruited'],
            'username' => $user['username'],
            'refer' => $user['refer'],
            'referrals' => $user['referrals'],
            'recommends' => $user['recommends'],
            'recruits' => $user['recruits'],
            'lastlogin' => $user['lastlogin'],
            'quitdate' => $user['quitdate'],
            'login' => $user['login']
        ];
    }

    return $user_row;
}
