<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_session(array $request, array $user): array
{
    $_SESSION['user'] = $user;

    return $user;
}
