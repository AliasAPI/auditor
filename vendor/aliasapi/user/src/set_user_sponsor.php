<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_sponsor(array $request, array $user): array
{
    // database
    if (isset($user['sponsor'])) {
        $user['sponsor'] = filter_var($user['sponsor'], FILTER_SANITIZE_STRING);
    }

    // request update
    if (isset($user['uuid'])
        && (isset($request['params']['s'])
        || isset($request['params']['S']))) {
        $request['params'] = array_change_key_case($request['params'], CASE_LOWER);
        $sponsor = strtolower(filter_var($request['params']['s'], FILTER_SANITIZE_STRING));
        $sponsor_row = read_user_row(['username' => $sponsor ]);
        if (isset($sponsor_row['username']) && $sponsor_row['username'] !== '') {
            $user['sponsor'] = $sponsor_row['username'];
        }
    }

    // neither set
    if (! isset($user['sponsor'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['sponsor'] = '';
    }

    return $user;
}
