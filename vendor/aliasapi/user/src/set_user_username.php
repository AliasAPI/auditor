<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_username(array $request, array $user): array
{
    // database
    if (isset($user['username'])) {
        $user['username'] = filter_var($user['username'], FILTER_SANITIZE_STRING);
    }

    // request update
    if (isset($request['username'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['username'] = strtolower(filter_var($request['username'], FILTER_SANITIZE_STRING));
    }

    // neither set
    if (! isset($user['username'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['username'] = '';
    }

    return $user;
}
