<?php

declare(strict_types=1);

namespace AliasAPI\User;

function set_user_uuid(array $request, array $user): array
{
    // database
    if (isset($user['uuid'])) {
        $user['uuid'] = filter_var($user['uuid'], FILTER_SANITIZE_STRING);
    }

    // request update
    if (isset($request['uuid'])
        && isset($user['uuid'])
        && $user['uuid'] !== '') {
        $user['uuid'] = filter_var($request['uuid'], FILTER_SANITIZE_STRING);
    }

    // neither set
    if (! isset($user['uuid'])
        && isset($user['email'])
        && $user['email'] !== '') {
        $token = '';
        // Define a character set that is easily communicated
        $characters = "AFHILKQRSWXY2345679";
        // Count the characters in the set
        $characterslength = strlen($characters)-1;
        // Loop 6 times to get 6 random characters for $login
        while (strlen($token) < 6) {
            $token .= $characters[rand(0, $characterslength)];
            // Make sure the string begins with a letter
            $token = preg_replace("/^[^A-Z]+/", '', $token);
        }

        $user['uuid'] = $token . date('YmdHis', time());
    }

    return $user;
}
