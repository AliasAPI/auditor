<?php

declare(strict_types=1);

namespace AliasAPI\User;

use AliasAPI\CrudTable as CrudTable;

function update_user_row(array $key_pairs_array, array $update_pairs_array): void
{
    if (! empty($key_pairs_array) && ! empty($update_pairs_array)) {
        CrudTable\update_rows('users', $key_pairs_array, $update_pairs_array);
    }
}
